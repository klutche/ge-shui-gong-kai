<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'forms/LoginForm.php';

class Auth extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('encrypt');
    }

    public function index()
    {
        show_404();
    }

    public function login()
    {
        if ($this->session->userdata('logged_in')) {
            redirect('');
        }

        assign('title', 'ログイン');

        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $form = new LoginForm();
            assign('form', $form);
            render('auth/login');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new LoginForm($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render('auth/login');
                return;
            }

            $data = $form->cleaned_data;

            if ($this->auth($data['username'], $data['password']) === FALSE) {
                assign('form', $form);
                assign('error', 'ログインできません。');
                render('auth/login');
                return;
            }

            $this->session->set_userdata('logged_in', true);

            if (isset($data['redirect_url']) && strlen($data['redirect_url']) > 0) {
                redirect($data['redirect_url']);
                return;
            } else {
                redirect('member/index');
                return;
            }
        }
    }

    public function logout()
    {
        filter_auth();

        $this->session->sess_destroy();
        redirect('');
    }

    private function auth($username, $password)
    {
        $saved_username = get_item('admin_username');
        $saved_password = $this->encrypt->decode(get_item('admin_password'));

        if ($username == $saved_username && $password == $saved_password) {
            return true;
        }

        return false;
    }

}

