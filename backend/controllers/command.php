<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Command extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('membermodel');
        $this->layout = '__NONE__';
    }

    public function deleteExpiredNonMember()
    {
        $this->membermodel->deleteExpiredNonMember(1); // 1day
        log_message('info', 'Delete expired non-members: ' . $this->db->affected_rows() . ' member(s)');
    }

}
