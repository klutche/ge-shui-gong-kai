<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'forms/ConfigForm.php';

class Config extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        filter_auth();
        $this->load->model('configmodel');
    }

    public function index()
    {

        $configs = $this->configmodel->search(array(), 0, 1000000, 'id');

        assign('configs', $configs);
        assign('title', '設定');
        render('config/index');
    }

    public function input($id=null)
    {

        assign('title', '設定' . is_null($id) ? '追加' : '変更');
        assign('id', $id);

        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $init = null;
            if (!is_null($id)) {
                $config = $this->configmodel->get($id);
                if (is_string($config->value)) {
                    $decoded_value = $this->encrypt->decode($config->value);
                    if ($decoded_value !== false) {
                        $config->value = $decoded_value;
                        $config->encrypt = 1;
                    }
                } else {
                    $config->value = 'return ' . var_export($config->value, true) . ';';
                    $config->php = 1;
                }
                $init = $config;
            }
            $form = new ConfigForm(null, $init);
            assign('form', $form);
            render('config/input');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new ConfigForm($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render('config/input');
                return;
            }

            $data = $form->cleaned_data;

            try {
                $value = $data['value'];
                if (isset($data['encrypt'])) {
                    $value = $this->encrypt->encode($value);
                } elseif (isset($data['php'])) {
                    $value = eval($value);
                }

                set_item($data['key'], $value, $data['label']);

                $cache_file = dirname(APPPATH) . '/frontend/cache/config_' . $data['key'];
                if (is_file($cache_file)) {
                    unlink($cache_file);
                }

            } catch (Exception $e) {
                assign('form', $form);
                assign('error', $e->getMessage());
                render('config/input');
                return;
            }

            redirect('config/index');
        }
    }

    public function delete($id)
    {
        $this->configmodel->delete($id);
        redirect('config/index');
    }


}
