<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends CI_Controller {

    /*=====================================================
    コンストラクタ
    =====================================================*/
    public function __construct()
    {
        parent::__construct();
        filter_auth();
        $this->load->library('pagination');
    }

    /*=====================================================
    indexページ
    =====================================================*/
    public function index()
    {
        /* タイトル
        -------------------------------------------------*/
        assign('title', '送信メール履歴');
        
        /* ページネーション設定
        -------------------------------------------------*/
        $config['base_url'] = '/__admin__/email/index';
        $config['per_page'] = 20;
        $config['num_links'] = 3;
        
        /* 検索
        -------------------------------------------------*/
        $word = '';
        $like_array = array();
        if ($this->input->get('word')) {
            $word = preg_replace('/\A[\x00\s]++|[\x00\s]++\z/u', '', $this->input->get('word'));
            $like_array = array(
                'type' => $word,
                'mailto' => $word,
                'name' => $word
            );
            $config['suffix'] = '?word='.$word;
            $config['first_url'] = $config['base_url'].'?word='.$word;
        }
        assign('word', $word);
        
        /* データ取得
        -------------------------------------------------*/
        $data = $this->db
            ->order_by('id', 'DESC')
            ->or_like($like_array)
            ->get('emails', $config['per_page'], $this->uri->segment(3))
            ->result_array();
        assign('data', $data);
        
        /* ページネーション
        -------------------------------------------------*/
        $config['total_rows'] = $this->db
            ->order_by('id', 'DESC')
            ->or_like($like_array)
            ->get('emails')
            ->num_rows();
        $this->pagination->initialize($config);
        assign('pager', $this->pagination->create_links());
        
        /* 描画
        -------------------------------------------------*/
        render('email/index');
    }
    
    /*=====================================================
    detailページ
    =====================================================*/
    public function detail($id = NULL)
    {
        /* タイトル
        -------------------------------------------------*/
        assign('title', 'メール詳細');
        
        /* IDチェック
        -------------------------------------------------*/
        if (is_null($id)) {
            show_404();
            return;
        }
        $query = $this->db
            ->where('id', $id)
            ->get('emails');
        if ($query->num_rows() < 1) {
            show_404();
            return;
        }
        
        /* データ取得
        -------------------------------------------------*/
        $data = $query->row_array();
        $prefix = $data['type'] == '見積もり' ? 'estimate' : 'order';
        assign('data', $data);
        assign('prefix', $prefix);
        
        /* 描画
        -------------------------------------------------*/
        render('email/detail');
        return;
    }
    
    /*=====================================================
    csvダウンロードページ
    =====================================================*/
    public function csv()
    {
        $this->load->dbutil();
        $this->load->helper('download');
        // 項目名
        $select = array(
            'id as "ID"',
            'type as "タイプ"',
            'template as "テンプレート"',
            'source_id as "見積・注文ID"',
            'mailto as "送信先アドレス"',
            'name as "送信先名前"',
            'subject as "件名"',
            'content as "メール本文"',
            'created_at as "送信日時"',
        );
        $query = $this->db->select($select)->get('emails');
        $csv = '';
        $delim = ",";
        $newline = "\n";
        $enclosure = '"';
        foreach ($query->list_fields() as $name) {
            $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $name).$enclosure.$delim;
        }
        $csv = rtrim($csv);
        $csv .= $newline;
        foreach ($query->result_array() as $row) {
            foreach ($row as $key => $item) {
                $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $item).$enclosure.$delim;
            }
            $csv = rtrim($csv);
            $csv .= $newline;
        }
        $csv = mb_convert_encoding($csv,'Shift-JIS','UTF-8');
        $fname = '送信メール_'.date("ymdhi",time()).'.csv';
        force_download($fname , $csv);
    }
}
