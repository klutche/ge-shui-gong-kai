<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Enquete extends CI_Controller {

    /*=====================================================
    コンストラクタ
    =====================================================*/
    public function __construct()
    {
        parent::__construct();
        filter_auth();
        $this->load->library('pagination');
    }

    /*=====================================================
    indexページ
    =====================================================*/
    public function index()
    {
        /* タイトル
        -------------------------------------------------*/
        assign('title', 'アンケート一覧');
        
        /* CSS, JS
        -------------------------------------------------*/
        assign('styles', array('assets/css/jquery-ui.min.css'));
        assign('scripts', array('assets/js/libs/jquery-ui.min.js', 'assets/js/enquete.js'));
        
        /* ページネーション設定
        -------------------------------------------------*/
        $config['per_page'] = 20;
        $config['num_links'] = 3;
        $config['base_url'] = '/__admin__/enquete/index';
        
        /* データ取得
        -------------------------------------------------*/
        $data = $this->db
            ->where('deleted_at', NULL)
            ->order_by('id', 'DESC')
            ->get('enquete', $config['per_page'], $this->uri->segment(3))
            ->result_array();
        assign('data', $data);
        
        /* 注文情報取得
        -------------------------------------------------*/
        $orders = array();
        foreach ($data as $val) {
            $orders[$val['order_id']] = $this->db
                ->select('name, email')
                ->where('id', $val['order_id'])
                ->get('orders')
                ->row_array();
        }
        assign('orders', $orders);
        
        /* ページネーション
        -------------------------------------------------*/
        $config['total_rows'] = $this->db
            ->where('deleted_at', NULL)
            ->get('enquete')
            ->num_rows();
        $this->pagination->initialize($config);
        assign('pager', $this->pagination->create_links());
        
        /* 描画
        -------------------------------------------------*/
        render('enquete/index');
        return;
    }
    
    /*=====================================================
    detailページ
    =====================================================*/
    public function detail($id = NULL)
    {
        /* IDチェック
        -------------------------------------------------*/
        $query = $this->db
            ->where('id', $id)
            ->get('enquete');
        if (!is_null($id)) {
            if ($query->num_rows() < 1) {
                show_404();
                return;
            }
        }
        assign('id', $id);
        
        /* タイトル
        -------------------------------------------------*/
        $title = 'アンケート詳細';
        assign('title', $title);
        
        /* データ取得
        -------------------------------------------------*/
        $data = $query->row_array();
        $data['question_1'] = unserialize($data['question_1']);
        assign('data', $data);
        
        /* 注文情報取得
        -------------------------------------------------*/
        $order = $this->db
            ->select('name, email')
            ->where('id', $data['order_id'])
            ->get('orders')
            ->row_array();
        assign('order', $order);
        
        /* 描画
        -------------------------------------------------*/
        render('enquete/detail');
        return;
    }
    
    /*=====================================================
    csvダウンロードページ
    =====================================================*/
    public function csv()
    {
        $this->load->dbutil();
        $this->load->helper('download');
        // 項目名
        $select = array(
            'id as "ID"',
            'order_id as "注文ID"',
            'member_id as "会員ID"',
            'question_1 as "当サイトをどこで知りましたか？"',
            'remarks_1 as "その他SNS"',
            'remarks_2 as "知人からの口コミ"',
            'remarks_3 as "口コミサイトを見て"',
            'remarks_4 as "その他"',
            'created_at as "送信日時"',
        );
        $this->db->select($select);
        $this->db->where('deleted_at', NULL);
        if ($this->input->post('start')) {
            $this->db->where('created_at >=', $this->input->post('start').' 00:00:00');
        }
        if ($this->input->post('end')) {
            $this->db->where('created_at <=', $this->input->post('end').' 23:59:59');
        }
        $query = $this->db->get('enquete');
        $csv = '';
        $delim = ",";
        $newline = "\n";
        $enclosure = '"';
        foreach ($query->list_fields() as $name) {
            $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $name).$enclosure.$delim;
        }
        $csv = rtrim($csv);
        $csv .= $newline;
        foreach ($query->result_array() as $row) {
            foreach ($row as $key => $item) {
                if ($key == '当サイトをどこで知りましたか？') {
                    $temp_item = '';
                    foreach (unserialize($item) as $val) {
                        $temp_item .= $val."\r\n";
                    }
                    $item = rtrim($temp_item);
                }
                $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $item).$enclosure.$delim;
            }
            $csv = rtrim($csv);
            $csv .= $newline;
        }
        $csv = mb_convert_encoding($csv,'Shift-JIS','UTF-8');
        $fname = 'アンケート_'.date("ymdhi",time()).'.csv';
        force_download($fname , $csv);
    }
}
