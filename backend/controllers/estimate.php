<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'forms/EstimateForm.php';

class Estimate extends CI_Controller {

    /*=====================================================
    コンストラクタ
    =====================================================*/
    public function __construct()
    {
        parent::__construct();
        filter_auth();
        $this->load->library('pagination');
    }

    /*=====================================================
    indexページ
    =====================================================*/
    public function index()
    {
        /* タイトル
        -------------------------------------------------*/
        assign('title', '見積もり管理');
        
        /* 未送信件数取得
        -------------------------------------------------*/
        $unsent_num = $this->db
            ->where('deleted_at', NULL)
            ->where('is_sendmail', 0)
            ->get('estimates')
            ->num_rows();
        assign('unsent_num', $unsent_num);
        
        /* ページネーション設定
        -------------------------------------------------*/
        $config['base_url'] = '/__admin__/estimate/index';
        $config['per_page'] = 20;
        $config['num_links'] = 3;
        
        /* 検索
        -------------------------------------------------*/
        $word = '';
        $serch_type = '';
        if ($this->input->get('word')) {
            $word = preg_replace('/\A[\x00\s]++|[\x00\s]++\z/u', '', $this->input->get('word'));
            if ($word == '未送信') {
                $serch_type = 'unsent';
            } else {
                $serch_type = 'like';
                $config['suffix'] = '?word='.$word;
                $config['first_url'] = $config['base_url'].'?word='.$word;
            }
        }
        assign('word', $word);
        
        /* データ取得
        -------------------------------------------------*/
        $offset = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $sql_total =
            'SELECT * '.
            'FROM estimates '.
            'WHERE deleted_at IS NULL ';
        if ($serch_type == 'unsent') {
            $sql_total .= 'AND is_sendmail = 0 ';
        } else if ($serch_type == 'like') {
            $sql_total .=
                'AND ('.
                    'name LIKE "%'.$word.'%" '.
                    'OR kana LIKE "%'.$word.'%" '.
                    'OR email LIKE "%'.$word.'%"'.
                ') ';
        }
        $sql_page = $sql_total.
            'ORDER BY id DESC '.
            'LIMIT '.$config['per_page'].' '.
            'OFFSET '.$offset;
        $query_page = $this->db->query($sql_page);
        $data = $query_page->result_array();
        assign('data', $data);
        
        /* メール送信履歴取得
        -------------------------------------------------*/
        $emails = array();
        foreach ($data as $val) {
            if ($val['is_sendmail'] == '1') {
                $row = $this->db
                    ->select('id, template')
                    ->where('source_id', $val['id'])
                    ->where('type', '見積もり')
                    ->order_by('id', 'DESC')
                    ->get('emails')
                    ->row_array();
                if (!empty($row)) {
                    $emails[$val['id']] = $row;
                }
            }
        }
        assign('emails', $emails);
        
        /* ページネーション
        -------------------------------------------------*/
        $query_total = $this->db->query($sql_total);
        $config['total_rows'] = $query_total->num_rows();
        $this->pagination->initialize($config);
        assign('pager', $this->pagination->create_links());
        
        /* 描画
        -------------------------------------------------*/
        render('estimate/index');
    }
    
    /*=====================================================
    detailページ
    =====================================================*/
    public function detail($id = NULL)
    {
        /* IDチェック
        -------------------------------------------------*/
        $query = $this->db
            ->where('id', $id)
            ->get('estimates');
        if (!is_null($id)) {
            if ($query->num_rows() < 1) {
                show_404();
                return;
            }
        }
        assign('id', $id);
        
        /* タイトル
        -------------------------------------------------*/
        $title = $id == NULL ? '見積もり作成' : '見積もり詳細';
        assign('title', $title);
        
        /* JS
        -------------------------------------------------*/
        assign('scripts', array('assets/js/order.js'));
        
        /* 見積もりデータ取得
        -------------------------------------------------*/
        $estimate = $query->row_array();
        
        /* 品目データ取得
        -------------------------------------------------*/
        $items = $this->db
            ->where('estimate_id', $id)
            ->get('estimate_items')
            ->result_array();
        $i = 0;
        foreach ($items as $val) {
            if (array_key_exists($val['item'], get_item('order_items'))) {
                $estimate['item_'.$i] = $val['item'];
            } else {
                $estimate['item_'.$i] = 'その他';
                $estimate['item_etc_'.$i] = $val['item'];
            }
            $estimate['item_num_'.$i] = $val['quantity'];
            $i++;
        }
        assign('estimate', $estimate);
        
        /* 画像データ取得
        -------------------------------------------------*/
        $images = $this->db
            ->where('estimate_id', $id)
            ->get('estimate_images')
            ->result_array();
        assign('images', $images);
        
        /* フォーム生成
        -------------------------------------------------*/
        $result = '';
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $form = new EstimateForm(null, $estimate);
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new EstimateForm($_POST);
            if ($form->isValid()) {
                // 見積もり情報の登録
                $set_data = array(
                    'name' => $this->input->post('name'),
                    'kana' => $this->input->post('kana'),
                    'email' => $this->input->post('email'),
                    'tel' => $this->input->post('tel'),
                    'prefcode' => $this->input->post('prefcode'),
                    'is_expensive' => $this->input->post('is_expensive'),
                    'pickup_boxes' => $this->input->post('pickup_boxes'),
                    'remarks' => $this->input->post('remarks'),
                    'amount' => $this->input->post('amount'),
                    'is_sendmail' => $this->input->post('is_sendmail'),
                );
                // 新規作成
                if ($id == NULL) {
                    $set_data['estimate_at'] = date('Y-m-d H:i:s');
                    $ret = $this->db
                        ->set($set_data)
                        ->insert('estimates');
                    $id = $this->db->insert_id();
                    $redirect = TRUE;
                // 更新
                } else {
                    $set_data['updated_at'] = date('Y-m-d H:i:s');
                    $ret = $this->db
                        ->where('id', $id)
                        ->set($set_data)
                        ->update('estimates');
                    if ($ret) {
                        $result = '見積もり情報を更新しました';
                    } else {
                        $result = '見積もり情報の更新に失敗しました';
                    }
                }
                // 見積もり品目の登録
                $this->db
                    ->where('estimate_id', $id)
                    ->delete('estimate_items');
                $items_data = array();
                for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
                    if (strlen($this->input->post('item_'.$i)) > 0) {
                        $items_data = array(
                            'estimate_id' => $id,
                            'item' => $this->input->post('item_'.$i) == 'その他' && strlen($this->input->post('item_etc_'.$i)) > 0 ? $this->input->post('item_etc_'.$i) : $this->input->post('item_'.$i),
                            'quantity' => $this->input->post('item_num_'.$i),
                        );
                        $ret = $this->db->from('estimate_items')
                            ->set($items_data)
                            ->insert();
                    }
                }
                if ($redirect) {
                    redirect('estimate/detail/'.$id);
                }
            }
        }
        
        /* 描画
        -------------------------------------------------*/
        assign('result', $result);
        assign('estimate', $estimate);
        assign('form', $form);
        render('estimate/detail');
        return;
    }
    
    /*=====================================================
    deleteページ
    =====================================================*/
    public function delete($id)
    {
        // IDチェック
        $count = $this->db
            ->where('id', $id)
            ->get('estimates')
            ->num_rows();
        if ($count < 1) {
            die('存在しないIDです');
        }
        
        // データベース更新
        $this->db
            ->where('id', $id)
            ->set('deleted_at', date('Y-m-d H:i:s'))
            ->update('estimates');
        
        redirect('estimate');
    }
    
    /*=====================================================
    csvダウンロードページ
    =====================================================*/
    public function csv()
    {
        $this->load->dbutil();
        $this->load->helper('download');
        // 項目名
        $select = array(
            'id as "ID"',
            'estimate_at as "依頼日時"',
            'name as "名前"',
            'kana as "カナ"',
            'email as "メールアドレス"',
            'tel as "電話番号"',
            'prefcode as "県名"',
            'pickup_boxes as "依頼品サイズ"',
            'remarks as "備考"',
            'amount as "見積り金額"',
            'is_sendmail as "メール送信"',
            'updated_at as "更新日時"',
        );
        $query = $this->db->select($select)->get('estimates');
        $csv = '';
        $delim = ",";
        $newline = "\n";
        $enclosure = '"';
        foreach ($query->list_fields() as $name) {
            $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $name).$enclosure.$delim;
        }
        $csv = rtrim($csv);
        $csv .= $newline;
        foreach ($query->result_array() as $row) {
            foreach ($row as $key => $item) {
                if ($key == '県名') {
                    $item = get_item('prefcode')[$item];
                }
                $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $item).$enclosure.$delim;
            }
            $csv = rtrim($csv);
            $csv .= $newline;
        }
        $csv = mb_convert_encoding($csv,'Shift-JIS','UTF-8');
        $fname = '見積もり_'.date("ymdhi",time()).'.csv';
        force_download($fname , $csv);
    }
}
