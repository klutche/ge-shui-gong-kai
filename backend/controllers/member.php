<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Member extends CI_Controller {
    
    /*=====================================================
    コンストラクタ
    =====================================================*/
    public function __construct() {
        parent::__construct();
        filter_auth();
        $this->load->model('membermodel');
        $this->load->library('pagination');
    }
    
    /*=====================================================
    indexページ
    =====================================================*/
    public function index($type = NULL) {
        
        /* タイトル
        -------------------------------------------------*/
        $title = $type == 'resign' ? '退会者一覧' : '会員管理';
        assign('title', $title);
        assign('type', $type);
        
        /* CSS, JS
        -------------------------------------------------*/
        assign('styles', array('assets/css/jquery-ui.min.css'));
        assign('scripts', array('assets/js/libs/jquery-ui.min.js', 'assets/js/enquete.js'));
        
        /* 退会者切り替え
        -------------------------------------------------*/
        $table = $type == 'resign' ? 'resign_members' : 'members';
        
        /* ページネーション設定
        -------------------------------------------------*/
        $config['base_url'] = $type == 'resign' ? '/__admin__/member/index/resign' : '/__admin__/member/index';
        $config['per_page'] = 20;
        $config['num_links'] = 3;
        $config['uri_segment'] = $type == 'resign' ? 4 : 3;
        
        /* 検索
        -------------------------------------------------*/
        $start = '';
        $end = '';
        $word = '';
        $params = '';
        if ($this->input->get()) {
            $start = $this->input->get('start');
            $end = $this->input->get('end');
            $word = preg_replace('/\A[\x00\s]++|[\x00\s]++\z/u', '', $this->input->get('word'));
            $params = '?'.parse_url($_SERVER['REQUEST_URI'])['query'];
            $config['suffix'] = $params;
            $config['first_url'] = $config['base_url'].$params;
        }
        assign('start', $start);
        assign('end', $end);
        assign('word', $word);
        assign('params', $params);
        
        /* データ取得
        -------------------------------------------------*/
        $is_active = $type == 'resign' ? 0 : 1;
        $at = $type == 'resign' ? 'resigned_at' : 'verified_at';
        $offset = $this->uri->segment($config['uri_segment']) ? $this->uri->segment($config['uri_segment']) : 0;
        $sql_total =
            'SELECT * '.
            'FROM '.$table.' '.
            'WHERE is_active = '.$is_active.' ';
        if ($start != '') {
            $sql_total .= 'AND '.$at.' >= "'.$start.' 00:00:00" ';
        }
        if ($end != '') {
            $sql_total .= 'AND '.$at.' <= "'.$end.' 23:59:59" ';
        }
        if ($word != '') {
            $sql_total .=
                'AND ('.
                    'name LIKE "%'.$word.'%" '.
                    'OR kana LIKE "%'.$word.'%" '.
                    'OR email LIKE "%'.$word.'%"'.
                    'OR address LIKE "%'.$word.'%"'.
                ') ';
        }
        $sql_page = $sql_total.
            'ORDER BY '.$at.' DESC '.
            'LIMIT '.$config['per_page'].' '.
            'OFFSET '.$offset;
        $query_page = $this->db->query($sql_page);
        $data = $query_page->result_array();
        assign('data', $data);
        
        /* ページネーション
        -------------------------------------------------*/
        $query_total = $this->db->query($sql_total);
        $config['total_rows'] = $query_total->num_rows();
        $this->pagination->initialize($config);
        assign('pager', $this->pagination->create_links());
        assign('total', $config['total_rows']);
        
        /* 描画
        -------------------------------------------------*/
        render('member/index');
    }

    /*=====================================================
    detailページ
    =====================================================*/
    public function detail($id, $resign = '', $show_pw = false) {
        
        /* IDチェック
        -------------------------------------------------*/
        if (is_null($id)) {
            show_404();
            return;
        }
        
        /* 会員データ取得
        -------------------------------------------------*/
        if ($resign == 'resign') {
            $member = $this->db
                ->where('id', $id)
                ->get('resign_members')
                ->row();
        } else {
            $member = $this->membermodel->get($id);
        }
        
        /* 集配先データ取得
        -------------------------------------------------*/
        $this->load->model('pickupmodel');
        $pickups = $this->pickupmodel->search(array('member_id' => $id), 0, 100);
        
        /* 見積注文データ取得
        -------------------------------------------------*/
        // 注文データ取得
        $orders = $this->db
            ->select('id, estimate_id, name, is_sendmail, created_at')
            ->where('deleted_at', NULL)
            ->where('member_id', $id)
            ->order_by('created_at', 'DESC')
            ->get('orders')
            ->result_array();
        assign('orders', $orders);
        // 注文に紐付いた見積データ取得
        $estimates = array();
        foreach ($orders as $order) {
            $qyery = $this->db
                ->select('id, order_id, name, is_sendmail, created_at')
                ->where('deleted_at', NULL)
                ->where('order_id', $order['estimate_id'])
                ->get('estimates');
            if ($qyery->num_rows() > 0) {
                $estimates[$order['id']] = $qyery->row_array();
            }
        }
        assign('estimates', $estimates);
        
        /* パスワード表示
        -------------------------------------------------*/
        if ($show_pw) {
            $this->load->library('encrypt');
            echo $this->encrypt->decode($member->password);
        }
        
        /* 描画
        -------------------------------------------------*/
        assign('resign', $resign);
        assign('member', $member);
        assign('pickups', $pickups);
        assign('title', '会員情報');
        render('member/detail');
    }

    /*=====================================================
    deleteページ
    =====================================================*/
    public function delete($id) {
        $this->load->model('pickupmodel');
        $member = $this->membermodel->get($id);
        if (!$member) {
            show_404();
            return;
        }

        if ($member->is_active == 1) {
            show_error('この会員はまだ有効です。');
            return;
        }

        $this->db->trans_start();
        $this->pickupmodel->deleteByMemberId($id);
        $this->membermodel->delete($id);
        $this->db->trans_complete();

        redirect('member/resign');
    }

    /*=====================================================
    csvダウンロードページ
    =====================================================*/
    public function csv($type = NULL) {
        $this->load->dbutil();
        $this->load->helper('download');
        
        $table = $type == 'resign' ? 'resign_members' : 'members';
        $is_active = $type == 'resign' ? 0 : 1;
        $at = $type == 'resign' ? 'resigned_at' : 'verified_at';
        $start = '';
        $end = '';
        $word = '';
        if ($this->input->get()) {
            $start = $this->input->get('start');
            $end = $this->input->get('end');
            $word = preg_replace('/\A[\x00\s]++|[\x00\s]++\z/u', '', $this->input->get('word'));
        }
        $sql =
            'SELECT '.
                'id as "ID", '.
                'name as "名前", '.
                'kana as "カナ", '.
                'email as "メールアドレス", '.
                'zipcode as "郵便番号", '.
                'address as "住所", '.
                'tel as "電話番号", '.
                'remarks as "備考", '.
                'is_active as "アクティブ", '.
                'logined_at as "最終ログイン", '.
                'is_verify as "入会承認", '.
                'verified_at as "入会日時", '.
                'last_ordered_at as "最終注文日時", '.
                'resigned_at as "退会日時", '.
                'resign_reason as "退会理由", '.
                'created_at as "作成日時", '.
                'updated_at as "更新日時" '.
            'FROM '.$table.' '.
            'WHERE is_active = '.$is_active.' ';
        if ($start != '') {
            $sql .= 'AND '.$at.' >= "'.$start.' 00:00:00" ';
        }
        if ($end != '') {
            $sql .= 'AND '.$at.' <= "'.$end.' 23:59:59" ';
        }
        if ($word != '') {
            $sql .=
                'AND ('.
                    'name LIKE "%'.$word.'%" '.
                    'OR kana LIKE "%'.$word.'%" '.
                    'OR email LIKE "%'.$word.'%"'.
                    'OR address LIKE "%'.$word.'%"'.
                ') ';
        }
        $sql .=
            'ORDER BY '.$at.' DESC';
        $query = $this->db->query($sql);
        $csv = '';
        $delim = ",";
        $newline = "\n";
        $enclosure = '"';
        foreach ($query->list_fields() as $name) {
            $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $name).$enclosure.$delim;
        }
        $csv = rtrim($csv);
        $csv .= $newline;
        foreach ($query->result_array() as $row) {
            foreach ($row as $key => $item) {
                $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $item).$enclosure.$delim;
            }
            $csv = rtrim($csv);
            $csv .= $newline;
        }
        $csv = mb_convert_encoding($csv, 'Shift-JIS', 'UTF-8');
        $fname = '会員_'.date("ymdhi", time()).'.csv';
        force_download($fname, $csv);
    }
}