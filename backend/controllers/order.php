<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'forms/OrderForm.php';

class Order extends CI_Controller {

    /*=====================================================
    コンストラクタ
    =====================================================*/
    public function __construct()
    {
        parent::__construct();
        filter_auth();
        $this->load->library('pagination');
        $this->load->library('my_librarie');
    }

    /*=====================================================
    indexページ
    =====================================================*/
    public function index()
    {
        /* タイトル
        -------------------------------------------------*/
        assign('title', '注文管理');
        
        /* 未送信件数取得
        -------------------------------------------------*/
        $unsent_num = $this->db
            ->where('deleted_at', NULL)
            ->where('is_sendmail', 0)
            ->get('orders')
            ->num_rows();
        assign('unsent_num', $unsent_num);
        
        /* ページネーション設定
        -------------------------------------------------*/
        $config['base_url'] = '/__admin__/order/index';
        $config['per_page'] = 20;
        $config['num_links'] = 3;
        
        /* 検索
        -------------------------------------------------*/
        $word = '';
        $serch_type = '';
        if ($this->input->get('word')) {
            $word = preg_replace('/\A[\x00\s]++|[\x00\s]++\z/u', '', $this->input->get('word'));
            if ($word == '未送信') {
                $serch_type = 'unsent';
            } else {
                $serch_type = 'like';
                $config['suffix'] = '?word='.$word;
                $config['first_url'] = $config['base_url'].'?word='.$word;
            }
        }
        assign('word', $word);
        
        /* データ取得
        -------------------------------------------------*/
        $offset = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $sql_total =
            'SELECT * '.
            'FROM orders '.
            'WHERE deleted_at IS NULL ';
        if ($serch_type == 'unsent') {
            $sql_total .= 'AND is_sendmail = 0 ';
        } else if ($serch_type == 'like') {
            $sql_total .=
                'AND ('.
                    'name LIKE "%'.$word.'%" '.
                    'OR kana LIKE "%'.$word.'%" '.
                    'OR email LIKE "%'.$word.'%"'.
                ') ';
        }
        $sql_page = $sql_total.
            'ORDER BY id DESC '.
            'LIMIT '.$config['per_page'].' '.
            'OFFSET '.$offset;
        $query_page = $this->db->query($sql_page);
        $data = $query_page->result_array();
        assign('data', $data);
        
        /* メール送信履歴取得
        -------------------------------------------------*/
        $emails = array();
        foreach ($data as $val) {
            if ($val['is_sendmail'] == '1') {
                $row = $this->db
                    ->select('id, template')
                    ->where('source_id', $val['id'])
                    ->where('type', '注文')
                    ->order_by('id', 'DESC')
                    ->get('emails')
                    ->row_array();
                if (!empty($row)) {
                    $emails[$val['id']] = $row;
                }
            }
        }
        assign('emails', $emails);
        
        /* ページネーション
        -------------------------------------------------*/
        $query_total = $this->db->query($sql_total);
        $config['total_rows'] = $query_total->num_rows();
        $this->pagination->initialize($config);
        assign('pager', $this->pagination->create_links());
        
        /* 描画
        -------------------------------------------------*/
        render('order/index');
    }
    
    /*=====================================================
    detailページ
    =====================================================*/
    public function detail($id = NULL)
    {
        /* IDチェック
        -------------------------------------------------*/
        $query = $this->db
            ->where('id', $id)
            ->get('orders');
        if (!is_null($id)) {
            if ($query->num_rows() < 1) {
                show_404();
                return;
            }
        }
        assign('id', $id);
        
        /* タイトル
        -------------------------------------------------*/
        $title = $id == NULL ? '注文作成' : '注文詳細';
        assign('title', $title);
        
        /* JS
        -------------------------------------------------*/
        assign('scripts', array('assets/js/order.js'));
        
        /* 注文データ取得
        -------------------------------------------------*/
        $order = $query->row_array();
        assign('order', $order);
        
        /* 品目データ取得
        -------------------------------------------------*/
        $items = $this->db
            ->where('order_id', $id)
            ->get('order_items')
            ->result_array();
        $i = 0;
        foreach ($items as $val) {
            if (array_key_exists($val['item'], get_item('order_items'))) {
                $order['item_'.$i] = $val['item'];
            } else {
                $order['item_'.$i] = 'その他';
                $order['item_etc_'.$i] = $val['item'];
            }
            $order['item_num_'.$i] = $val['quantity'];
            $i++;
        }
        assign('order', $order);
        
        /* フォーム生成
        -------------------------------------------------*/
        $result = '';
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $form = new OrderForm(null, $order);
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new OrderForm($_POST);
            if ($form->isValid()) {
                $set_data = array(
                    'name' => $this->input->post('name'),
                    'kana' => $this->input->post('kana'),
                    'email' => $this->input->post('email'),
                    'tel' => $this->input->post('tel'),
                    'zipcode' => $this->input->post('zipcode'),
                    'address' => $this->input->post('address'),
                    'pickup_name' => $this->input->post('pickup_name'),
                    'pickup_tel' => $this->input->post('pickup_tel'),
                    'pickup_zipcode' => $this->input->post('pickup_zipcode'),
                    'pickup_address' => $this->input->post('pickup_address'),
                    'pickup_method' => $this->input->post('pickup_method'),
                    'pickup_date' => $this->input->post('pickup_date'),
                    'pickup_time' => $this->input->post('pickup_time'),
                    'pickup_boxes' => $this->input->post('pickup_boxes'),
                    'payment' => $this->input->post('payment'),
                    'remarks' => $this->input->post('remarks'),
                    'is_expensive' => $this->input->post('is_expensive'),
                    'amount' => $this->input->post('amount'),
                    'is_sendmail' => $this->input->post('is_sendmail'),
                );
                // 新規作成
                if ($id == NULL) {
                    $set_data['order_at'] = date('Y-m-d H:i:s');
                    $ret = $this->db
                        ->set($set_data)
                        ->insert('orders');
                    $id = $this->db->insert_id();
                    $redirect = TRUE;
                // 更新
                } else {
                    $set_data['updated_at'] = date('Y-m-d H:i:s');
                    $ret = $this->db
                        ->where('id', $id)
                        ->set($set_data)
                        ->update('orders');
                    if ($ret) {
                        $result = '注文情報を更新しました';
                    } else {
                        $result = '注文情報の更新に失敗しました';
                    }
                }
                // 見積もり品目の登録
                $this->db
                    ->where('order_id', $id)
                    ->delete('order_items');
                $items_data = array();
                for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
                    if (strlen($this->input->post('item_'.$i)) > 0) {
                        $items_data = array(
                            'order_id' => $id,
                            'item' => $this->input->post('item_'.$i) == 'その他' && strlen($this->input->post('item_etc_'.$i)) > 0 ? $this->input->post('item_etc_'.$i) : $this->input->post('item_'.$i),
                            'quantity' => $this->input->post('item_num_'.$i),
                        );
                        $ret = $this->db->from('order_items')
                            ->set($items_data)
                            ->insert();
                    }
                }
                if ($redirect) {
                    redirect('order/detail/'.$id);
                }
            }
        }
        
        /* 描画
        -------------------------------------------------*/
        assign('result', $result);
        assign('form', $form);
        render('order/detail');
        return;
    }
    
    /*=====================================================
    deleteページ
    =====================================================*/
    public function delete($id)
    {
        // IDチェック
        $count = $this->db
            ->where('id', $id)
            ->get('orders')
            ->num_rows();
        if ($count < 1) {
            die('存在しないIDです');
        }
        
        // データベース更新
        $this->db
            ->where('id', $id)
            ->set('deleted_at', date('Y-m-d H:i:s'))
            ->update('orders');
        
        redirect('order');
    }
    
    /*=====================================================
    csvダウンロードページ
    =====================================================*/
    public function csv()
    {
        $this->load->dbutil();
        $this->load->helper('download');
        // 項目名
        $select = array(
            'id as "ID"',
            'order_at as "注文日時"',
            'estimate_id as "見積もりID"',
            'member_id as "会員ID"',
            'is_expensive as "高額商品の有無"',
            'name as "名前"',
            'kana as "カナ"',
            'email as "メールアドレス"',
            'tel as "電話番号"',
            'zipcode as "郵便番号"',
            'address as "住所"',
            'pickup_name as "集配先名前"',
            'pickup_tel as "集配先電話番号"',
            'pickup_zipcode as "集配先郵便番号"',
            'pickup_address as "集配先住所"',
            'pickup_method as "集配方法"',
            'pickup_date as "集配日"',
            'pickup_time as "集配時間"',
            'pickup_date_exclude as "集配日除外"',
            'pickup_boxes as "集配ボックス"',
            'payment as "入金方法"',
            'remarks as "備考"',
            'amount as "見積り金額"',
            'is_sendmail as "メール送信"',
            'created_at as "作成日時"',
            'updated_at as "更新日時"',
            'deleted_at as "削除日時"',
        );
        $query = $this->db->select($select)->get('orders');
        $csv = '';
        $delim = ",";
        $newline = "\n";
        $enclosure = '"';
        foreach ($query->list_fields() as $name) {
            $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $name).$enclosure.$delim;
        }
        $csv = rtrim($csv);
        $csv .= $newline;
        foreach ($query->result_array() as $row) {
            foreach ($row as $key => $item) {
                $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $item).$enclosure.$delim;
            }
            $csv = rtrim($csv);
            $csv .= $newline;
        }
        $csv = mb_convert_encoding($csv,'Shift-JIS','UTF-8');
        $fname = '注文_'.date("ymdhi",time()).'.csv';
        force_download($fname , $csv);
    }
}
