<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Route extends CI_Controller {

    /*=====================================================
    コンストラクタ
    =====================================================*/
    public function __construct()
    {
        parent::__construct();
        filter_auth();
        $this->load->library('pagination');
    }

    /*=====================================================
    indexページ
    =====================================================*/
    public function index($type = 'orders')
    {
        /* タイトル
        -------------------------------------------------*/
        assign('title', '見積もり注文経路一覧');
        assign('type', $type);
        
        /* ページネーション設定
        -------------------------------------------------*/
        $config['per_page'] = 20;
        $config['num_links'] = 3;
        
        /* データ取得
        -------------------------------------------------*/
        if ($type == 'orders_only') {
            // 注文のみのデータ
            $result_array = $this->db
                ->select('id, estimate_id, name, is_sendmail, created_at')
                ->where('deleted_at', NULL)
                ->where('estimate_id', NULL)
                ->order_by('created_at', 'DESC')
                ->get('orders')
                ->result_array();
            $data_type = '注文';
            $config['base_url'] = '/__admin__/route/index/orders_only';
        } else if ($type == 'estimates_only') {
            // 見積のみのデータ
            $result_array = $this->db
                ->select('id, order_id, name, is_sendmail, created_at')
                ->where('deleted_at', NULL)
                ->where('order_id', NULL)
                ->order_by('created_at', 'DESC')
                ->get('estimates')
                ->result_array();
            $data_type = '見積';
            $config['base_url'] = '/__admin__/route/index/estimates_only';
        } else if ($type == 'estimates_orders') {
            // 見積もり後注文のデータ
            $result_array = $this->db
                ->select('id, estimate_id, name, is_sendmail, created_at')
                ->where('deleted_at', NULL)
                ->where('estimate_id IS NOT NULL', null, false)
                ->order_by('created_at', 'DESC')
                ->get('orders')
                ->result_array();
            $data_type = '注文';
            $config['base_url'] = '/__admin__/route/index/estimates_orders';
        } else {
            // 注文全件のデータ
            $result_array = $this->db
                ->select('id, estimate_id, name, is_sendmail, created_at')
                ->where('deleted_at', NULL)
                ->order_by('created_at', 'DESC')
                ->get('orders')
                ->result_array();
            $data_type = '注文';
            $config['base_url'] = '/__admin__/route/index/orders';
        }
        assign('data_type', $data_type);
        // 注文IDを持つ見積データ
        $sub_estimates = array();
        $estimates_array = $this->db
            ->select('id, order_id, name, is_sendmail, created_at')
            ->where('deleted_at', NULL)
            ->where('order_id IS NOT NULL', null, false)
            ->order_by('created_at', 'DESC')
            ->get('estimates')
            ->result_array();
        foreach ($estimates_array as $val) {
            $sub_estimates[$val['order_id']] = $val;
        }
        assign('sub_estimates', $sub_estimates);

        /* ページネーション
        -------------------------------------------------*/
        $config['total_rows'] = count($result_array);
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        assign('pager', $this->pagination->create_links());
        
        /* 配列から1ページ分のデータを抽出
        -------------------------------------------------*/
        $data = array_slice($result_array, $this->uri->segment(4), $config['per_page']);
        assign('data', $data);
        
        /* 描画
        -------------------------------------------------*/
        render('route/index');
    }
    
    /*=====================================================
    csvダウンロードページ
    =====================================================*/
    public function csv()
    {
        $this->load->dbutil();
        $this->load->helper('download');
        // 項目名
        $select = array(
            'id as "ID"',
            'order_at as "注文日時"',
            'estimate_id as "見積もりID"',
            'member_id as "会員ID"',
            'is_expensive as "高額商品の有無"',
            'name as "名前"',
            'kana as "カナ"',
            'email as "メールアドレス"',
            'tel as "電話番号"',
            'zipcode as "郵便番号"',
            'address as "住所"',
            'pickup_name as "集配先名前"',
            'pickup_tel as "集配先電話番号"',
            'pickup_zipcode as "集配先郵便番号"',
            'pickup_address as "集配先住所"',
            'pickup_method as "集配方法"',
            'pickup_date as "集配日"',
            'pickup_time as "集配時間"',
            'pickup_date_exclude as "集配日除外"',
            'pickup_boxes as "集配ボックス"',
            'payment as "入金方法"',
            'remarks as "備考"',
            'amount as "見積り金額"',
            'is_sendmail as "メール送信"',
            'created_at as "作成日時"',
            'updated_at as "更新日時"',
            'deleted_at as "削除日時"',
        );
        $query = $this->db->select($select)->get('orders');
        $csv = '';
        $delim = ",";
        $newline = "\n";
        $enclosure = '"';
        foreach ($query->list_fields() as $name) {
            $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $name).$enclosure.$delim;
        }
        $csv = rtrim($csv);
        $csv .= $newline;
        foreach ($query->result_array() as $row) {
            foreach ($row as $key => $item) {
                $csv .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $item).$enclosure.$delim;
            }
            $csv = rtrim($csv);
            $csv .= $newline;
        }
        $csv = mb_convert_encoding($csv,'Shift-JIS','UTF-8');
        $fname = '注文_'.date("ymdhi",time()).'.csv';
        force_download($fname , $csv);
    }
}
