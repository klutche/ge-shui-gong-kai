<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'vendor/autoload.php';
require_once APPPATH.'forms/SendForm.php';

class Send extends CI_Controller {

    /*=====================================================
    コンストラクタ
    =====================================================*/
    public function __construct()
    {
        parent::__construct();
        filter_auth();
    }
    
    /*=====================================================
    indexページ
    =====================================================*/
    public function index($table = 'estimates', $id = null, $template_num = '1')
    {
        /* タイトル
        -------------------------------------------------*/
        assign('title', 'メール送信');
        
        /* JS CSS
        -------------------------------------------------*/
        assign('styles', array('assets/css/lite-editor.css'));
        assign('scripts', array(
            'assets/js/libs/lite-editor.js',
            'assets/js/send.js',
        ));
        
        /* パラメータのチェック
        -------------------------------------------------*/
        if (!isset($table) || !isset($id) || strlen($table) == 0 || strlen($id) == 0) {
            die('URLの形式が正しくありません');
        }
        if ($table != 'estimates' && $table != 'orders') {
            die('データベースの指定に誤りがあります');
        }
        $query = $this->db
            ->where('id', $id)
            ->get($table);
        if ($query->num_rows() < 1) {
            die('データが存在しません');
        }
        assign('id', $id);
        assign('table', $table);
        
        /* 見積もり・受注分岐
        -------------------------------------------------*/
        if ($table == 'estimates') {
            $type = '見積もり';
            $prefix = 'estimate';
            $email_template = array(
                '1' => '見積もり',
            );
            $email_subject_array = array(
                '1' => '革水です：見積もり依頼ありがとうございます',
            );
        } else if ($table == 'orders') {
            $type = '注文';
            $prefix = 'order';
            $email_template = array(
                '1' => '注文受付',
                '2' => '依頼品到着',
                '3' => '見積もり',
                '4' => '作業開始',
                '5' => 'ご入金確認及びカード決済確認',
                '6' => 'クリーニング完了',
                '7' => '発送完了',
                '8' => '注文キャンセル',
            );
            $email_subject_array = array(
                '1' => '革水です：ご注文ありがとうございます',
                '2' => '革水です：依頼品到着のお知らせ',
                '3' => '革水です：お見積もりのお知らせ',
                '4' => '革水です：作業開始のお知らせ',
                '5' => '革水です：ご入金確認のご連絡',
                '6' => '革水です：クリーニング完了のお知らせ',
                '7' => '革水です：発送完了のお知らせ',
                '8' => '革水です：ご注文キャンセル受付',
            );
        }
        if (!array_key_exists($template_num, $email_template)) {
            die('メールテンプレートの指定に誤りがあります');
        }
        // メールタイトル
        $email_subject = $email_subject_array[$template_num];
        assign('type', $type);
        assign('prefix', $prefix);
        assign('email_template', $email_template);
        assign('email_subject', $email_subject);
        assign('template_num', $template_num);
        
        /* データ取得
        -------------------------------------------------*/
        // 見積・注文データ取得
        $data = $query->row_array();
        assign('data', $data);
        // 品目データ取得
        $items = $this->db
            ->where($prefix.'_id', $id)
            ->get($prefix.'_items')
            ->result_array();
        assign('items', $items);
        // 画像データ取得
        $images = 0;
        if ($table == 'estimates') {
            $images = $this->db
                ->where('estimate_id', $id)
                ->get('estimate_images')
                ->num_rows();
        }
        assign('images', $images);
        
        /* フォーム生成
        -------------------------------------------------*/
        $result = '';
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $form_amount = new AmountForm(null, $data);
            assign('result', $result);
            assign('form_amount', $form_amount);
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            // 見積もり金額を更新
            if ($this->input->post('amount')) {
                $form_amount = new AmountForm($_POST);
                if ($form_amount->isValid()) {
                    // 見積もり金額を更新
                    $set_data = array(
                        'amount' => $this->input->post('amount'),
                    );
                    $ret = $this->db
                        ->where('id', $id)
                        ->set($set_data)
                        ->update($table);
                    if ($ret) {
                        $result = '見積もり金額を更新しました';
                    }
                }
            }
            // メール送信
            if ($this->input->post('send')) {
                $form_amount = new AmountForm(null, $data);
                // データベース登録
                $set_data = array(
                    'type' => $type,
                    'template' => $email_template[$template_num],
                    'source_id' => $id,
                    'mailto' => $data['email'],
                    'name' => $data['name'],
                    'subject' => $email_subject,
                    'content' => trim(preg_replace( "~\x{00a0}~siu", " ", $this->input->post('content') )),
                );
                $this->db
                    ->set($set_data)
                    ->insert('emails');
                // 送信
                $email = new \SendGrid\Mail\Mail();
                $email->setFrom(get_item('email_sender'));
                $email->setSubject($email_subject);
                $email->addTo($data['email']);
                $email->addContent('text/html', trim(preg_replace( "~\x{00a0}~siu", " ", $this->input->post('content') )));
//                $email->addContent('text/html', $this->input->post('content'));
                $tracking_settings = new \SendGrid\Mail\TrackingSettings();
                $tracking_settings->setOpenTracking(
                    new \SendGrid\Mail\OpenTracking(false, null)
                );
                $email->setTrackingSettings($tracking_settings);
                $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
                $sendgrid->send($email);
                // データベース更新
                $set_data = array(
                    'is_sendmail' => '1',
                );
                $ret = $this->db
                    ->where('id', $id)
                    ->set($set_data)
                    ->update($table);
                $result = 'メールを送信しました';
            }
            // データ再取得
            $data = $this->db
                ->where('id', $id)
                ->get($table)
                ->row_array();
            assign('data', $data);
            assign('result', $result);
            assign('form_amount', $form_amount);
        }
        render('send/index');
        return;
    }
}
