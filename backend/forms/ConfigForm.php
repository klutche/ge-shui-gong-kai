<?php

require_once APPPATH . 'libraries/Form.php';

class ConfigForm extends Form
{
    public function config($data=NULL, $init=NULL, $opts=NULL)
    {

        $ci =& get_instance();

        $this->addField('key',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => 'Key',
                'rules' => 'normalize|max_length[50]|xss_clean',
                'extra' => array(
                    'class' => 'input-large',
                    'maxlength' => 50))));

        $this->addField('label',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => 'Label',
                'rules' => 'normalize|max_length[50]|xss_clean',
                'extra' => array(
                    'class' => 'input-large',
                    'maxlength' => 50))));

        $this->addField('value',
            new Form_TextareaField(array(
                'required' => true,
                'label' => 'Value',
                'rules' => 'normalize|max_length[100000]|xss_clean',
                'extra' => array(
                    'class' => 'input-xxlarge',
                    'cols' => 50,
                    'rows' => 10))));

        $this->addField('encrypt',
            new Form_CheckboxField(array(
                'required' => false,
                'label' => '暗号化する?',
                'rules' => '',
                'choices' => array(1 => 'はい'))));

        $this->addField('php',
            new Form_CheckboxField(array(
                'required' => false,
                'label' => 'PHPのコードとして評価する?',
                'rules' => '',
                'choices' => array(1 => 'はい'))));

    }

}


