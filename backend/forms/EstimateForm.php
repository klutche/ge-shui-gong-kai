<?php
require_once APPPATH . 'libraries/Form.php';

class EstimateForm extends Form
{
    public function config($data = NULL, $init = NULL, $opts = NULL) {
        $this->addField('name', new Form_TextField(array(
            'required' => TRUE,
            'label' => 'お名前',
            'rules' => 'normalize|max_length[50]|xss_clean',
            'extra' => array(
                'class' => 'input-large',
                'maxlength' => 50
            )
        )));
        
        $this->addField('kana', new Form_TextField(array(
            'required' => TRUE,
            'label' => 'フリガナ',
            'rules' => 'normalize|max_length[50]|xss_clean',
            'extra' => array(
                'class' => 'input-large',
                'maxlength' => 50
            )
        )));
        
        $this->addField('email', new Form_TextField(array(
            'required' => TRUE,
            'label' => 'メールアドレス',
            'rules' => 'normalize|valid_email',
            'extra' => array(
                'class' => 'input-large'
            )
        )));
        
        $this->addField('tel', new Form_TextField(array(
            'required' => TRUE,
            'label' => '電話番号',
            'rules' => 'normalize|valid_tel',
            'extra' => array(
                'class' => 'input-large',
                'maxlength' => 13
            )
        )));
        
        $this->addField('prefcode', new Form_DropdownField(array(
            'required' => TRUE,
            'label' => '集配先',
            'choices' => get_item('prefcode'),
        )));
        
        for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
            $this->addField('item_'.$i, new Form_DropdownField(array(
                'required' => ($i == 0 ? true : false),
                'label' => '依頼品('.($i+1).')',
                'rules' => 'normalize|max_length[50]',
                'choices' => get_item('order_items'),
                'extra' => array(
                    'class' => 'item'
                )
            )));
            $style = 'display: none';
            if ((isset($init['item_'.$i]) && $init['item_'.$i] == 'その他') || (isset($data['item_'.$i]) && $data['item_'.$i] == 'その他')) {
                $style = '';
            }
            $this->addField('item_etc_'.$i, new Form_TextField(array(
                'required' => false,
                'label' => '依頼品その他('.($i+1).')',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'class' => 'item-etc',
                    'style' => $style,
                    'maxlength' => 50,
                    'placeholder' => 'ブランド・アイテム名・素材'
                )
            )));
            $opt = get_item('order_item_nums');
            if ($i == 0) {
                unset($opt[0]);
            }
            $this->addField('item_num_'.$i, new Form_DropdownField(array(
                'required' => ($i == 0 ? true : false),
                'label' => '依頼品数('.($i+1).')',
                'rules' => 'normalize|max_length[2]|numeric|check_item_num[item_'.$i.']',
                'choices' => $opt,
                'extra' => array(
                    'class' => 'input-mini'
                )
            )));
        }
        
        $this->addField('is_expensive', new Form_CheckboxField(array(
            'required' => false,
            'label' => '高級品の有無',
            'rules' => 'normalize|max_length[20]',
            'choices' => array(
                1 => ''
            )
        )));
        
        $this->addField('pickup_boxes', new Form_RadioField(array(
            'required' => true,
            'label' => '依頼品のサイズ',
            'initial' => 'バッグ以外',
            'rules' => 'normalize|max_length[50]',
            'choices' => get_item('pickup_size'),
        )));
        
        $this->addField('remarks', new Form_TextareaField(array(
            'required' => false,
            'label' => 'ご要望・備考',
            'rules' => 'normalize|max_length[2000]',
            'extra' => array(
                'class' => 'input-xxlarge',
                'row' => 5,
                'placeholder' => '汚れの状態や原因を分る範囲でご記入ください。'
            )
        )));
        
        $this->addField('amount', new Form_TextField(array(
            'required' => false,
            'label' => '見積り金額',
            'rules' => 'normalize|max_length[10]',
            'prefix' => '￥',
            'extra' => array(
                'maxlength' => 10,
                'class' => 'input-small'
            )
        )));
        
        $this->addField('is_sendmail', new Form_CheckboxField(array(
            'required' => false,
            'label' => '見積りメール送信',
            'rules' => 'normalize|max_length[20]',
            'choices' => array(
                1 => '<span>送信済み</span>'
            )
        )));
    }
}