<?php
require_once APPPATH . 'libraries/Form.php';

class OrderForm extends Form
{
    public function config($data=NULL, $init=NULL, $opts=NULL)
    {
        $this->addField('name', new Form_TextField(array(
            'required' => TRUE,
            'label' => 'お名前',
            'rules' => 'normalize|max_length[50]|xss_clean',
            'extra' => array(
                'class' => 'input-large',
                'maxlength' => 50
            )
        )));
        
        $this->addField('kana', new Form_TextField(array(
            'required' => TRUE,
            'label' => 'フリガナ',
            'rules' => 'normalize|max_length[50]|xss_clean',
            'extra' => array(
                'class' => 'input-large',
                'maxlength' => 50
            )
        )));
        
        $this->addField('email', new Form_TextField(array(
            'required' => TRUE,
            'label' => 'メールアドレス',
            'rules' => 'normalize|valid_email',
            'extra' => array(
                'class' => 'input-large',
                'maxlength' => 40
            )
        )));
        
        $this->addField('tel', new Form_TextField(array(
            'required' => TRUE,
            'label' => '電話番号',
            'rules' => 'normalize|valid_tel',
            'extra' => array(
                'class' => 'input-large',
                'maxlength' => 13
            )
        )));
        
        $this->addField('zipcode', new Form_TextField(array(
            'required' => true,
            'label' => '郵便番号',
            'rules' => 'normalize|max_length[10]',
            'prefix' => '〒',
            'extra' => array(
                'maxlength' => 10,
                'class' => 'input-small'
            )
        )));
        
        $this->addField('address', new Form_TextField(array(
            'required' => true,
            'label' => '住所',
            'rules' => 'normalize|max_length[200]',
            'extra' => array(
                'maxlength' => 200,
                'class' => 'input-xxlarge'
            )
        )));
        
        $this->addField('pickup_name', new Form_TextField(array(
            'required' => TRUE,
            'label' => '集配先お名前',
            'rules' => 'normalize|max_length[50]|xss_clean',
            'extra' => array(
                'class' => 'input-large',
                'maxlength' => 50
            )
        )));
        
        $this->addField('pickup_tel', new Form_TextField(array(
            'required' => TRUE,
            'label' => '集配先電話番号',
            'rules' => 'normalize|valid_tel',
            'extra' => array(
                'class' => 'input-large',
                'maxlength' => 13
            )
        )));
        
        $this->addField('pickup_zipcode', new Form_TextField(array(
            'required' => true,
            'label' => '集配先郵便番号',
            'rules' => 'normalize|max_length[10]',
            'prefix' => '〒',
            'extra' => array(
                'maxlength' => 10,
                'class' => 'input-small'
            )
        )));
        
        $this->addField('pickup_address', new Form_TextField(array(
            'required' => true,
            'label' => '集配先住所',
            'rules' => 'normalize|max_length[200]',
            'extra' => array(
                'maxlength' => 200,
                'class' => 'input-xxlarge'
            )
        )));
        
        $this->addField('pickup_method', new Form_RadioField(array(
            'required' => true,
            'label' => '集配方法',
            'initial' => 'ヤマト運輸による集配',
            'rules' => 'normalize',
            'choices' => get_item('pickup_methods')
        )));
        
        $this->addField('pickup_date', new Form_TextField(array(
            'required' => true,
            'label' => 'ご希望の集荷日',
            'rules' => 'normalize|max_length[100]',
            'extra' => array(
                'maxlength' => 100,
                'class' => 'input-large'
            )
        )));
        
        $this->addField('pickup_time', new Form_DropdownField(array(
            'required' => true,
            'label' => 'ご希望の集荷時間',
            'rules' => 'normalize',
            'choices' => get_item('pickup_times')
        )));
        
        $this->addField('pickup_boxes', new Form_RadioField(array(
            'required' => true,
            'label' => '梱包箱',
            'initial' => '希望しない',
            'rules' => 'normalize|max_length[50]',
            'choices' => get_item('pickup_boxes')
        )));
        
        $this->addField('payment', new Form_RadioField(array(
            'required' => true,
            'label' => 'お支払い方法',
            'rules' => 'normalize',
            'initial' => 'Paypal決済',
            'choices' => get_item('payments')
        )));
        
        for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
            $this->addField('item_'.$i, new Form_DropdownField(array(
                'required' => ($i == 0 ? true : false),
                'label' => '依頼品('.($i+1).')',
                'rules' => 'normalize|max_length[50]',
                'choices' => get_item('order_items'),
                'extra' => array(
                    'class' => 'item'
                )
            )));
            $style = 'display: none';
            if ((isset($init['item_'.$i]) && $init['item_'.$i] == 'その他') || (isset($data['item_'.$i]) && $data['item_'.$i] == 'その他')) {
                $style = '';
            }
            $this->addField('item_etc_'.$i, new Form_TextField(array(
                'required' => false,
                'label' => '依頼品その他('.($i+1).')',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'class' => 'item-etc',
                    'style' => $style,
                    'maxlength' => 50,
                    'placeholder' => 'ブランド・アイテム名・素材'
                )
            )));
            $opt = get_item('order_item_nums');
            if ($i == 0) {
                unset($opt[0]);
            }
            $this->addField('item_num_'.$i, new Form_DropdownField(array(
                'required' => ($i == 0 ? true : false),
                'label' => '依頼品数('.($i+1).')',
                'rules' => 'normalize|max_length[2]|numeric|check_item_num[item_'.$i.']',
                'choices' => $opt,
                'extra' => array(
                    'class' => 'input-mini'
                )
            )));
        }
        
        $this->addField('is_expensive', new Form_CheckboxField(array(
            'required' => false,
            'label' => '高級品の有無',
            'rules' => 'normalize|max_length[20]',
            'choices' => array(
                1 => ''
            )
        )));
        
        $this->addField('remarks', new Form_TextareaField(array(
            'required' => false,
            'label' => 'ご要望・備考',
            'rules' => 'normalize|max_length[2000]',
            'extra' => array(
                'class' => 'input-xxlarge',
                'row' => 5,
                'placeholder' => '汚れの状態や原因を分る範囲でご記入ください。'
            )
        )));
        
        $this->addField('amount', new Form_TextField(array(
            'required' => false,
            'label' => '見積り金額',
            'rules' => 'normalize|max_length[10]',
            'prefix' => '￥',
            'extra' => array(
                'maxlength' => 10,
                'class' => 'input-small'
            )
        )));
        
        $this->addField('is_sendmail', new Form_CheckboxField(array(
            'required' => false,
            'label' => '見積りメール送信',
            'rules' => 'normalize|max_length[20]',
            'choices' => array(
                1 => '<span>送信済み</span>'
            )
        )));
    }
}