<?php
require_once APPPATH . 'libraries/Form.php';

class AmountForm extends Form
{
    public function config($data = NULL, $init = NULL, $opts = NULL)
    {
        $this->addField('amount', new Form_TextField(array(
            'required' => TRUE,
            'label' => '見積もり金額',
            'rules' => 'normalize|max_length[10]|integer',
            'prefix' => '￥',
            'extra' => array(
                'maxlength' => 10,
                'class' => 'input-small'
            )
        )));
    }
}
