<?php

if (!function_exists('filter_auth')) {
    function filter_auth()
    {
        $ci =& get_instance();
        if (!$ci->session->userdata('logged_in')) {
            redirect('auth/login');
            exit;
        }
    }
}
