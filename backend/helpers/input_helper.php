<?php

// 全角英数字を半角英数字、半角カタカナを全角カタカナに変換
function normalize($str)
{
    if (is_array($str)) {
        foreach ($str as $k => $v) {
            $str[$k] = normalize($v);
        }
    }

    $str = trim(mb_convert_kana($str, 'KVas'));
    return $str;
}

// ひらがなに変換する
function hconv($str)
{
    if (is_array($str)) {
        foreach ($str as $k => $v) {
            $str[$k] = hconv($v);
        }
    }

    $str = mb_convert_kana($str, 'HVc');
    return $str;

}

// カタカナに変換する
function kconv($str)
{
    if (is_array($str)) {
        foreach ($str as $k => $v) {
            $str[$k] = hconv($v);
        }
    }

    $str = mb_convert_kana($str, 'KVC');
    return $str;

}

if (!function_exists('getallheaders')) {
    function getallheaders() {
        $out = array();
        foreach ($_SERVER as $key => $val) {
            if (substr($key, 0, 5) == 'HTTP_') {
                $key = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($key, 5)))));
                $out[strtolower($key)] = $val;
            } else {
                $out[strtolower($key)] = $val;
            }
        }
        return $out;
    }
}
