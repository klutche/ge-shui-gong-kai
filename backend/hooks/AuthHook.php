<?php

class AuthHook
{
    private $exclude = array(
        'auth:login',
        'auth:check',
        'command:*',
        'error:*'
    );


    public function check()
    {
        $ci =& get_instance();

        if (!$ci->session->userdata('logged_in')) {
            $class = $ci->router->class;
            $method = $ci->router->method;
            $class_method = $class . ':' . $method;
            $class_any = $class . ':' . '*';

            $is_redirect = TRUE;
            $redirect_url = 'login';

            if (in_array($class_method, $this->exclude)) {
                $is_redirect = FALSE;
            }

            if (in_array($class_any, $this->exclude)) {
                $is_redirect = FALSE;
            }


            if ($is_redirect) {
                redirect($redirect_url);
            }
        }
    }
}
