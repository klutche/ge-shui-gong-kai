<?php

class SettingHook
{
    public function init()
    {
        $ci =& get_instance();

        // Form
        Form::$normal_row = "<div class=\"control-group\" id=\"%s\">\n%s%s<div class=\"controls\" %s>\n%s\n%s\n</div></div>\n\n";
        Form::$required_html = "<span class=\"required label label-important\">必須</span>\n";
        Form::$error_row = "<p class=\"alert alert-error\">%s</p>";
        Form::$help_text_html = '%s';

        // Cache
        $ci->load->driver('cache',
            array(
                'adapter' => 'dummy',
            )
        );
    }

}
