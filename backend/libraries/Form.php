<?php

class Form implements Iterator, ArrayAccess
{
    public $fields = array();
    public $data = array();
    public $cleaned_data = array();
    public $errors = array();
    public $is_bound = FALSE;
    public $id_fields = 'id_%s';

    protected $is_valid = NULL;

    public $form_validation;

    public $hidden = '';

    public static $normal_row = "<div class=\"control-group\" id=\"%s\">\n%s%s<div class=\"controls\" %s>\n%s\n</div>%s\n</div>\n\n";
    public static $error_row = "<p class=\"alert\"><a class=\"close\" data-dismiss=\"alert\">×</a>%s</p>";
    public static $required_html = "<span class=\"required\">【必須】</span>\n";
    public static $help_text_html = "\n<div class=\"help_text\">%s</div>";

    protected $global_xss_filtering;
    protected $master_data = array();

    public function __construct($data=NULL, $initial=NULL, $opts=NULL)
    {
        $this->config($data, $initial, $opts);

        $ci =& get_instance();
        $ci->load->library('form_validation');
        $ci->load->helper('security');
        $this->form_validation = $ci->form_validation;
        $this->global_xss_filtering = $ci->config->item('global_xss_filtering');
        $this->form_validation->set_error_delimiters('', '');

        if ($data !== NULL) {
            if ($this->global_xss_filtering) {
                $this->data = $this->recursive_htmlentity_decode($data);
            } else {
                $this->data = $data;
            }
            $this->is_bound = TRUE;
        }

        if ($initial !== NULL) {
            $this->setInitial($initial);
        }
    }

    public function config($data, $initial, $opts=NULL)
    {
    }

    public function recursive_htmlentity_decode($str)
    {
        if (is_array($str)) {
            foreach ($str as $k => $v) {
                $str[$k] = $this->recursive_htmlentity_decode($v);
            }
        } else {
            $str = html_entity_decode($str, ENT_QUOTES);
        }
        return $str;
    }

    public function addField($name, $field)
    {
        $field->name = $name;
        $field->form = $this;
        if ($field->required && $field->func != 'form_upload') {
            if (strlen($field->rules)) {
                $field->rules .= '|required';
            } else {
                $field->rules = 'required';
            }
        }
        $this->fields[$name] = $field;
    }

    protected function outputHtml($normal_row, $error_row, $row_ender,
                                  $help_text_html, $error_on_separate_row)
    {

    }

    public function render()
    {
        $out = '';
        $this->hidden = '';
        $count = 0;

        foreach ($this->fields as $name => $field) {

            $error = isset($this->errors[$name]) ? $this->errors[$name] : NULL;

            if ($error) {
                $error = sprintf(self::$error_row, $error);
            } else {
                $error = '';
            }

            if ($field->help_text) {
                $help_text = sprintf(self::$help_text_html, $field->help_text);
            } else {
                $help_text = '';
            }

            $label = $field->labelTag(NULL, array(), $field->required);

            if (in_array($field->func, array('form_radio', 'form_checkbox'))) {
                $class = ' class="col"';
            } else {
                $class = '';
            }

            if (!$field->is_hidden && !$field->is_empty) {
                $out .= sprintf(self::$normal_row,
                                sprintf('%s-field-%d', strtolower(get_class($this)), $count++),
                                $label,
                                $error,
                                $class,
                                $field->prefix . $field->render($field->getRenderParams()) . $field->suffix,
                                $help_text
                                );
            }

        }

        return $out;
    }

    public function isValid()
    {
        if ($this->is_bound === FALSE) {
            $this->is_valid = FALSE;
            return $this->is_valid;
        }

        $this->is_valid = FALSE;
        $this->cleaned_data = array();

        foreach ($this->fields as $field) {
            $this->form_validation->set_rules(($field->is_multiple ? $field->name . '[]' : $field->name),
                                              $field->label,
                                              $field->rules);
        }

        $this->is_valid = $this->form_validation->run();

        if (!$this->is_valid) {
            foreach ($this->fields as $name => $field) {
                if ($field->is_multiple) {
                    $error = form_error($name . '[]');
                } else {
                    $error = form_error($name);
                }

                if (strlen($error)) {
                    $this->errors[$name] = $error;
                    if (!isset($this->errors['__all__'])) {
                        $this->errors['__all__'] = array();
                    }

                    $this->errors['__all__'][] = $error;
                }
            }

        }

        foreach ($this->fields as $name => &$field) {
            if (isset($this->data[$name])) {
                /*
                if ($field->is_multiple === TRUE) {
                    $value = $this->form_validation->_field_data[$name . '[]']['postdata'];
                    foreach ($value as $k => $v) {
                        $value[$k] = form_prep($v);
                    }
                } else {
                    $value = set_value($name);
                }
                 */
                if ($this->global_xss_filtering) {
                    $value = $this->recursive_htmlentity_decode($this->data[$name]);
                } else {
                    $value = $this->data[$name];
                }

                $rules = explode('|', $field->rules);
                foreach ($rules as $rule) {
                    if (function_exists($rule)) {
                        $value = $rule($value);
                    }
                }

                $this->cleaned_data[$name] = $value;
                $field->setValue($value);
            }
        }

        return $this->is_valid;
    }

    public function preview()
    {
        foreach ($this->fields as $name => $field) {
            $this->fields[$name] = new Form_PreviewField($this->fields[$name]);
        }
    }

    public function setInitial($data)
    {
        if (is_object($data)) {
            $data = (array) $data;
        }

        foreach ($this->fields as $name => &$field) {
            if (isset($data[$name])) {
                $field->setInitial($data[$name]);
            }
        }
    }

    public function clean()
    {
        return $this->is_bound ? $this->cleaned_data : array();
    }

    public function current()
    {
        $field = current($this->fields);
        return new Form_ProxyField($this, $field->name);
    }

    public function key()
    {
        return key($this->fields);
    }

    public function next()
    {
        next($this->fields);
    }

    public function rewind()
    {
        reset($this->fields);
    }

    public function valid()
    {
        return (current($this->fields) !== FALSE);
    }

    public function offsetUnset($index)
    {
        unset($this->fields[$index]);
    }

    public function offsetSet($index, $value)
    {
        $this->addField($index, $value);
    }

    public function offsetGet($index)
    {
        if (!isset($this->fields[$index])) {
            throw new Exception('Undefined index: ' . $index);
        }

        return new Form_ProxyField($this, $index);
    }

    public function offsetExists($index)
    {
        return isset($this->fields[$index]);
    }

    public function __get($key)
    {
        if ($key == 'errorlist') {
            if (isset($this->errors['__all__'])) {
                return $this->errors['__all__'];
            } else {
                return array();
            }
        }

        if ($key == 'hiddenfields') {
            $out = '';
            foreach ($this->fields as $name => $field) {
                if ($field->is_hidden && !$field->is_empty) {
                    $out .= $field->render(array());
                }
            }
            return $out;
        }

        return new Form_ProxyField($this, $key);
    }

    public function __toString()
    {
        return $this->render();
    }
}

class Form_Field
{
    public $name = '';
    public $func = 'form_input';
    public $label = '';
    public $required = FALSE;
    public $help_text = '';
    public $initial = '';
    public $choices = NULL;
    public $value = '';
    public $extra = array();
    public $prefix = '';
    public $suffix = '';

    public $is_multiple = FALSE;
    public $is_hidden = FALSE;
    public $is_empty = FALSE;

    public $rules = '';

    public $form;

    protected $empty_values = array('', array(), NULL);

    protected $id_suffix = '';

    public function __construct($params=array())
    {
        $default = array();
        foreach ($params as $key => $val) {
            $default[$key] = $this->$key;
        }

        $m = array_merge($default, $params);

        foreach ($params as $key => $val) {
            $this->$key = $m[$key];
        }
    }

    public function setForm($form)
    {
        $this->form = $form;
    }

    public function getRenderParams()
    {
        $default = $this->getDefaultParams();
        $params = array_merge($default, $this->extra);

        $params['name'] = $this->name;
        $params['id'] = isset($this->extra['id']) ? $this->extra['id'] : $this->autoId();
        return $params;
    }

    protected function getDefaultParams()
    {
        return array('size' => 20,
                     'maxlength' => 20,
                     'value' => !in_array($this->value, $this->empty_values)
                                ? $this->value : $this->initial);
    }

    public function labelTag($contents=NULL, $attrs=array(), $is_required = FALSE)
    {
        $contents = $contents ? $contents : $this->label;
        $id = isset($this->extra['id']) ? $this->extra['id'] : $this->autoId();
        $tmp = array();
        foreach ($attrs as $key => $val) {
            $tmp[] = sprintf('%s="$s"', $key, $val);
        }

        if (count($tmp)) {
            $attrs = ' ' . implode(' ', $tmp);
        } else {
            $attrs = '';
        }

        $required = '';
        if ($is_required) {
            $required = Form::$required_html;
        }

        return sprintf('<label class="control-label" for="%s"%s>%s%s</label>',
                       $this->idForLabel($id), $attrs, $contents, $required);

    }

    public function autoId()
    {
        $id_fields = $this->form->id_fields;
        $name = strtr($this->name, array('[]' => ''));

        if (strpos($id_fields, '%s') !== FALSE) {
            return sprintf($id_fields, $name) . $this->id_suffix;
        } elseif ($id_fields) {
            return $name . $this->id_suffix;
        }

        return '';
    }

    public function idForLabel($id)
    {
        return $id;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function render($params)
    {
        $func = $this->func;
        $out = '';
        if (function_exists($func)) {
            $out = $func($params);
        }

        return $out;
    }

    public function setInitial($data)
    {
        $this->initial = $data;
    }

    public function formatAttrs($attrs)
    {
        if (is_array($attrs)) {
            $tmp = array();
            foreach($attrs as $key => $val) {
                $tmp[] = sprintf('%s="%s"', $key, $val);
            }
            return implode(' ', $tmp);
        }
        return '';
    }
}

class Form_TextField extends Form_Field
{
}

class Form_PasswordField extends Form_Field
{
    public $func = 'form_password';
}

class Form_UploadField extends Form_Field
{
    public $func = 'form_upload';
}

class Form_TextareaField extends Form_Field
{
    public $func = 'form_textarea';

    protected function getDefaultParams()
    {
        return array('cols' => 20,
                     'rows' => 5,
                     'value' => strlen($this->value) ? $this->value : $this->initial);
    }
}

class Form_RadioField extends Form_Field
{
    public $func = 'form_radio';
    private static $i;

    public function getRenderParams()
    {
        $default = $this->getDefaultParams();
        $params = array_merge($default, $this->extra);

        $params['name'] = $this->name;
        $params['id'] = isset($this->extra['id']) ? $this->extra['id'] : $this->autoId();

        return $params;
    }

    protected function getDefaultParams()
    {
        return array();
    }

    public function render($params)
    {
        if (!isset(self::$i[$this->name])) {
            self::$i[$this->name] = 0;
        }

        $func = $this->func;
        $out = '';
        $checked_value = $this->value ? $this->value : $this->initial;

        if (!is_array($checked_value)) {
            $checked_value = array($checked_value);
        }

        foreach ($this->choices as $value => $label) {
            $this->id_suffix = '_' . self::$i[$this->name]++;
            $params['id'] = isset($this->extra['id']) ? $this->extra['id'] : $this->autoId();
            $params['value'] = $value;

            if (!in_array($checked_value, $this->empty_values)
                && in_array($value, $checked_value)) {

                $params['checked'] = TRUE;
            } else {
                $params['checked'] = FALSE;
            }

            if ($this->is_multiple) {
                $params['name'] = $this->name . '[]';
            }

            /*
            $out .= '<span>';
            $out .= $func($params);
            $out .= $this->labelTag($label);
            $out .= '</span>';
             */
            $class = '';
            if ($func == 'form_radio') {
                $class = 'radio';
            } elseif ($func == 'form_checkbox') {
                $class = 'checkbox';
            }
            $inline = isset($this->extra['inline']) ? 'inline' : '';
            $out .= '<label class="' . $inline . ' ' . $class . '">';
            $out .= $func($params);
            $out .= $label;
            $out .= '</label>';
            $out .= "\n";
        }

        return $out;
    }
}

class Form_CheckboxField extends Form_RadioField
{
    private static $i;
    public $func = 'form_checkbox';
}

class Form_MultiCheckboxField extends Form_CheckboxField
{
    private static $i;
    public $is_multiple = TRUE;
}

class Form_DropdownField extends Form_Field
{
    public $func = 'form_dropdown';

    public function getDefaultParams()
    {
        return array('value' => !in_array($this->value, $this->empty_values) ? $this->value : $this->initial);
    }

    public function render($params)
    {
        $func = $this->func;
        $selected = $params['value'];
        unset($params['name']);
        unset($params['value']);

        $out = $func($this->name, $this->choices, $selected, $this->formatAttrs($params));

        return $out;
    }

}

class Form_SubmitField extends Form_Field
{
    public $func = 'form_submit';
}

class Form_HiddenField extends Form_Field
{
    public $func = 'form_hidden';
    public $is_hidden = TRUE;

    public function render()
    {
        $func = $this->func;
        $value = $this->value ? $this->value : $this->initial;
        $out = $func($this->name, $value);

        return $out;
    }
}

class Form_EmptyField extends Form_Field
{
    public $func = '';
    public $is_hidden = TRUE;
    public $is_empty = TRUE;

    public function render()
    {
        return '';
    }
}


class Form_PreviewField extends Form_Field
{
    public function __construct($field)
    {
        $props = get_object_vars($field);
        foreach ($props as $key => $val) {
            $this->$key = $val;
        }
    }

    public function render($params)
    {
        $out = '';
        switch ($this->func) {
        case 'form_radio':
            $out .= sprintf('<span class="cleaned_data">%s</span>', form_prep($this->choices[$this->value]));
            $out .= form_hidden($this->name, form_prep($this->value));
            break;
        case 'form_checkbox':
            if (!$this->is_multiple) {
                $out .= sprintf('<span class="cleaned_data">%s</span>', form_prep($this->choices[$this->value]));
                $out .= form_hidden($this->name, form_prep($this->value));
            } else {
                if (is_array($this->value)) {
                    foreach ($this->value as $value) {
                        $out .= sprintf('<span class="cleaned_data">%s</span>', form_prep($this->choices[$value]));
                        $out .= form_hidden($this->name . '[]', form_prep($value));
                    }
                }
            }
            break;
        case 'form_hidden':
            $out .= form_hidden($this->name, form_prep($this->value));
            break;
        default:
            $out = sprintf('<span class="cleaned_data">%s</span>', nl2br(form_prep($this->value)));
            $out .= form_hidden($this->name, form_prep($this->value));
        }
        return $out;
    }
}

class Form_ProxyField
{

    public $form;
    public $name;
    public $field;

    public function __construct($form, $name)
    {
        $this->form = $form;
        $this->name = $name;
        $this->field = $form->fields[$name];
    }

    public function error()
    {
        $error = isset($this->form->errors[$this->name]) ? $this->form->errors[$this->name] : NULL;

        if ($error) {
            $error = sprintf(Form::$error_row, $error);
        } else {
            $error = '';
        }

        return $error;
    }

    public function required()
    {
        return $this->field->required;
    }

    public function labelTag($required=NULL)
    {
        return $this->field->labelTag($this->field->label, array(), $this->required());
    }

    public function label()
    {
        return $this->field->label;
    }

    public function id()
    {
        return isset($this->field->extra['id']) ? $this->field->extra['id'] : $this->field->autoId();
    }

    public function help_text()
    {
        if ($this->field->help_text) {
            $help_text = $this->field->help_text;
        } else {
            $help_text = '';
        }

        return $help_text;
    }

    public function render()
    {
        return $this->field->prefix . $this->field->render($this->field->getRenderParams()) . $this->field->suffix;
    }

    public function __get($prop)
    {
        if (in_array($prop, array('error', 'label', 'labelTag', 'id', 'required', 'help_text'))) {
            return $this->$prop();
        } else {
            return $this->$prop;
        }
    }

    public function __toString()
    {
        return (string) $this->render();
    }
}
