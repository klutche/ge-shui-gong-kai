<?php

class User
{
    private $username;
    private $password;
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function initialize($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function login()
    {
        $this->CI->load->model('usermodel');

        $user = $this->CI->usermodel->authenticate($this->username, $this->password);

        if (!$user) {
           return FALSE;
        }

        $this->CI->session->set_userdata('logged_in', TRUE);
        $this->CI->session->set_userdata('login_user', $user->username);
        $this->CI->session->set_userdata('login_user_name', $user->name);
        $this->CI->session->set_userdata('login_user_area', AREA_NAME);
        $this->CI->session->set_userdata('login_user_privilege', $user->role);
        $this->CI->session->set_userdata('login_id', $user->id);
        $this->CI->session->set_userdata('login_group_id', $user->group_id);
        $this->CI->session->set_userdata('login_group_name', $user->group->name);

        $this->CI->usermodel->recodeLoginDate($user->id);

        return TRUE;
    }

    public function logout()
    {
        $this->CI->session->sess_destroy();
        
        $this->CI->load->model('usermodel');
        $this->CI->usermodel->recodeLogoutDate($this->CI->session->userdata('login_id'));
    }
}


