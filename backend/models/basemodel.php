<?php

class Basemodel extends CI_Model
{
    protected $table;
    protected $CI;

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function search($condition, $offset=0, $limit=10, $order='id')
    {
        $this->db->from($this->table);
        $this->appendCondition($condition);
        $this->db->limit($limit, $offset);
        $this->db->order_by($order);

        $q = $this->db->get();

        log_message('debug', $this->db->last_query());

        $ret = array();
        foreach ($q->result() as $row) {
            $this->afterResult($row);
            $ret[] = $row;
        }

        return $ret;
    }

    public function get($id)
    {
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $this->beforeGet();
        $q = $this->db->get();

        $row = $q->row();

        $this->afterGet($row);

        return $row;
    }

    public function total($condition)
    {
        $this->db->select('count(*) as c');
        $this->db->from($this->table);
        $this->appendCondition($condition);
        $q = $this->db->get();

        $row = $q->row();

        return $row->c;
    }

    public function save($data, $id=NULL)
    {
        $this->beforeSave($data);

        if (is_null($id)) {
            $this->beforeInsert($data);
            $res = $this->_insert($data);
            if ($res) {
                $this->afterInsert($data);
            }
        } else {
            $this->beforeUpdate($data, $id);
            $res = $this->_update($data, $id);
            if ($res) {
                $this->afterUpdate($data, $id);
            }
        }

        $this->afterSave($data);
        return $res;
    }

    public function delete($id)
    {
        $this->beforeDelete($id);

        $res = $this->db->from($this->table)
            ->where('id', $id)
            ->delete();

        if ($res) {
            $this->afterDelete($id);
        }

        return $res;
    }

    protected function _insert($data)
    {
        return $this->db->from($this->table)
            ->set($data)
            ->insert();
    }

    protected function _update($data, $id)
    {
        return $this->db->from($this->table)
            ->set($data)
            ->where('id', $id)
            ->update();
    }

    protected function afterResult($row)
    {
    }

    protected function beforeGet()
    {
    }

    protected function afterGet($row)
    {
        $this->afterResult($row);
    }

    protected function beforeSave(&$data)
    {
    }

    protected function beforeInsert(&$data)
    {
    }

    protected function beforeUpdate(&$data, $id=NULL)
    {
    }

    protected function beforeDelete($id)
    {
    }

    protected function afterSave(&$data)
    {
    }

    protected function afterInsert(&$data)
    {
    }

    protected function afterUpdate(&$data, $id)
    {
    }

    protected function afterDelete($id)
    {
    }

    protected function appendCondition($condition)
    {
    }
}
