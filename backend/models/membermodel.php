<?php

require_once __DIR__ . '/basemodel.php';

class Membermodel extends Basemodel
{
    protected $table = 'members';

    private $self;
    private $secret_key;
    private $encrypt;


    public function __construct()
    {
        parent::__construct();
        $ci = get_instance();
        $this->encrypt = $ci->encrypt;
    }

    public function setSecretKey($secret_key)
    {
        $this->secret_key = $secret_key;
    }

    public function registerTemporary($email, $password)
    {
        $q = $this->db->from($this->table)
            ->where('email', $email)
            ->get();

        // email の重複チェック
        if ($q->num_rows() > 0) {
            throw new MemberDuplicateException('このメールアドレスは登録できません。');
        }

        $encoded_password = $this->encrypt->encode($password);
        $created_at = date('Y-m-d H:i:s');

        $data = array(
            'email' => $email,
            'password' => $encoded_password,
            'is_verify' => 0,
            'is_active' => 0,
            'created_at' => $created_at,
            'hash' => $this->hash($email, $created_at));

        $ret = $this->db->from($this->table)
            ->set($data)
            ->insert();

        if (!$ret) {
            throw new MemberRegisterException('登録に失敗しました。');
        }

        $this->afterInsert();

        return TRUE;
    }

    public function generateVerifyUrl($base_url)
    {
        if (!$this->self) {
            return FALSE;
        }

        $url = sprintf('%sap/member/verify/%d/%s', $base_url, $this->self->id, $this->self->hash);

        return $url;
    }

    public function auth($username, $password)
    {
        $member = $this->getByUsername($username);
        if (!$member) {
            return FALSE;
        }

        $decoded_password = $this->encrypt->decode($member->password);
        if ($decoded_password == $password) {
            return $member;
        }
        return FALSE;
    }

    public function hash($username, $created_at)
    {
        $hash = hash_hmac('sha256', $username . $created_at, $this->secret_key);

        return $hash;
    }

    public function verify($hash)
    {
        if (!$this->self) {
            return FALSE;
        }

        if ($hash == $this->self->hash) {
            $ret = $this->db->from($this->table)
                    ->set(array(
                        'is_verify' => 1,
                        'is_active' => 1,
                        'verified_at' => date('Y-m-d H:i:s')))
                    ->where('id', $this->self->id)
                    ->where('is_verify', 0)
                    ->update();

            // update 失敗
            if (!$ret) {
                return FALSE;
            }

            // 該当データなし
            if ($this->db->affected_rows() == 0) {
                return FALSE;
            }

            return TRUE;
        }

        return FALSE;
    }


    public function updateLoginedAt()
    {
        if (!$this->self) {
            return FALSE;
        }

        return $this->db->from($this->table)
                ->set(array(
                    'logined_at' => date('Y-m-d H:i:s')))
                ->where('id', $self->id)
                ->update();
    }

    public function get($id, $is_active=null)
    {
        $this->db->from($this->table)
            ->where('id', $id);

        if ($is_active !== null) {
            $this->db->where('is_active', $is_active);
        }
    
        $q = $this->db->get();


        if ($q->num_rows() > 0) {
            $row = $q->row();
            $this->self = $row;
            return $row;
        }

        return FALSE;
    }

    public function getByUsername($username, $is_active=null)
    {
        $this->db->from($this->table)->where('email', $username);
        if (!is_null($is_active)) {
            $this->db->where('is_active', $is_active);
        }

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            $row = $q->row();
            $this->self = $row;
            return $row;
        }

        return FALSE;
    }

    public function updatePassword($new_password, $old_password)
    {
        if (!$this->self) {
            return FALSE;
        }

        $decoded_password = $this->encrypt->decode($this->self->password);

        if ($old_password != $decoded_password) {
            return FALSE;
        }

        return $this->db->from($this->table)
                ->set(array(
                    'password' => $this->encrypt->encode($new_password)))
                ->where('id', $this->self->id)
                ->update();
    }

    public function resign($password, $reason)
    {
        if (!$this->self) {
            return FALSE;
        }

        $decoded_password = $this->encrypt->decode($this->self->password);
        if ($password != $decoded_password) {
            return FALSE;
        }

        return $this->db->from($this->table)
                ->set(array(
                    'resigned_at' => date('Y-m-d H:i:s'),
                    'is_active' => 0,
                    'resign_reason' => $reason
                ))
                ->where('id', $this->self->id)
                ->update();
    }

    public function getSelf()
    {
        return $this->self;
    }

    protected function afterInsert()
    {
        $this->insert_id = $this->db->insert_id();
    }

    protected function appendCondition($condition)
    {
        if (isset($condition['query']) && strlen($condition['query']) > 0) {
            $this->db->or_like('name', $condition['query']);
            $this->db->or_like('kana', $condition['query']);
            $this->db->or_like('address', $condition['query']);
            $this->db->or_like('tel', $condition['query']);
            $this->db->or_like('email', $condition['query']);
        }

        if (isset($condition['is_resign']) && $condition['is_resign']) {
            $this->db->where('resigned_at IS NOT NULL');
        }

        if (isset($condition['is_verify']) && $condition['is_verify']) {
            $this->db->where('is_verify', 0);
        }

        if (isset($condition['verify_expire']) && $condition['verify_expire'] > 0) {
            $this->db->where('datediff(created_at, CURRENT_DATE) < -' . (int) $condition['verify_expire']);
        }
    }

    public function deleteExpiredNonMember($days=7)
    {
        return $this->db->from($this->table)
            ->where('is_verify', 0)
            ->where('datediff(created_at, CURRENT_DATE) < -' . (int) $days)
            ->delete();
    }

}


class MemberDuplicateException extends Exception {}
class MemberRegisterException extends Exception {}
