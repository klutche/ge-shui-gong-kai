<?php

require_once __DIR__ . '/basemodel.php';

class Pickupmodel extends Basemodel
{
    protected $table = 'pickups';
    protected $member_id;

    public function setMemberId($member_id)
    {
        $this->member_id = $member_id;
    }

    public function deleteByMemberId($member_id)
    {
        return $this->db->from($this->table)
            ->where('member_id', $member_id)
            ->delete();
    }

    protected function beforeGet()
    {
        if (!is_null($this->member_id) && $this->member_id > 0) {
            $this->db->where('member_id', $this->member_id);
        }

    }

    protected function appendCondition($condition)
    {
        if (isset($condition['member_id'])) {
            $this->db->where('member_id', $condition['member_id']);
        }
    }

}
