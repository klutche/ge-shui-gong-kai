<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <?= form_open('auth/login') ?>
    <?if (isset($error)): ?>
    <div class="alert alert-error">
      <?= $error ?>
    </div>
    <?endif?>

    <?= $form ?>

    <div class="actions">
      <?= form_submit('login', 'ログイン', 'class="btn btn-primary"') ?>
    </div>
    <?= form_close() ?>

  </div>

</div>

