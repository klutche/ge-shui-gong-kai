<div class="row-fluid">
  <div class="span10" id="main" role="main">
    <h1><?= $title ?></h1>

    <div class="text-right">
        <a class="btn btn-primary" href="<?= site_url('config/input') ?>">追加</a>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>Key</th>
                <th>Label</th>
                <th>Value</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?foreach($configs as $config): ?>
            <tr>
                <td><?= $config->key ?></td>
                <td><?= $config->label ?></td>
                <td style="word-break: break-all"><?if (is_string($config->value)): ?>
                    <?= nl2br($config->value) ?>
                    <?else:?>
                    <pre style="font-family: sans-serif"><? print_r($config->value) ?></pre>
                    <?endif?>
                    </td>
                <td><a class="btn btn-edit" href="<?= site_url('config/input/' . $config->id) ?>">編集</a></td>
            </tr>
            <?endforeach?>

        </tbody>
    </table>
    </table>
  </div>

  <div class="span2" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>


