<div class="row-fluid">
  <div class="span10" id="main" role="main">
    <h1><?= $title ?></h1>

    <?= form_open('config/input/' . $id) ?>
    <?= $form ?>

    <div class="actions">
        <button type="submit" class="btn btn-primary">保存する</button>
        <?if (isset($id)): ?>
        <a class="offset9" href="<?= site_url('config/delete/' . $id) ?>">削除する</a>
        <?endif?>
    </div>
    <?= form_close() ?>
  </div>

  <div class="span2" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>


