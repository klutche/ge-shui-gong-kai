<div class="row-fluid">
    <div class="span10" id="main" role="main">
        <h2>
        <?= $title ?>
        </h2>
        <table class="table table-bordered">
            <tr>
                <th>ID</th>
                <td><?= $data['id'] ?></td>
            </tr>
            <tr>
                <th>タイプ</th>
                <td><?= $data['type'] ?></td>
            </tr>
            <tr>
                <th>テンプレート</th>
                <td><?= $data['template'] ?></td>
            </tr>
            <tr>
                <th><?= $data['type'] ?>ID</th>
                <td><a href="<?= site_url($prefix.'/detail/'.$data['source_id']) ?>"><?= $data['source_id'] ?></a></td>
            </tr>
            <tr>
                <th>送信先</th>
                <td><?= $data['name'] ?> (<?= $data['mailto'] ?>)</td>
            </tr>
            <tr>
                <th>件名</th>
                <td><?= $data['subject'] ?></td>
            </tr>
            <tr>
                <th>送信日時</th>
                <td><?= $data['created_at'] ?></td>
            </tr>
        </table>
        <div class="well">
            <?= $data['content'] ?>
        </div>
    </div>
    <div class="span2" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
