<div class="row-fluid">
    <div class="span10" id="main" role="main">
        <h2><?= $title ?></h2>
        <div class="row-fluid">
            <div class="span9">
                <?= form_open('email', array('method' => 'get', 'class' => 'form-inline')) ?>
                    <div class="input-append">
                        <input type="text" name="word" value="<?= $word ?>" placeholder="タイプ・名前・メールアドレス">
                        <button type="submit" class="btn">検索</button>
                    </div>
                <?= form_close() ?>
            </div>
            <div class="span3 text-right">
                <a href="<?= site_url('email/csv') ?>" class="btn">CSV出力</a>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>タイプ</th>
                    <th>テンプレート</th>
                    <th>送信先</th>
                    <th>送信日時</th>
                    <th style="text-align: center;">詳細</th>
                </tr>
            </thead>
            <tbody>
<?php foreach ($data as $email) { ?>
                <tr>
                    <td><?= $email['id'] ?></td>
                    <td><?= $email['type'] ?></td>
                    <td><?= $email['template'] ?></td>
                    <td><?= $email['name'] ?> (<?= $email['mailto'] ?>)</td>
                    <td><?= $email['created_at'] ?></td>
                    <td><a href="<?= site_url('email/detail/'.$email['id']) ?>" class="btn">詳細</a></td>
                </tr>
<?php } ?>
            </tbody>
        </table>
        <?= $pager ?>
    </div>
    <div class="span2" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
