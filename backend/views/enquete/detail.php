<div class="row-fluid">
    <div class="span10" id="main" role="main">
        <h2><?= $title ?></h2>
        <table class="table table-bordered">
            <tr>
                <th>ID</th>
                <td><?= $data['id'] ?></td>
            </tr>
            <tr>
                <th>送信日時</th>
                <td><?= $data['created_at'] ?></td>
            </tr>
            <tr>
                <th>注文ID</th>
                <td><a href="<?= site_url('order/detail/'.$data['order_id']) ?>"><?= $data['order_id'] ?></a></td>
            </tr>
            <tr>
                <th>会員ID</th>
                <td>
<?php if (strlen($data['member_id']) > 0) { ?>
                    <a href="<?= site_url('member/detail/'.$data['member_id']) ?>"><?= $data['member_id'] ?></a>
<?php } ?>
                </td>
            </tr>
            <tr>
                <th>名前</th>
                <td><?= $order['name'] ?> (<?= $order['email'] ?>)</td>
            </tr>
            <tr>
                <th>当サイトをどこで知りましたか？</th>
                <td>
<?php foreach($data['question_1'] as $val) { ?>
                    <?= $val ?><br>
<?php } ?>
                </td>
            </tr>
            <tr>
                <th>その他SNS</th>
                <td><?= $data['remarks_1'] ?></td>
            </tr>
            <tr>
                <th>知人からの口コミ</th>
                <td><?= $data['remarks_2'] ?></td>
            </tr>
            <tr>
                <th>口コミサイトを見て</th>
                <td><?= $data['remarks_3'] ?></td>
            </tr>
            <tr>
                <th>その他</th>
                <td><?= $data['remarks_4'] ?></td>
            </tr>
        </table>
    </div>
    <div class="span2" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
