<div class="row-fluid">
    <div class="span10" id="main" role="main">
        <h2><?= $title ?></h2>
        <div class="text-right">
            <?= form_open('enquete/csv', array('class' => 'form-inline')) ?>
                <input name="start" type="text" class="input-medium datepicker">
                ～
                <input name="end" type="text" class="input-medium datepicker">
                <input type="submit" value="CSV出力" class="btn">
            <?= form_close() ?>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>注文ID</th>
                    <th>名前</th>
                    <th>送信日時</th>
                    <th style="text-align: center;">詳細</th>
                </tr>
            </thead>
            <tbody>
<?php foreach ($data as $enquete) { ?>
                <tr>
                    <td><?= $enquete['id'] ?></td>
                    <td><?= $enquete['order_id'] ?></td>
                    <td><?= $orders[$enquete['order_id']]['name'].' ('.$orders[$enquete['order_id']]['email'].')' ?></td>
                    <td><?= $enquete['created_at'] ?></td>
                    <td style="text-align: center;"><a href="/__admin__/enquete/detail/<?= $enquete['id'] ?>" class="btn">詳細</a></td>
                </tr>
<?php } ?>
            </tbody>
        </table>
        <?= $pager ?>
    </div>
    <div class="span2" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
