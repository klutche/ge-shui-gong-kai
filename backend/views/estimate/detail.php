<div class="row-fluid">
    <div class="span10" id="main" role="main">
        <div class="row-fluid">
            <h2 class="span6"><?= $title ?></h2>
<?php if ($id != NULL) { ?>
            <div class="span6 text-right">
                <a href="<?= site_url('send/index/estimates/'.$estimate['id']) ?>" class="btn btn-info">メール作成</a>
                <a href="javascript:void(0);" class="btn btn-danger" onclick="var ok = confirm('削除します。\nよろしいですか？'); if (ok) location.href='<?= site_url('estimate/delete/'.$id) ?>'; return false;">削除</a>
            </div>
<?php } ?>
        </div>
        <?= form_open('', array('class' => 'form-horizontal')) ?>
<?php if ($result != '') { ?>
            <p class="alert"><?= $result ?></p>
<?php } ?>
            <div class="control-group"<?= $id == NULL ? ' style="display: none;"' : '' ?>>
                <label class="control-label">ID</label>
                <div class="controls">
                    <?= $estimate['id'] ?>
                </div>
            </div>
            <div class="control-group"<?= $id == NULL ? ' style="display: none;"' : '' ?>>
                <label class="control-label">依頼日時</label>
                <div class="controls">
                    <?= $estimate['estimate_at'] ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->name->id ?>"><?= $form->name->label ?></label>
                <div class="controls">
                    <?= $form->name->error; ?>
                    <?= $form->name ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->kana->id ?>"><?= $form->kana->label ?></label>
                <div class="controls">
                    <?= $form->kana->error; ?>
                    <?= $form->kana ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->email->id ?>"><?= $form->email->label ?></label>
                <div class="controls">
                    <?= $form->email->error; ?>
                    <?= $form->email ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->tel->id ?>"><?= $form->tel->label ?></label>
                <div class="controls">
                    <?= $form->tel->error; ?>
                    <?= $form->tel ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->prefcode->id ?>"><?= $form->prefcode->label ?></label>
                <div class="controls">
                    <?= $form->prefcode->error; ?>
                    <?= $form->prefcode ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">依頼品</label>
<?php for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) { ?>
                <div class="controls" style="margin-bottom: 10px;">
                    <?= $form->{'item_'.$i}->error ?>
                    <?= $form->{'item_etc_'.$i}->error ?>
                    <?= $form->{'item_num_'.$i}->error ?>
                    <?= $form->{'item_'.$i} ?>
                    <?= $form->{'item_etc_'.$i} ?>
                    <span class="item-num-label">数量:</span>
                    <?= $form->{'item_num_'.$i} ?>
                </div>
<?php } ?>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->is_expensive->id ?>"><?= $form->is_expensive->label ?></label>
                <div class="controls">
                    <?= $form->is_expensive->error; ?>
                    <?= $form->is_expensive ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->pickup_boxes->id ?>"><?= $form->pickup_boxes->label ?></label>
                <div class="controls">
                    <?= $form->pickup_boxes->error; ?>
                    <?= $form->pickup_boxes ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->remarks->id ?>"><?= $form->remarks->label ?></label>
                <div class="controls">
                    <?= $form->remarks->error; ?>
                    <?= $form->remarks ?>
                </div>
            </div>
            <div class="control-group"<?= $id == NULL ? ' style="display: none;"' : '' ?>>
                <label class="control-label" for="<?= $form->amount->id ?>"><?= $form->amount->label ?></label>
                <div class="controls">
                    <?= $form->amount->error; ?>
                    <?= $form->amount ?>
                </div>
            </div>
            <div class="control-group"<?= $id == NULL ? ' style="display: none;"' : '' ?>>
                <label class="control-label" for="<?= $form->is_sendmail->id ?>"><?= $form->is_sendmail->label ?></label>
                <div class="controls">
                    <?= $form->is_sendmail->error; ?>
                    <?= $form->is_sendmail ?>
                </div>
            </div>
            <div class="control-group"<?= $id == NULL ? ' style="display: none;"' : '' ?>>
                <label class="control-label">更新日時</label>
                <div class="controls">
                    <?= $estimate['updated_at'] ?>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <?= $form->hiddenfields ?>
                    <button type="submit" class="btn btn-success"><?= $id == NULL ? '作成' : '更新' ?></button>
                    <a href="<?= site_url('estimate') ?>" class="btn">キャンセル</a>
                </div>
            </div>
        <?= form_close() ?>
<?php if (!empty($images)) { ?>
        <h2>画像</h2>
<?php foreach ($images as $image) { ?>
        <img src="<?= '/ap/uploads/'.$image['path'] ?>" alt="" class="img-polaroid" style="max-width: 600px;">
<?php } ?>
<?php } ?>
    </div>
    <div class="span2" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
