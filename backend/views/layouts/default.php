<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title><?= isset($title) ? $title : 'Untitled' ?>|<?= get_item('site_name') ?></title>
<link rel="stylesheet" href="<?= site_url('assets/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= site_url('assets/css/bootstrap-responsive.min.css') ?>">
<?php if (isset($styles) && is_array($styles)): ?>
<?php foreach ($styles as $style): ?>
<link rel="stylesheet" href="<?= site_url($style) ?>">
<?php endforeach; ?>
<?php endif; ?>
<link rel="stylesheet" href="<?= site_url('assets/css/style.css?v=2021-07-08') ?>">
<!--[if IE]>
<link rel="stylesheet" href="<?= site_url('assets/css/ie.css') ?>">
<![endif]-->
</head>
<body>
<header id="header">
    <div class="container-fluid">
        <h1 id="brand"><a href="/"><img src="<?= site_url('assets/img/logo.png') ?>" alt="革水"></a></h1>
        <div id="header-login">
<?php if ($this->session->userdata('logged_in')): ?>
            <a href="<?= site_url('member/logout') ?>">ログアウト</a>
<?php else: ?>
            <a href="<?= site_url('member/login') ?>">ログイン</a>
<?php endif; ?>
        </div>
    </div>
</header>
<div class="container-fluid">
    {yield}
</div>
<footer id="footer">
    <div class="container-fluid">
        <p id="copyright"><small>&copy; Kawasui.</small></p>
    </div>
</footer>
<script>
var base_url = '<?= site_url() ?>';
</script> 
<script src="<?= site_url('assets/js/libs/jquery-1.9.1.min.js') ?>"></script> 
<script src="<?= site_url('assets/js/libs/jquery.pjax.js') ?>"></script> 
<script src="<?= site_url('assets/js/libs/jquery.cookie.js') ?>"></script> 
<script src="<?= site_url('assets/js/libs/bootstrap.min.js') ?>"></script> 
<script src="<?= site_url('assets/js/application.js') ?>"></script>
<?php if (isset($scripts) && is_array($scripts)): ?>
<?php foreach ($scripts as $script): ?>
<script src="<?= site_url($script) ?>"></script>
<?php endforeach; ?>
<?php endif; ?>
<script>
</script>
</body>
</html>
