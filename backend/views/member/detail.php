<div class="row-fluid">
    <div class="span10" id="main" role="main">
        <h2><?= $title ?></h2>
        <table class="table">
            <tr>
                <th class="span2">名前</th>
                <td><?= $member->name ?></td>
            </tr>
            <tr>
                <th>なまえ</th>
                <td><?= $member->kana ?></td>
            </tr>
            <tr>
                <th>郵便番号</th>
                <td><?= $member->zipcode ?></td>
            </tr>
            <tr>
                <th>住所</th>
                <td><?= $member->address ?></td>
            </tr>
            <tr>
                <th>電話番号</th>
                <td><?= $member->tel ?></td>
            </tr>
            <tr>
                <th>メールアドレス</th>
                <td><?= $member->email ?></td>
            </tr>
            <tr>
                <th>備考</th>
                <td><?= $member->remarks ?></td>
            </tr>
        </table>
        <h2>集配先</h2>
<?php foreach ($pickups as $pickup): ?>
        <table class="table table-pickup">
            <tr>
                <th class="span2">名前</th>
                <td><?= $pickup->name ?></td>
            </tr>
            <tr>
                <th>郵便番号</th>
                <td><?= $pickup->zipcode ?></td>
            </tr>
            <tr>
                <th>住所</th>
                <td><?= $pickup->address ?></td>
            </tr>
            <tr>
                <th>電話番号</th>
                <td><?= $pickup->tel ?></td>
            </tr>
        </table>
<?php endforeach; ?>
        <h2>その他</h2>
        <table class="table">
            <tr>
                <th class="span2">登録日</th>
                <td><?= date('Y-m-d H:i', strtotime($member->created_at)) ?></td>
            </tr>
            <tr>
                <th class="span2">承認日</th>
                <td><?= (!is_null($member->verified_at) ? date('Y-m-d H:i', strtotime($member->verified_at)) : '') ?></td>
            </tr>
            <tr>
                <th class="span2">変更日</th>
                <td><?= (!is_null($member->updated_at) ? date('Y-m-d H:i', strtotime($member->updated_at)) : '') ?></td>
            </tr>
            <tr>
                <th class="span2">最終ログイン日</th>
                <td><?= (!is_null($member->logined_at) ? date('Y-m-d H:i', strtotime($member->logined_at)) : '') ?></td>
            </tr>
            <tr>
                <th class="span2">最終注文日</th>
                <td><?= (!is_null($member->last_ordered_at) ? date('Y-m-d H:i', strtotime($member->last_ordered_at)) : '') ?></td>
            </tr>
<?php if ($resign == 'resign') { ?>
            <tr>
                <th class="span2">退会日</th>
                <td><?= (!is_null($member->resigned_at) ? date('Y-m-d H:i', strtotime($member->resigned_at)) : '') ?></td>
            </tr>
<?php } ?>
        </table>
        <h2>注文履歴</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>種別</th>
                    <th>ID</th>
                    <th>名前</th>
                    <th>注文日時</th>
                    <th style="text-align: center;">詳細</th>
                </tr>
            </thead>
            <tbody>
<?php foreach ($orders as $order) { ?>
                <tr>
                    <td>注文</td>
                    <td><?= $order['id'] ?></td>
                    <td><?= $order['name'] ?></td>
                    <td><?= $order['created_at'] ?></td>
                    <td style="text-align: center;"><a href="/__admin__/order/detail/<?= $order['id'] ?>" class="btn">詳細</a></td>
                </tr>
<?php
    if (isset($estimates[$order['id']])) {
        $estimate = $estimates[$order['id']];
?>
                <tr style="background: #f5f5f5;">
                    <td>└ 見積</td>
                    <td><?= $estimate['id'] ?></td>
                    <td><?= $estimate['name'] ?></td>
                    <td><?= $estimate['created_at'] ?></td>
                    <td style="text-align: center;"><a href="/__admin__/estimate/detail/<?= $estimate['id'] ?>" class="btn">詳細</a></td>
                </tr>
<?php } ?>
<?php } ?>
            </tbody>
        </table>
    </div>
    <div class="span2" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
