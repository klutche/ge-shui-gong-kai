<div class="row-fluid">
    <div class="span10" id="main" role="main">
        <h2><?= $title ?></h2>
        <div class="row-fluid">
            <div class="span9">
                <?= form_open('', array('method' => 'get', 'class' => 'form-inline')) ?>
                    <div class="input-prepend">
                        <span class="add-on"><?= $type == 'resign' ? '退会日' : '入会日' ?></span>
                        <input type="text" name="start" value="<?= $start ?>" class="input-small datepicker">
                    </div>
                    <span>～</span>
                    <input type="text" name="end" value="<?= $end ?>" class="input-small datepicker">
                    <span>&nbsp;</span>
                    <div class="input-append">
                        <input type="text" name="word" value="<?= $word ?>" placeholder="名前・カナ・住所・メール">
                        <button type="submit" class="btn">検索</button>
                    </div>
                <?= form_close() ?>
            </div>
            <div class="span3 text-right">
                <a href="<?= site_url('member/csv') ?><?= $type == 'resign' ? '/resign' : '' ?><?= $params ?>" class="btn">CSV出力</a>
            </div>
        </div>
        <p class="label"><?= $total ?>件</p>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>名前</th>
                    <th>住所</th>
                    <th>メールアドレス</th>
                    <th><?= $type == 'resign' ? '退会日' : '入会日' ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
<?php foreach ($data as $val) { ?>
                <tr>
                    <td><?= $val['id'] ?></td>
                    <td><?= $val['name'] ?></td>
                    <td><?= $val['address'] ?></td>
                    <td><?= $val['email'] ?></td>
                    <td><?= $type == 'resign' ? $val['resigned_at'] : $val['verified_at'] ?></td>
                    <td><a href="<?= site_url('member/detail/'.$val['id']) ?><?= $type == 'resign' ? '/resign' : '' ?>" class="btn">詳細</a></td>
                </tr>
<?php } ?>
            </tbody>
        </table>
        <?= $pager ?>
    </div>
    <div class="span2" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
