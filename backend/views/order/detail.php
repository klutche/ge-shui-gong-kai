<div class="row-fluid">
    <div class="span10" id="main" role="main">
        <div class="row-fluid">
            <h2 class="span6"><?= $title ?></h2>
<?php if ($id != NULL) { ?>
            <div class="span6 text-right">
                <a href="<?= site_url('send/index/orders/'.$order['id']) ?>" class="btn btn-info">メール作成</a>
                <a href="javascript:void(0);" class="btn btn-danger" onclick="var ok = confirm('削除します。\nよろしいですか？'); if (ok) location.href='<?= site_url('order/delete/'.$id) ?>'; return false;">削除</a>
            </div>
<?php } ?>
        </div>
        <?= form_open('order/detail/'.$id, array('class' => 'form-horizontal')) ?>
            <?php if ($result != '') { ?>
            <p class="alert"><?= $result ?></p>
            <?php } ?>
            <div class="control-group"<?= $id == NULL ? ' style="display: none;"' : '' ?>>
                <label class="control-label">注文ID</label>
                <div class="controls">
                    <?= $id ?>
                </div>
            </div>
            <div class="control-group"<?= $id == NULL ? ' style="display: none;"' : '' ?>>
                <label class="control-label">見積もりID</label>
                <div class="controls">
<?php if ($order['estimate_id'] !== null) { ?>
                    <a href="<?= site_url('estimate/detail/'.$order['estimate_id']) ?>"><?= $order['estimate_id'] ?></a>
<?php } else { ?>
                    なし
<?php } ?>
                </div>
            </div>
            <div class="control-group"<?= $id == NULL ? ' style="display: none;"' : '' ?>>
                <label class="control-label">会員ID</label>
                <div class="controls">
<?php if ($order['member_id'] !== null) { ?>
                    <a href="<?= site_url('member/detail/'.$order['member_id']) ?>"><?= $order['member_id'] ?></a>
<?php } else { ?>
                    なし
<?php } ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->name->id ?>"><?= $form->name->label ?></label>
                <div class="controls">
                    <?= $form->name->error; ?>
                    <?= $form->name ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->kana->id ?>"><?= $form->kana->label ?></label>
                <div class="controls">
                    <?= $form->kana->error; ?>
                    <?= $form->kana ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->email->id ?>"><?= $form->email->label ?></label>
                <div class="controls">
                    <?= $form->email->error; ?>
                    <?= $form->email ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->tel->id ?>"><?= $form->tel->label ?></label>
                <div class="controls">
                    <?= $form->tel->error; ?>
                    <?= $form->tel ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->zipcode->id ?>"><?= $form->zipcode->label ?></label>
                <div class="controls">
                    <?= $form->zipcode->error; ?>
                    <?= $form->zipcode ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->address->id ?>"><?= $form->address->label ?></label>
                <div class="controls">
                    <?= $form->address->error; ?>
                    <?= $form->address ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->pickup_name->id ?>"><?= $form->pickup_name->label ?></label>
                <div class="controls">
                    <?= $form->pickup_name->error; ?>
                    <?= $form->pickup_name ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->pickup_tel->id ?>"><?= $form->pickup_tel->label ?></label>
                <div class="controls">
                    <?= $form->pickup_tel->error; ?>
                    <?= $form->pickup_tel ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->pickup_zipcode->id ?>"><?= $form->pickup_zipcode->label ?></label>
                <div class="controls">
                    <?= $form->pickup_zipcode->error; ?>
                    <?= $form->pickup_zipcode ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->pickup_address->id ?>"><?= $form->pickup_address->label ?></label>
                <div class="controls">
                    <?= $form->pickup_address->error; ?>
                    <?= $form->pickup_address ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->pickup_method->id ?>"><?= $form->pickup_method->label ?></label>
                <div class="controls">
                    <?= $form->pickup_method->error; ?>
                    <?= $form->pickup_method ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->pickup_date->id ?>"><?= $form->pickup_date->label ?></label>
                <div class="controls">
                    <?= $form->pickup_date->error; ?>
                    <?= $form->pickup_date ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->pickup_time->id ?>"><?= $form->pickup_time->label ?></label>
                <div class="controls">
                    <?= $form->pickup_time->error; ?>
                    <?= $form->pickup_time ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->pickup_boxes->id ?>"><?= $form->pickup_boxes->label ?></label>
                <div class="controls">
                    <?= $form->pickup_boxes->error; ?>
                    <?= $form->pickup_boxes ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->payment->id ?>"><?= $form->payment->label ?></label>
                <div class="controls">
                    <?= $form->payment->error; ?>
                    <?= $form->payment ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">依頼品</label>
<?php for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) { ?>
                <div class="controls" style="margin-bottom: 10px;">
                    <?= $form->{'item_'.$i}->error ?>
                    <?= $form->{'item_etc_'.$i}->error ?>
                    <?= $form->{'item_num_'.$i}->error ?>
                    <?= $form->{'item_'.$i} ?>
                    <?= $form->{'item_etc_'.$i} ?>
                    <span class="item-num-label">数量:</span>
                    <?= $form->{'item_num_'.$i} ?>
                </div>
<?php } ?>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->is_expensive->id ?>"><?= $form->is_expensive->label ?></label>
                <div class="controls">
                    <?= $form->is_expensive->error; ?>
                    <?= $form->is_expensive ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<?= $form->remarks->id ?>"><?= $form->remarks->label ?></label>
                <div class="controls">
                    <?= $form->remarks->error; ?>
                    <?= $form->remarks ?>
                </div>
            </div>
            <div class="control-group"<?= $id == NULL ? ' style="display: none;"' : '' ?>>
                <label class="control-label" for="<?= $form->amount->id ?>"><?= $form->amount->label ?></label>
                <div class="controls">
                    <?= $form->amount->error; ?>
                    <?= $form->amount ?>
                </div>
            </div>
            <div class="control-group"<?= $id == NULL ? ' style="display: none;"' : '' ?>>
                <label class="control-label" for="<?= $form->is_sendmail->id ?>"><?= $form->is_sendmail->label ?></label>
                <div class="controls">
                    <?= $form->is_sendmail->error; ?>
                    <?= $form->is_sendmail ?>
                </div>
            </div>
            <div class="control-group"<?= $id == NULL ? ' style="display: none;"' : '' ?>>
                <label class="control-label">更新日時</label>
                <div class="controls">
                    <?= $order['updated_at'] ?>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <?= $form->hiddenfields ?>
                    <button type="submit" class="btn btn-success"><?= $id == NULL ? '作成' : '更新' ?></button>
                    <a href="<?= site_url('order') ?>" class="btn">キャンセル</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
    <div class="span2" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
