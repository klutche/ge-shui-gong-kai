<div class="row-fluid">
    <div class="span10" id="main" role="main">
        <h2><?= $title ?></h2>
        <div class="row-fluid">
            <div class="span6">
                <?= form_open('order', array('method' => 'get', 'class' => 'form-inline')) ?>
                    <div class="input-append">
                        <input type="text" name="word" value="<?= $word ?>" placeholder="名前・カナ・メール・未送信">
                        <button type="submit" class="btn">検索</button>
                    </div>
                <?= form_close() ?>
            </div>
            <div class="span3 text-right">
<?php if ($unsent_num > 0) { ?>
                未送信があります <span class="badge badge-important"><?= $unsent_num ?></span>
<?php } ?>
            </div>
            <div class="span3 text-right">
                <a href="<?= site_url('order/detail') ?>" class="btn">新規作成</a>
                <a href="<?= site_url('order/csv') ?>" class="btn">CSV出力</a>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>名前</th>
                    <th>注文日時</th>
                    <th>電話番号</th>
                    <th style="text-align: center;">詳細</th>
                    <th style="text-align: center;">メール送信</th>
                    <th style="text-align: center;">メール作成</th>
                </tr>
            </thead>
            <tbody>
<?php foreach ($data as $order) { ?>
                <tr>
                    <td><?= $order['id'] ?></td>
                    <td><?= $order['name'] ?> (<?= $order['email'] ?>)</td>
                    <td><?= $order['order_at'] ?></td>
                    <td><?= $order['tel'] ?></td>
                    <td style="text-align: center;"><a href="/__admin__/order/detail/<?= $order['id'] ?>" class="btn">詳細</a></td>
                    <td style="text-align: center;"><?= $order['is_sendmail'] == '1' ? '済' : '<span style="color: #f00;">未</span>' ?><?= array_key_exists($order['id'], $emails) ? ' ('.$emails[$order['id']]['template'].')' : '' ?></td>
                    <td style="text-align: center;"><a href="<?= site_url('send/index/orders/'.$order['id']) ?>" class="btn btn-info">メール作成</a></td>
                </tr>
<?php } ?>
            </tbody>
        </table>
        <?= $pager ?>
    </div>
    <div class="span2" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
