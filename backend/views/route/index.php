<div class="row-fluid">
    <div class="span10" id="main" role="main">
        <h2><?= $title ?></h2>
        <div class="text-right">
            <a href="<?= site_url('order/csv') ?>" class="btn">CSV出力</a>
        </div>
        <ul class="nav nav-tabs">
            <li<?= $type == 'orders' ? ' class="active"' : '' ?>><a href="<?= site_url('route/index/orders') ?>">すべての注文</a></li>
            <li<?= $type == 'orders_only' ? ' class="active"' : '' ?>><a href="<?= site_url('route/index/orders_only') ?>">注文のみ</a></li>
            <li<?= $type == 'estimates_only' ? ' class="active"' : '' ?>><a href="<?= site_url('route/index/estimates_only') ?>">見積のみ</a></li>
            <li<?= $type == 'estimates_orders' ? ' class="active"' : '' ?>><a href="<?= site_url('route/index/estimates_orders') ?>">見積後注文</a></li>
        </ul>
        <table class="table">
            <thead>
                <tr>
                    <th>種別</th>
                    <th>ID</th>
                    <th>名前</th>
                    <th>注文日時</th>
                    <th style="text-align: center;">詳細</th>
                </tr>
            </thead>
            <tbody>
<?php foreach ($data as $val) { ?>
                <tr>
                    <td><?= $data_type ?></td>
                    <td><?= $val['id'] ?></td>
                    <td><?= $val['name'] ?></td>
                    <td><?= $val['created_at'] ?></td>
                    <td style="text-align: center;"><a href="/__admin__/<?= $type == 'estimates_only' ? 'estimate' : 'order' ?>/detail/<?= $val['id'] ?>" class="btn">詳細</a></td>
                </tr>
<?php
    if ($type == ('orders' || 'estimates_orders') && isset($sub_estimates[$val['id']])) {
        $sub_val = $sub_estimates[$val['id']];
?>
                <tr style="background: #f5f5f5;">
                    <td>└ 見積</td>
                    <td><?= $sub_val['id'] ?></td>
                    <td><?= $sub_val['name'] ?></td>
                    <td><?= $sub_val['created_at'] ?></td>
                    <td style="text-align: center;"><a href="/__admin__/estimate/detail/<?= $sub_val['id'] ?>" class="btn">詳細</a></td>
                </tr>
<?php } ?>
<?php } ?>
            </tbody>
        </table>
        <?= $pager ?>
    </div>
    <div class="span2" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
