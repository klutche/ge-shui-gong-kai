【ご依頼内容】<br>
<br>
◇お名前<br>
<?= $data['name'] ?><br>
<br>
◇フリガナ<br>
<?= $data['kana'] ?><br>
<br>
◇メールアドレス<br>
<?= $data['email'] ?><br>
<br>
◇電話番号<br>
<?= $data['tel'] ?><br>
<br>
◇集配先<br>
<?= get_item('prefcode')[$data['prefcode']] ?><br>
<br>
◇クリーニング品<br>
<?php
for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
    if (strlen($items[$i]['item']) > 0) {
?>
<?= $items[$i]['item'] ?>　数量：<?= $items[$i]['quantity'] ?><br>
<?php
    }
}
?>
<br>
◇高級品の有無<br>
<?= $data['is_expensive'] == '1' ? 'あり' : 'なし' ?><br>
<br>
◇依頼品のサイズ<br>
<?= $data['pickup_boxes'] ?><br>
<br>
◇ご要望・汚れの程度<br>
<?= nl2br($data['remarks']) ?><br>
