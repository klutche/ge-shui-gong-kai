<?= $data['name'] ?>様<br>
<br>
ご連絡頂きありがとうございます。<br>
下記のご注文のキャンセルを受付ました。<br>
またのご利用お待ち致しております。<br>
<hr>
◇ご依頼品<br>
<?php
for ($i = 0, $max = get_item('max_order_item'); $i < $max; $i++) {
    if (strlen($items[$i]['item']) > 0) {
?>
<?= $items[$i]['item'] ?>　数量：<?= $items[$i]['quantity'] ?><br>
<?php
    }
}
?>

<br>
★インスタグラム始めました。是非ご覧ください。★<br>
革水インスタグラム<br>
<a href="https://www.instagram.com/kumamotokawasui/" target="_blank">https://www.instagram.com/kumamotokawasui/</a>
<br>
<hr>
<?= $this->load->view('send/email_order_data', array('data' => $data, 'items' => $items), TRUE) ?>
<hr>
<?= str_replace('<br />', '<br>', nl2br(get_item('signature'))) ?>