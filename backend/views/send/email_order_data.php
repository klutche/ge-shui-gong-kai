【ご依頼内容】<br>
<br>
◇お名前<br>
<?= $data['name'] ?><br>
<br>
◇フリガナ<br>
<?= $data['kana'] ?><br>
<br>
◇メールアドレス<br>
<?= $data['email'] ?><br>
<br>
◇電話番号<br>
<?= $data['tel'] ?><br>
<br>
◇住所<br>
<?= $data['zipcode'] ?><br>
<?= $data['address'] ?><br>
<br>
◇集配方法<br>
<?= $data['pickup_method'] ?><br>
<br>
◇集配先お名前<br>
<?= $data['pickup_name'] ?><br>
<br>
◇集配先電話番号<br>
<?= $data['pickup_tel'] ?><br>
<br>
◇集配先住所<br>
<?= $data['pickup_zipcode'] ?><br>
<?= $data['pickup_address'] ?><br>
<br>
<?php
/* ヤマト運輸による集配の場合
-------------------------------------------------*/
if (strpos($data['pickup_method'], 'ヤマト運輸') !== false) {
?>
◇ご希望の集荷日<br>
<?= $data['pickup_date'] ?><br>
<br>
◇ご希望の集荷時間<br>
<?= $data['pickup_time'] ?><br>
<br>
◇梱包箱を希望<br>
<?= $data['pickup_boxes'] ?><br>
<br>
<?php
/*-----------------------------------------------*/
}
?>
◇お支払い方法<br>
<?= $data['payment'] ?><br>
<br>
◇クリーニング品<br>
<?php
for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
    if (strlen($items[$i]['item']) > 0) {
?>
<?= $items[$i]['item'] ?>　数量：<?= $items[$i]['quantity'] ?><br>
<?php
    }
}
?>
<br>
◇ご要望・汚れの程度<br>
<?= nl2br($data['remarks']) ?><br>
