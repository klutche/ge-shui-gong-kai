<div class="row-fluid">
    <div class="span10" id="main" role="main">
        <h2><?= $title ?></h2>
<?php if ($result != '') { ?>
        <p class="alert"><?= $result ?></p>
<?php } ?>
        <?= form_open('', array('class' => 'form-horizontal')) ?>
            <div class="control-group">
                <?= $form_amount->amount->labelTag ?>
                <div class="controls">
                    <?= $form_amount->amount->error; ?>
                    <?= $form_amount->amount ?>
                    <?= $form_amount->hiddenfields ?>
                    <button type="submit" class="btn">編集</button>
                </div>
            </div>
        <?= form_close() ?>
        <table class="table table-bordered">
            <tr>
                <th width="20%"><?= $type ?>ID</th>
                <td><a href="<?= site_url($prefix.'/detail/'.$id) ?>"><?= $id ?></a></td>
            </tr>
            <tr>
                <th>送信先</th>
                <td><?= $data['email'] ?></td>
            </tr>
            <tr>
                <th>メールタイトル</th>
<?php
/* ご入金確認及びカード決済確認テンプレートでPaypal決済の場合はタイトル切り替え
-------------------------------------------------*/
if (strpos($email_subject, 'ご入金確認') !== false && strpos($data['payment'], 'Paypal') !== false) {
$email_subject = '革水です：カード決済確認のご連絡';
}?>
                <td><?= $email_subject ?></td>
            </tr>
<?php if ($type == '見積もり') { ?>
            <tr>
                <th>添付画像</th>
                <td><?= $images > 0 ? 'あり' : 'なし' ?></td>
            </tr>
<?php } ?>
        </table>
        <ul class="nav nav-tabs">
<?php foreach ($email_template as $key => $name) { ?>
            <li<?= $key == $template_num ? ' class="active"' : '' ?>><a href="<?= site_url('send/index/'.$table.'/'.$id.'/'.$key) ?>"><?= $name ?></a></li>
<?php } ?>
        </ul>
        <?= form_open() ?>
            <textarea name="content" rows="40" class="input-block-level js-lite-editor">
<?php if ($this->input->post('content')) { ?>
<?= $this->input->post('content') ?>
<?php } else { ?>
<?= $this->load->view('send/email_'.$prefix.'_'.$template_num, array('data' => $data, 'items' => $items, 'images' => $images), TRUE) ?>
<?php } ?>
            </textarea>
            <?= form_hidden('send', '1') ?>
            <button type="submit" class="btn btn-success">メール送信</button>
            <a href="<?= site_url($prefix) ?>" class="btn">一覧へ戻る</a>
        <?= form_close() ?>
    </div>
    <div class="span2" id="sidebar">
        <?= $this->load->view('sidebar', null, TRUE) ?>
    </div>
</div>
