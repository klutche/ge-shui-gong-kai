<?if ($this->session->userdata('logged_in')):?>
<aside class="aside-sidebar">
    <nav>
        <ul>
            <li><a href="<?= site_url('member') ?>">会員管理</a></li>
            <li><a href="<?= site_url('member/index/resign') ?>">退会者一覧</a></li>
            <li><a href="<?= site_url('estimate') ?>">見積もり管理</a></li>
            <li><a href="<?= site_url('order') ?>">注文管理</a></li>
            <li><a href="<?= site_url('route') ?>">注文経路一覧</a></li>
            <li><a href="<?= site_url('email') ?>">送信メール履歴</a></li>
            <li><a href="<?= site_url('enquete') ?>">アンケート一覧</a></li>
            <li><a href="<?= site_url('config/index') ?>">設定</a></li>
            <li><a href="<?= site_url('auth/logout') ?>">ログアウト</a></li>
        </ul>
    </nav>
</aside>
<?endif?>
