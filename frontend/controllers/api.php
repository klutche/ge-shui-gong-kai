<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->layout = '__NONE__';
    }

    function index()
    {
        show_404();
    }

    function search_address($zipcode)
    {
        $url = 'http://search.olp.yahooapis.jp/OpenLocalPlatform/V1/zipCodeSearch?';
        $url .= 'appid=' . get_item('yahoo_appid');
        $url .= '&query=' . $zipcode;
        $url .= '&zkind=0,1';
        $url .= '&results=1';
        $url .= '&detail=standard';
        $url .= '&output=json';

        $json = file_get_contents($url);

        $data = json_decode($json);
        if (isset($data->Feature[0])) {
            $ret = array(
                'status' => 'success',
                'address' => $data->Feature[0]->Property->Address
            );
        } else {
            $ret = array(
                'status' => 'failed',
            );
            $this->output->set_status_header('404');
        }

        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($ret));
    }

    public function check_login()
    {
        $this->layout = '__NONE__';

        if ($this->session->userdata('logged_in')) {
            $member = $this->session->userdata('login_member');
            $ret = array(
                'logged_in' => true,
                'member' => array(
                    'name' => $member->name,
                    'email' => $member->email),
                'html' => $this->load->view('api/logged', array('member' => $member), true)
            );
        } else {
            require_once APPPATH . 'forms/MemberLoginForm.php';
            $form = new MemberLoginForm(null, array(
                'redirect_url' => $this->input->get('redirect_url')
            ));
            $ret = array(
                'logged_in' => false,
                'html' => $this->load->view('api/login', array('form' => $form), true)
            );
        }

        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($ret));
    }

    public function get_csrf_cookie()
    {
        $this->layout = '__NONE__';
    }

}
