<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require_once APPPATH . 'libraries/qdmail.php';
require_once APPPATH . 'vendor/autoload.php';

class Baseform extends CI_Controller {

    protected $form_class;
    protected $prefix;
    protected $admin_email_subject;
    protected $thanks_email_subject;

    function __construct()
    {
        parent::__construct();

        $form_class_path = APPPATH . 'forms/' . $this->form_class . '.php';
        if (is_file($form_class_path)) {
            require_once $form_class_path;
        } else {
            show_error('Cannot load file');
        }

    }

    function index()
    {
        redirect($this->prefix . '/input');
    }

    function input()
    {
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $init = null;
            if ($this->session->userdata('logged_in')) {
                $this->load->model('membermodel');
                $login_member = $this->session->userdata('login_member');
                $init = $this->membermodel->get($login_member->id);
            }
            $form = new $this->form_class(null, $init);
            assign('form', $form);
            render($this->prefix . '/input');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new $this->form_class($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render($this->prefix . '/input');
                return;
            }

            $form->preview();
            assign('form', $form);
            render($this->prefix . '/confirm');
        }
    }

    function send()
    {
        $form = new $this->form_class($_POST);
        if (!$form->isValid()) {
            assign('form', $form);
            render($this->prefix . '/input');
            return;
        }

        if ($this->input->post('back')) {
            assign('form', $form);
            render($this->prefix . '/input');
            return;
        }

        $data = $form->cleaned_data;

        $this->email_data['data'] = $data;
/*
        $mail = new Qdmail('UTF-8', 'base64');
        $mail->debug(get_item('email_debug'));
        $mail->from($data['email'], $data['name']);
        $mail->to(get_item('email_to'));
        $mail->bcc(get_item('email_bcc'));
        $mail->subject(sprintf('【%s】%s様', $this->admin_email_subject, $data['name']));
        $mail->text($this->load->view($this->prefix . '/email_admin', $this->email_data, TRUE));
        $mail->send();
*/
		$email = new \SendGrid\Mail\Mail();
		$email->setFrom($data['email'], $data['name']);
		$email->setSubject(sprintf('【%s】%s様', $this->admin_email_subject, $data['name']));
		$email->addTo(get_item('email_to'));
		$email->addBcc(get_item('email_bcc'));
		$email->addContent("text/plain", $this->load->view($this->prefix . '/email_admin', $this->email_data, TRUE));
		$tracking_settings = new \SendGrid\Mail\TrackingSettings();
		$tracking_settings->setOpenTracking(
		    new \SendGrid\Mail\OpenTracking(false, null)
		);
		$email->setTrackingSettings($tracking_settings);
		$sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
		$sendgrid->send($email);

/*
        $mail = new Qdmail('UTF-8', 'base64');
        $mail->debug(get_item('email_debug'));
        $mail->from(get_item('email_sender'));
        $mail->to($data['email']);
        $mail->subject($this->thanks_email_subject);
        $mail->text($this->load->view($this->prefix . '/email_thanks', $this->email_data, TRUE));
        $mail->send();
*/
		$email = new \SendGrid\Mail\Mail();
		$email->setFrom(get_item('email_sender'));
		$email->setSubject($this->thanks_email_subject);
		$email->addTo($data['email']);
		$email->addContent("text/plain", $this->load->view($this->prefix . '/email_thanks', $this->email_data, TRUE));
		$tracking_settings = new \SendGrid\Mail\TrackingSettings();
		$tracking_settings->setOpenTracking(
		    new \SendGrid\Mail\OpenTracking(false, null)
		);
		$email->setTrackingSettings($tracking_settings);
		$sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
		$sendgrid->send($email);


        if (strpos($data['email'], '@gmail.com') !== false) {
            $this->session->set_flashdata('use_gmail', true);
        }
        redirect($this->prefix . '/finish');

    }

    function finish()
    {
        render($this->prefix . '/finish');
    }

}
