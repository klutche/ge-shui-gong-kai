<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require_once APPPATH . 'libraries/qdmail.php';
require_once APPPATH . 'vendor/autoload.php';
require_once APPPATH . 'forms/MemberLoginForm.php';

class Care extends CI_Controller {

    protected $admin_email_subject;
    protected $thanks_email_subject;

    function __construct()
    {
        parent::__construct();
        
        redirect('https://www.kawasui.com/news/4172.html', 'location', 301);
        return;

        assign('scripts', array('assets/js/care.js'));

        $this->load->model('membermodel');
        $this->load->model('pickupmodel');

        $login_form = new MemberLoginForm();
        assign('login_form', $login_form);
        assign('title', 'お手入れクリーニングご注文フォーム');

   }

    function index()
    {
        redirect('care/input');
    }

    function input()
    {
        $logged_in = $this->session->userdata('logged_in');

        assign('logged_in', $logged_in);

        $member = null;
        $pickups = array();

        if ($logged_in) {
            $_member = $this->session->userdata('login_member');

            // 最新データを取得
            $member = $this->membermodel->get($_member->id);
            if (strlen($member->name) == 0) {
                redirect('member/edit');
                return;
            }

            // step2
            $step2 = array(
                'name' => $member->name,
                'kana' => $member->kana,
                'zipcode' => $member->zipcode,
                'address' => $member->address,
                'tel' => $member->tel,
                'email' => $member->email
            );

            $this->session->set_userdata('care_step_2', $step2);

            $pickups = $this->pickupmodel->search(array(
                'member_id' => $member->id
            ), 0, 100, 'id desc');

            if (count($pickups) == 0) {
                redirect('pickup/add');
                return;
            }

            $step3 = null;
            if ($pickups) {
                $step3 = $pickups[0];
            }

            $this->session->set_userdata('care_step_3', $step3);
            $this->session->set_userdata('pickups', $pickups);
            $this->session->set_userdata('selected_pickup', $step3->id);
        }
        assign('member', $member);
        render('care/input');

    }

    function partial($step)
    {
        $this->layout = '__NONE__';

        $step = (int) $step;

        $editable = $this->input->get('editable');

        $form_class = 'CareStep' . $step . 'Form';

        $form_class_path = APPPATH . 'forms/' . $form_class . '.php';
        if (is_file($form_class_path)) {
            require_once $form_class_path;
        } else {
            show_error('Cannot load file');
        }


        $data = array();
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            if ($editable) {
                $inputed = null;

                $session_inputed = $this->session->userdata('care_step_' . $step);
                if ($session_inputed) {
                    $inputed = $session_inputed;
                }

                $form = new $form_class(null, $inputed);
                $data = array(
                    'form' => $form
                );
                $status = 'input';
                $template = 'care/partial_step_' . $step;
            } else {
                $status = 'bound';
                $template = 'care/partial_step_' . $step . '_bound';
            }
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new $form_class($_POST);
            if (!$form->isValid()) {
                $data = array(
                    'form' => $form
                );
                $status = 'input';
                $template = 'care/partial_step_' . $step;
            } else {
                $this->session->set_userdata('care_step_' . $step, $form->cleaned_data);
                $status = 'bound';
                $template = 'care/partial_step_' . $step . '_bound';
            }
        }

        $data['input'] = $this->session->userdata('care_step_' . $step);
        $ret = array(
            'status' => $status,
            'url' => site_url('care/partial_step_' . $step),
            'html' => $this->load->view($template, $data, true)
        );
        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($ret));
    }

    function send()
    {
        $this->admin_email_subject = 'お手入れクリーニング注文';
        $this->thanks_email_subject = sprintf('【%s】お手入れクリーニングご注文の確認', get_item('site_name'));

        for ($i = 1; $i < 6; $i++) {
            $this->email_data['step' . $i] = $this->session->userdata('care_step_' . $i);
        }

        $this->email_data['is_member'] = $this->session->userdata('logged_in');

        $member_subject = '';
        if ($this->email_data['is_member']) {
            $member_subject = ' (会員)';
        }
/*
        $mail = new Qdmail('UTF-8', 'base64');
        $mail->debug(get_item('email_debug'));
        $mail->from($this->email_data['step2']['email'], $this->email_data['step2']['name']);
        $mail->to(get_item('email_order_to'));
        $mail->bcc(get_item('email_bcc'));
        $mail->subject(sprintf('【%s】%s様', $this->admin_email_subject, $this->email_data['step2']['name'] . $member_subject));
        $mail->text($this->load->view('email/care_admin', $this->email_data, TRUE));
        $mail->send();
*/
		$email = new \SendGrid\Mail\Mail();
		$email->setFrom($this->email_data['step2']['email'], $this->email_data['step2']['name']);
		$email->setSubject(sprintf('【%s】%s様', $this->admin_email_subject, $this->email_data['step2']['name'] . $member_subject));
		$email->addTo(get_item('email_order_to'));
		$email->addBcc(get_item('email_bcc'));
		$email->addContent("text/plain", $this->load->view('email/care_admin', $this->email_data, TRUE));
		$tracking_settings = new \SendGrid\Mail\TrackingSettings();
		$tracking_settings->setOpenTracking(
		    new \SendGrid\Mail\OpenTracking(false, null)
		);
		$email->setTrackingSettings($tracking_settings);
		$sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
		$sendgrid->send($email);
/*
        $mail = new Qdmail('UTF-8', 'base64');
        $mail->debug(get_item('email_debug'));
        $mail->from(get_item('email_sender'));
        $mail->to($this->email_data['step2']['email']);
        $mail->subject($this->thanks_email_subject);
        $mail->text($this->load->view('email/care_thanks', $this->email_data, TRUE));
        $mail->send();
*/
		$email = new \SendGrid\Mail\Mail();
		$email->setFrom(get_item('email_sender'));
		$email->setSubject($this->thanks_email_subject);
		$email->addTo($this->email_data['step2']['email']);
		$email->addContent("text/plain", $this->load->view('email/care_thanks', $this->email_data, TRUE));
		$tracking_settings = new \SendGrid\Mail\TrackingSettings();
		$tracking_settings->setOpenTracking(
		    new \SendGrid\Mail\OpenTracking(false, null)
		);
		$email->setTrackingSettings($tracking_settings);
		$sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
		$sendgrid->send($email);

        // 新しい集配先を登録
        if ($this->session->userdata('logged_in') && $this->session->userdata('selected_pickup') == 'add') {
            $data = $this->session->userdata('care_step_3');

            // 集荷先から不要なデータを削除
            $unset_keys = array('method', 'date', 'time', 'box');
            foreach ($unset_keys as $key) {
                unset($data[$key]);
            }

            $member = $this->session->userdata('login_member');
            $data['member_id'] = $member->id;

            $this->load->model('pickupmodel');
            $this->pickupmodel->save($data);
        }

        if (strpos($this->email_data['step2']['email'], '@gmail.com') !== false) {
            $this->session->set_flashdata('use_gmail', true);
        }
        redirect('care/finish');

    }

    function finish()
    {
        $logged_in = $this->session->userdata('logged_in');

        render('care/finish');

        /*
        if ($logged_in) {
            render('care/finish');
        } else {
            render('care/finish_and_register');
        }
         */
    }

    function get_step2_data()
    {
        $this->layout = '__NONE__';

        $step2 = $this->session->userdata('care_step_2');

        $ret = array(
            'data' => $step2
        );

        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($ret));

    }

    function get_step3_data()
    {
        $this->layout = '__NONE__';
        $pickups = $this->session->userdata('pickups');
        $selected_pickup = $this->input->get('id');

        $step3 = null;
        foreach ($pickups as $pickup) {
            if ($pickup->id == $selected_pickup) {
                $step3 = $pickup;
                break;
            }
        }

        $this->session->set_userdata('care_step_3', $step3);
        $this->session->set_userdata('selected_pickup', $selected_pickup);

        $ret = array(
            'data' => $step3
        );
        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($ret));

    }

}

