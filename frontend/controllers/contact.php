<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/baseform.php';

class Contact extends Baseform {

    protected $form_class = 'ContactForm';
    protected $admin_email_subject = 'お問い合わせ';
    protected $thanks_email_subject = '【革水】お問い合わせのご確認';
    protected $email_data = array();

    public function __construct()
    {
        parent::__construct();
        $this->prefix = strtolower(__CLASS__);

        assign('title', 'お問い合わせ');
    }
}

