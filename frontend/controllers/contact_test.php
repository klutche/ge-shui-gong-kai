<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/baseform.php';

class Contact_test extends Baseform {

    protected $form_class = 'ContactForm';
    protected $admin_email_subject = 'お問い合わせ';
    protected $thanks_email_subject = '【革水】お問い合わせのご確認';
    protected $email_data = array();

    public function __construct()
    {
        parent::__construct();
        $this->prefix = strtolower(__CLASS__);

        assign('title', 'お問い合わせ');
    }
    
        function index()
    {
        redirect($this->prefix . '/input');
    }

    function input()
    {
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $init = null;
            if ($this->session->userdata('logged_in')) {
                $this->load->model('membermodel');
                $login_member = $this->session->userdata('login_member');
                $init = $this->membermodel->get($login_member->id);
            }
            $form = new $this->form_class(null, $init);
            assign('form', $form);
            render($this->prefix . '/input');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new $this->form_class($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render($this->prefix . '/input');
                return;
            }

            $form->preview();
            assign('form', $form);
            render($this->prefix . '/confirm');
        }
    }

    function send()
    {
        $form = new $this->form_class($_POST);
        if (!$form->isValid()) {
            assign('form', $form);
            render($this->prefix . '/input');
            return;
        }

        if ($this->input->post('back')) {
            assign('form', $form);
            render($this->prefix . '/input');
            return;
        }

        $data = $form->cleaned_data;

        $this->email_data['data'] = $data;

        $mails = array(
        'abfactory.ogata@gmail.com',
'abfactory.tsuchida@gmail.com',
'tsuchida.y@gmail.com',
'manhattan9@gmail.com',
'kawasui.test@gmail.com',
);

        
        $mail = new Qdmail('UTF-8', 'base64');
        $mail->debug(get_item('email_debug'));
        $mail->from($data['email'], $data['name']);
        $mail->to(get_item('email_to'));
        $mail->bcc(get_item('email_bcc'));
        $mail->subject(sprintf('【%s】%s様', $this->admin_email_subject, $data['name']));
        $mail->text($this->load->view($this->prefix . '/email_admin', $this->email_data, TRUE));
//        $mail->send();


foreach ($mails as $to) {
        $mail = new Qdmail('UTF-8', 'base64');
        $mail->debug(1);
        $mail->from(get_item('email_sender'));
        $mail->to($to);
        $mail->subject($this->thanks_email_subject);
        $mail->text($this->load->view($this->prefix . '/email_thanks', $this->email_data, TRUE));
        $mail->send();
        }

        //redirect($this->prefix . '/finish');

    }

    function finish()
    {
        render($this->prefix . '/finish');
    }

}

