<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/baseform.php';

class Estimate extends Baseform {
    
    protected $form_class = 'EstimateForm';
    protected $admin_email_subject = 'クリーニング・修理品無料見積もり';
    protected $thanks_email_subject = '【革水】クリーニング・修理品無料見積もりのご確認';
    protected $email_data = array();
    
    /*=====================================================
    コンストラクタ
    =====================================================*/
    public function __construct()
    {
        parent::__construct();
        $this->prefix = strtolower(__CLASS__);
        assign('scripts', array('assets/js/estimate.js'));
        assign('title', 'クリーニング・修理品無料見積もり');
    }
    
    /*=====================================================
    inputページ
    =====================================================*/
    function input()
    {
        /* フォーム生成
        -------------------------------------------------*/
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $init = null;
            if ($this->session->userdata('logged_in')) {
                $this->load->model('membermodel');
                $login_member = $this->session->userdata('login_member');
                $init = get_object_vars($this->membermodel->get($login_member->id));
            }
            $form = new $this->form_class(null, $init);
            assign('form', $form);
            render($this->prefix.'/input');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new $this->form_class($_POST);
            if ($form->isValid()) {
                if ($_FILES['upload']['name'] == ''){
                    assign('form', $form);
                    assign('upload_error', '画像ファイルを選択してください。');
                    render($this->prefix.'/input');
                }else{
                if ($_FILES['upload']['error'] != 4) {
                    $config = array(
                        'upload_path' => FCPATH.'/uploads/',
                        'allowed_types' => 'jpg|jpeg',
                        'max_size' => 5120,
                        'encrypt_name' => true
                    );
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('upload')) {
                        assign('upload_error', $this->upload->display_errors());
                    }
                    $upload_data = $this->upload->data();
                    $config = array(
                        'image_library' => 'gd2',
                        'source_image' => $upload_data['full_path'],
                        'maintain_ratio' => true,
                        'width' => 2400,
                        'height' => 1800
                    );
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    assign('upload_filename', $upload_data['file_name']);
                    assign('upload_file_url', site_url('/uploads/'.$upload_data['file_name']));
                }
                $form->preview();
                assign('form', $form);
                render($this->prefix.'/confirm');
                }
            } else {
                assign('form', $form);
                if ($_FILES['upload']['name'] == ''){
                    assign('upload_error', '画像ファイルを選択してください。');
                }
                render($this->prefix.'/input');
            }
        }
    }

    /*=====================================================
    sendページ
    =====================================================*/
    function send()
    {
        $form = new $this->form_class($_POST);
        if (!$form->isValid()) {
            assign('form', $form);
            render($this->prefix.'/input');
            return;
        }

        if ($this->input->post('back')) {
            assign('form', $form);
            render($this->prefix.'/input');
            return;
        }

        $data = $form->cleaned_data;
        $this->email_data['data'] = $data;
        
        /* データベースに登録
        -------------------------------------------------*/
        // 見積もり情報の登録
        $estimate = array(
            'estimate_at' => date('Y-m-d H:i:s'),
            'name' => $data['name'],
            'kana' => $data['kana'],
            'email' => $data['email'],
            'tel' => $data['tel'],
            'prefcode' => $data['prefcode'],
            'is_expensive' => $data['is_expensive'],
            'pickup_boxes' => $data['pickup_boxes'],
            'remarks' => $data['message'],
        );
        $ret = $this->db->from('estimates')
            ->set($estimate)
            ->insert();
        if (!$ret) {
            throw new MemberRegisterException('見積もり情報の登録に失敗しました。');
        }
        $this->insert_id = $this->db->insert_id();
        $this->email_data['data']['id'] = $this->insert_id;
        
        // 見積もり品目の登録
        $items_data = array();
        for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
            if (strlen($data['item_'.$i]) > 0) {
                $items_data = array(
                    'estimate_id' => $this->insert_id,
                    'item' => $data['item_'.$i] == 'その他' ? $data['item_etc_'.$i] : $data['item_'.$i],
                    'quantity' => $data['item_num_'.$i],
                );
                $ret = $this->db->from('estimate_items')
                    ->set($items_data)
                    ->insert();
                if (!$ret) {
                    throw new MemberRegisterException('見積もり品目の登録に失敗しました。');
                }
            }
        }
        
        // アップロード画像の登録
        if (isset($data['upload']) && strlen($data['upload']) && is_file(FCPATH.'/uploads/'.$data['upload'])) {
            $estimate_images_data = array(
                'estimate_id' => $this->insert_id,
                'path' => $data['upload'],
            );
            $ret = $this->db->from('estimate_images')
                ->set($estimate_images_data)
                ->insert();
            if (!$ret) {
                throw new MemberRegisterException('アップロード画像の登録に失敗しました。');
            }
        }

        
        /* 管理者にメールを送信
        -------------------------------------------------*/
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($data['email'], $data['name']);
        $email->setSubject(sprintf('【%s】%s様', $this->admin_email_subject, $data['name']));
        $email->addTo(get_item('email_order_to'));
        $email->addContent("text/plain", $this->load->view($this->prefix.'/email_admin', $this->email_data, TRUE));
        if (isset($data['upload']) && strlen($data['upload']) && is_file(FCPATH.'/uploads/'.$data['upload'])) {
            $file_encoded = base64_encode(file_get_contents(FCPATH.'/uploads/'.$data['upload']));
            $email->addAttachment(
                $file_encoded,
                "application/text",
                $data['upload'],
                "attachment"
            );
        }
        $tracking_settings = new \SendGrid\Mail\TrackingSettings();
        $tracking_settings->setOpenTracking(
            new \SendGrid\Mail\OpenTracking(false, null)
        );
        $email->setTrackingSettings($tracking_settings);
        $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
        $sendgrid->send($email);

        /* ユーザーにメールを送信
        -------------------------------------------------*/
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom(get_item('email_sender'));
        $email->setSubject($this->thanks_email_subject);
        $email->addTo($data['email']);
        $email->addContent("text/plain", $this->load->view($this->prefix.'/email_thanks', $this->email_data, TRUE));
        $tracking_settings = new \SendGrid\Mail\TrackingSettings();
        $tracking_settings->setOpenTracking(
            new \SendGrid\Mail\OpenTracking(false, null)
        );
        $email->setTrackingSettings($tracking_settings);
        $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
        $sendgrid->send($email);

        if (strpos($data['email'], '@gmail.com') !== false) {
            $this->session->set_flashdata('use_gmail', true);
        }
        redirect($this->prefix.'/finish');

    }
}
