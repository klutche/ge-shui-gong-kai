<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'forms/MemberLoginForm.php';

class Home extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        if (preg_match('/iphone|android/i', $this->input->server('HTTP_USER_AGENT'))) {
            $login_form = new MemberLoginForm();
            assign('login_form', $login_form);
            assign('title', '革水');
            render('home');
        } else {
            redirect('https://' . $this->input->server('HTTP_HOST') . '/');
            return;
        }
    }


}
