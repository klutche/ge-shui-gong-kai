<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'forms/MemberSignupForm.php';
require_once APPPATH . 'forms/MemberLoginForm.php';
require_once APPPATH . 'forms/MemberUpdatePasswordForm.php';
require_once APPPATH . 'forms/MemberForgotPasswordForm.php';
require_once APPPATH . 'forms/MemberResignForm.php';
require_once APPPATH . 'forms/MemberForm.php';
require_once APPPATH . 'forms/MemberFullForm.php';
require_once APPPATH . 'forms/MemberUpdateEmailForm.php';
require_once APPPATH . 'forms/MemberVerifyEmailForm.php';
require_once APPPATH . 'vendor/autoload.php';

class Member extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->model('membermodel');
    }

    private function filter_auth()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('member/login');
            exit;
        }
    }
    
    public function index()
    {
        show_404();
        /*
        $this->filter_auth();
        assign('title', '会員メニュー');
        render('member/index');
         */
    }
    
    public function signup()
    {
        if ($this->session->userdata('logged_in')) {
            redirect('member/edit');
        }
        
        assign('is_static', true);
        assign('title', '会員登録');

        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $form = new MemberSignupForm();
            assign('form', $form);
            render('member/signup');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new MemberSignupForm($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render('member/signup');
                return;
            }
            
            $data = $form->cleaned_data;
            
            try {
                $is_active = 0;
                $member = $this->membermodel->getByUsername($data['email'], $is_active);
                
                // 仮登録
                $this->membermodel->registerTemporary($data['email'], $data['password']);
                
                // 再取得
                $is_active = 0;
                $member = $this->membermodel->getByUsername($data['email'], $is_active);
                
                $base_url = get_item('ssl_url');
/*
                // メール送信
                $qdmail = new Qdmail('UTF-8', 'Base64');
                $qdmail->debug(get_item('email_debug'));
                $qdmail->to($data['email']);
                $qdmail->from(get_item('email_sender'));
                $qdmail->subject(sprintf('【%s】会員登録の承認', get_item('site_name')));
                $qdmail->text($this->load->view('email/member_signup', array(
                    'member' => $member,
                    'email_info' => get_item('info_mail'),
                    'url' => $this->membermodel->generateVerifyUrl($base_url),
                ), TRUE));
                $qdmail->send();
*/
                $email = new \SendGrid\Mail\Mail();
                $email->setFrom(get_item('email_sender'), get_item('site_name'));
                $email->setSubject(sprintf('【%s】会員登録の承認', get_item('site_name')));
                $email->addTo($data['email']);
                $email->addContent("text/plain", $this->load->view('email/member_signup', array(
                    'member' => $member,
                    'email_info' => get_item('info_mail'),
                    'url' => $this->membermodel->generateVerifyUrl($base_url),
                ), TRUE));
                $tracking_settings = new \SendGrid\Mail\TrackingSettings();
                $tracking_settings->setOpenTracking(
                    new \SendGrid\Mail\OpenTracking(false, null)
                );
                $email->setTrackingSettings($tracking_settings);
                $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
                $sendgrid->send($email);
            } catch (Exception $e) {
                assign('form', $form);
                assign('error', $e->getMessage());
                render('member/signup');
                return;
            }

            redirect('member/signuped');
        }
    }

    public function signup_after($from)
    {
        if ($this->session->userdata('logged_in')) {
            redirect('');
        }

        assign('title', '会員登録');

        switch ($from) {
        case 'order':
            $member_key = 'order_step_2';
            $pickup_key = 'order_step_3';
            break;
        case 'care':
            $member_key = 'care_step_2';
            $pickup_key = 'care_step_3';
            break;
        case 'washstyle':
            $member_key = 'washstyle_step_1';
            $pickup_key = 'washstyle_step_2';
            break;
        }

        // 会員情報
        $member_data = $this->session->userdata($member_key);

        // 初期パスワード
        $this->load->helper('string');
        $initial_password = random_string('alnum', 8);
        $member_data['password'] = $initial_password;

        $pickup_data = $this->session->userdata($pickup_key);

        // 集荷先から不要なデータを削除
        $isset_keys = array('name', 'zipcode', 'address', 'tel');
        foreach ($pickup_data as $key => $val) {
            if (!in_array($key, $isset_keys)) {
                unset($pickup_data[$key]);
            }
        }

        try {
            // 重複チェック
            $is_active = null;
            $member = $this->membermodel->getByUsername($member_data['email'], $is_active);

            if ($member) {
                throw new MemberDuplicateException('このメールアドレスはすでに登録されています。');
            }

            // 仮登録
            $this->membermodel->registerTemporary($member_data['email'], $member_data['password']);
            $member_id = $this->membermodel->insert_id;

            // 注文時の顧客情報を登録
            $_member_data = array();
            $save_keys = array('name', 'kana', 'zipcode', 'address', 'tel');
            foreach ($save_keys as $key) {
                $_member_data[$key] = isset($member_data[$key]) ? $member_data[$key] : null;
            }

            $this->membermodel->save($_member_data, $member_id);

            // 集荷先を登録
            $this->load->model('pickupmodel');
            $pickup_data['member_id'] = $member_id;
            $this->pickupmodel->save($pickup_data);

            $base_url = get_item('ssl_url');

            // 最新データを取得
            $is_active = 0;
            $member = $this->membermodel->getByUsername($member_data['email'], $is_active);
/*
            // メール送信
            $qdmail = new Qdmail('UTF-8', 'Base64');
            $qdmail->debug(get_item('email_debug'));
            $qdmail->to($member_data['email']);
            $qdmail->from(get_item('email_sender'));
            $qdmail->subject(sprintf('【%s】会員登録の承認', get_item('site_name')));
            $qdmail->text($this->load->view('email/member_signup', array(
                'member' => $member,
                'email_info' => get_item('info_mail'),
                'initial_password' => $initial_password,
                'url' => $this->membermodel->generateVerifyUrl($base_url),
            ), TRUE));
            $qdmail->send();
*/
            $email = new \SendGrid\Mail\Mail();
            $email->setFrom(get_item('email_sender'), get_item('site_name'));
            $email->setSubject(sprintf('【%s】会員登録の承認', get_item('site_name')));
            $email->addTo($member_data['email']);
            $email->addContent("text/plain", $this->load->view('email/member_signup', array(
                'member' => $member,
                'email_info' => get_item('info_mail'),
                'initial_password' => $initial_password,
                'url' => $this->membermodel->generateVerifyUrl($base_url),
            ), TRUE));
            $tracking_settings = new \SendGrid\Mail\TrackingSettings();
            $tracking_settings->setOpenTracking(
                new \SendGrid\Mail\OpenTracking(false, null)
            );
            $email->setTrackingSettings($tracking_settings);
            $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
            $sendgrid->send($email);

        } catch (Exception $e) {
            assign('error', $e->getMessage());
            render('member/error');
            return;
        }

        redirect('member/signuped');
    }

    public function signuped()
    {
        assign('title', '会員登録の承認メールを送信しました');
        render('member/signuped');
    }

    public function verify($id, $hash)
    {
        if (!isset($id) || !isset($hash) || strlen($id) == 0 || strlen($hash) == 0) {
            show_404();
            return;
        }

        assign('title', '会員登録の承認');

        $is_active = 0;
        $member = $this->membermodel->get($id, $is_active);
        if (!$member) {
            render('member/verify_failed');
            return;
        }

        if (!$this->membermodel->verify($hash)) {
            render('member/verify_failed');
            return;
        }
/*
        // メール送信
        $qdmail = new Qdmail('UTF-8', 'Base64');
        $qdmail->debug(get_item('email_debug'));
        $qdmail->to($member->email);
        $qdmail->from(get_item('email_sender'));
        $qdmail->subject(sprintf('【%s】会員登録ありがとうございます', get_item('site_name')));
        $qdmail->text($this->load->view('email/member_thanks', array(
            'member' => $member,
            'email_info' => get_item('info_mail'),
        ), TRUE));
        $qdmail->send();
*/
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom(get_item('email_sender'), get_item('site_name'));
        $email->setSubject(sprintf('【%s】会員登録ありがとうございます', get_item('site_name')));
        $email->addTo($member->email);
        $email->addContent("text/plain", $this->load->view('email/member_thanks', array(
            'member' => $member,
            'email_info' => get_item('info_mail'),
        ), TRUE));
        $tracking_settings = new \SendGrid\Mail\TrackingSettings();
        $tracking_settings->setOpenTracking(
            new \SendGrid\Mail\OpenTracking(false, null)
        );
        $email->setTrackingSettings($tracking_settings);
        $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
        $sendgrid->send($email);

        // メール送信
        // $qdmail = new Qdmail('UTF-8', 'Base64');
        // $qdmail->debug(get_item('email_debug'));
        // $qdmail->to(get_item('email_member_to'));
        // $qdmail->bcc(get_item('email_bcc'));
        // $qdmail->from($member->email);
        // $qdmail->subject(sprintf('【%s】会員登録(仮登録)', get_item('site_name')));
        // $qdmail->text($this->load->view('email/member_admin', array(
        //     'member' => $member,
        //     'email_info' => get_item('info_mail'),
        // ), TRUE));
        // $qdmail->send();

        redirect('member/verified');
    }

    public function verified()
    {
        assign('title', '会員登録の承認');
        $form = new MemberLoginForm();
        assign('form', $form);
        render('member/verified');
    }

    public function login()
    {
        if ($this->session->userdata('logged_in')) {
            redirect('');
        }

        assign('is_static', true);
        assign('title', '会員ログイン');

        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            // リダイレクト先
            $ref = $this->input->get('ref');
            if (!preg_match('/^\/(order|care)\/input$/', $ref)) {
                $ref = false;
            }
            $data = array();
            if ($ref) {
                $data = array(
                    'redirect_url' => $ref
                );
            }
            $form = new MemberLoginForm(NULL, $data);
            assign('form', $form);
            render('member/login');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new MemberLoginForm($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render('member/login');
                return;
            }

            $data = $form->cleaned_data;

            if ($this->membermodel->auth($data['username'], $data['password']) === FALSE) {
                assign('form', $form);
                assign('error', 'ログインできません。');
                render('member/login');
                return;
            }

            $member = $this->membermodel->getSelf();

            $this->session->set_userdata('logged_in', TRUE);
            $this->session->set_userdata('login_member', $member);

            $this->membermodel->updateLoginedAt();

            // お客様情報が未入力なら登録画面へリダイレクト
            if (strlen($member->name) == 0) {
                $this->session->set_userdata('from_order', true);
                redirect('member/edit');
                return;
            }

            if (isset($data['redirect_url']) && strlen($data['redirect_url']) > 0) {
                redirect($data['redirect_url']);
                // $parsed_url = parse_url($data['redirect_url']);
                // if ($parsed_url['host'] === $this->input->server('HTTP_HOST')) {
                //     redirect($data['redirect_url']);
                // } else {
                //     redirect('');
                // }
                return;
            } else {
                redirect('');
                return;
            }
        }
    }

    public function forgot_password()
    {
        assign('is_static', true);
        assign('title', 'パスワードを忘れた方へ');

        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $ref = $this->input->server('HTTP_REFERER');
            $form = new MemberForgotPasswordForm();
            assign('form', $form);
            render('member/forgot_password');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new MemberForgotPasswordForm($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render('member/forgot_password');
                return;
            }

            $data = $form->cleaned_data;

            $is_active = 1;
            $member = $this->membermodel->getByUsername($data['username'], $is_active);

            if (!$member) {
                assign('form', $form);
                assign('error', '該当データが見つかりません。');
                render('member/forgot_password');
                return;
            }
/*
            // パスワードを再送信
            $qdmail = new Qdmail('UTF-8', 'Base64');
            $qdmail->debug(get_item('email_debug'));
            $qdmail->to($member->email);
            $qdmail->from(get_item('email_sender'));
            $qdmail->subject(sprintf('【%s】パスワードの再送信', get_item('site_name')));
            $qdmail->text($this->load->view('email/member_forgot_password', array(
                'member' => $member,
                'email_info' => get_item('info_mail')
            ), TRUE));
            $qdmail->send();
*/
            $base_url = get_item('ssl_url');

            $email = new \SendGrid\Mail\Mail();
            $email->setFrom(get_item('email_sender'), get_item('site_name'));
            $email->setSubject(sprintf('【%s】パスワードの再送信', get_item('site_name')));
            $email->addTo($member->email);
            $email->addContent("text/plain", $this->load->view('email/member_forgot_password', array(
                'member' => $member,
                'email_info' => get_item('info_mail'),
                'url' => $this->membermodel->generateVerifyUrl($base_url),
            ), TRUE));
            $tracking_settings = new \SendGrid\Mail\TrackingSettings();
            $tracking_settings->setOpenTracking(
                new \SendGrid\Mail\OpenTracking(false, null)
            );
            $email->setTrackingSettings($tracking_settings);
            $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
            $sendgrid->send($email);

            redirect('member/resend_password');

        }
    }

    public function resend_password()
    {
        assign('title', 'パスワードを再送信しました');
        render('member/resend_password');
    }

    public function update_password()
    {
        $this->filter_auth();

        assign('is_static', true);
        assign('title', 'パスワードの変更');

        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $form = new MemberUpdatePasswordForm();
            assign('form', $form);
            render('member/update_password');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new MemberUpdatePasswordForm($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render('member/update_password');
                return;
            }

            $data = $form->cleaned_data;

            $sess_member = $this->session->userdata('login_member');

            if (!$sess_member) {
                assign('form', $form);
                assign('error', '該当データが見つかりません。');
                render('member/update_password');
                return;
            }


            // 最新データを取得
            $member = $this->membermodel->get($sess_member->id);

            if (!$member) {
                assign('form', $form);
                assign('error', '該当データが見つかりません。');
                render('member/update_password');
                return;
            }

            if (!$this->membermodel->updatePassword($data['new_password'], $data['old_password'])) {
                assign('form', $form);
                assign('error', 'パスワードが変更できません。現在のパスワードが間違っていないか、ご確認ください。');
                render('member/update_password');
                return;
            }

            redirect('member/updated_password');

        }
    }

    public function updated_password()
    {
        $this->filter_auth();

        assign('title', 'パスワードの変更');
        render('member/updated_password');
    }

    /*=====================================================
    resignページ
    =====================================================*/
    public function resign()
    {
        $this->filter_auth();

        /* タイトル
        -------------------------------------------------*/
        assign('title', '退会');

        /* フォーム生成
        -------------------------------------------------*/
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $form = new MemberResignForm();
            assign('form', $form);
            render('member/resign');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new MemberResignForm($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render('member/resign');
                return;
            }

            $data = $form->cleaned_data;

            $sess_member = $this->session->userdata('login_member');

            if (!$sess_member) {
                assign('form', $form);
                assign('error', '該当データが見つかりません。');
                render('member/resign');
                return;
            }

            // 最新データを取得
            $member = $this->membermodel->get($sess_member->id);

            if (!$member) {
                assign('form', $form);
                assign('error', '該当データが見つかりません。');
                render('member/resign');
                return;
            }

            if (!$this->membermodel->resign($data['password'], $data['reason'])) {
                assign('form', $form);
                assign('error', '退会処理ができません。パスワードが間違っていないか、ご確認ください。');
                render('member/resign');
                return;
            }

            $member = $this->membermodel->get($sess_member->id, 0);
            
            // 退会者データベースにコピー
            $set_data = array(
                'id' => $member->id,
                'email' => $member->email,
                'password' => $member->password,
                'hash' => $member->hash,
                'is_verify' => $member->is_verify,
                'verified_at' => $member->verified_at,
                'logined_at' => $member->logined_at,
                'is_active' => $member->is_active,
                'name' => $member->name,
                'kana' => $member->kana,
                'zipcode' => $member->zipcode,
                'address' => $member->address,
                'tel' => $member->tel,
                'remarks' => $member->remarks,
                'resign_reason' => $member->resign_reason,
                'created_at' => $member->created_at,
                'updated_at' => $member->updated_at,
                'resigned_at' => $member->resigned_at,
                'last_ordered_at' => $member->last_ordered_at,
            );
            $this->db
                ->set($set_data)
                ->insert('resign_members');
            
            // ユーザーにメール送信
            $email = new \SendGrid\Mail\Mail();
            $email->setFrom(get_item('email_sender'), get_item('site_name'));
            $email->setSubject(sprintf('【%s】退会のご確認', get_item('site_name')));
            $email->addTo($member->email);
            $email->addContent("text/plain", $this->load->view('email/member_resign_thanks', array(
                'member' => $member,
                'email_info' => get_item('info_mail'),
            ), TRUE));
            $tracking_settings = new \SendGrid\Mail\TrackingSettings();
            $tracking_settings->setOpenTracking(
                new \SendGrid\Mail\OpenTracking(false, null)
            );
            $email->setTrackingSettings($tracking_settings);
            $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
            $sendgrid->send($email);
            
            // 管理者にメール送信
            $email = new \SendGrid\Mail\Mail();
            $email->setFrom($member->email, $member->name);
            $email->setSubject(sprintf('【%s】退会', get_item('site_name')));
            $email->addTo(get_item('email_member_to'));
            $email->addBcc(get_item('email_bcc'));
            $email->addContent("text/plain", $this->load->view('email/member_resign_admin', array(
                'member' => $member,
                'email_info' => get_item('info_mail'),
            ), TRUE));
            $tracking_settings = new \SendGrid\Mail\TrackingSettings();
            $tracking_settings->setOpenTracking(
                new \SendGrid\Mail\OpenTracking(false, null)
            );
            $email->setTrackingSettings($tracking_settings);
            $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
            $sendgrid->send($email);

            // 会員・配送先データを削除
            $this->load->model('pickupmodel');
            $this->db->trans_start();
            $this->pickupmodel->deleteByMemberId($member->id);
            $this->membermodel->delete($member->id);
            $this->db->trans_complete();

            // リダイレクト
            redirect('member/resigned');
        }
    }

    public function resigned()
    {
        $this->filter_auth();

        assign('title', '退会');
        render('member/resigned');
    }


    public function logout()
    {
        $this->filter_auth();

        $this->session->sess_destroy();
        redirect('');
    }

    public function edit()
    {
        assign('is_static', true);
        assign('title', 'お客様情報の変更');
        $this->filter_auth();
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $login_member = $this->session->userdata('login_member');
            $member = $this->membermodel->get($login_member->id);

            if ($member->name == "") {
                assign('title', 'お客様情報の登録');
            }

            $form = new MemberForm(null, $member);
            assign('member', $member);
            assign('form', $form);
            render('member/edit');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new MemberForm($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render('member/edit');
                return;
            }

            $data = $form->cleaned_data;
            $member = $this->session->userdata('login_member');

            // 初回登録かチェック
            $cur_member = $this->membermodel->get($member->id);
            if (strlen($cur_member->name) == 0) {
                $this->session->set_userdata('first_registered', true);
            }

            if (!$this->membermodel->save($data, $member->id)) {
                notifier('failed', '更新に失敗しました。');
                redirect('member/edit');
                return;
            }

            // セッション上の会員情報を更新
            $cur_member = $this->membermodel->get($member->id);
            $this->session->set_userdata('login_member', $cur_member);

            notifier('success', 'お客様情報を更新しました。');

            $this->load->model('pickupmodel');

            $pickups = $this->pickupmodel->search(array(
                'member_id' => $member->id
            ), 0, 10);

            // 集荷先が未登録の場合は、集荷先登録へ
            if (count($pickups) == 0) {
                redirect('pickup/add/continue');
                return;
            }

            //redirect('member/index');
            redirect('member/edit');

        }
    }

    /*=====================================================
    regist_after_estimateページ
    =====================================================*/
    public function regist_after_estimate($estimate_id = NULL, $estimate_email = NULL)
    {
        assign('title', '会員登録');
        $query = $this->db
            ->where('id', $estimate_id)
            ->get('estimates');
        
        /* パラメータチェック
        -------------------------------------------------*/
        try {
            if ($this->session->userdata('logged_in')) {
                throw new Exception('既にログイン中です');
            }
            if (!isset($estimate_id) || !isset($estimate_email) || strlen($estimate_id) == 0 || strlen($estimate_email) == 0) {
                throw new Exception('URLが正しくありません');
            }
            if ($query->num_rows() < 1) {
                throw new Exception('IDが正しくありません');
            }
            $estimate = $query->row_array();
            if ($estimate_email != $estimate['email']) {
                throw new Exception('メールアドレスが正しくありません');
            }
            $check_member = $this->membermodel->getByUsername($estimate_email, 1);
            if ($check_member) {
                throw new Exception('既に登録されたメールアドレスです');
            }
        } catch (Exception $e) {
            assign('error', $e->getMessage());
            render('member/regist_after_estimate');
            return;
        }

        /* フォーム生成
        -------------------------------------------------*/
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $form = new MemberFullForm(null, $estimate);
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new MemberFullForm($_POST);
            if ($form->isValid()) {
                // 会員情報の登録
                $set_member_data = array(
                    'email' => $estimate_email,
                    'password' => $this->encrypt->encode($this->input->post('password')),
                    'name' => $this->input->post('name'),
                    'kana' => $this->input->post('kana'),
                    'zipcode' => $this->input->post('zipcode'),
                    'address' => $this->input->post('address'),
                    'tel' => $this->input->post('tel'),
                    'is_verify' => 1,
                    'verified_at' => date('Y-m-d H:i:s'),
                    'is_active' => 1,
                    'hash' => $this->membermodel->hash($estimate_email, date('Y-m-d H:i:s')),
                );
                $ret_member = $this->db
                    ->set($set_member_data)
                    ->insert('members');
                $member_id = $this->db->insert_id();
                // 配送先の登録
                $set_pickup_data = array(
                    'name' => $this->input->post('name'),
                    'zipcode' => $this->input->post('zipcode'),
                    'address' => $this->input->post('address'),
                    'tel' => $this->input->post('tel'),
                    'member_id' => $member_id,
                );
                $ret_pickup = $this->db
                    ->set($set_pickup_data)
                    ->insert('pickups');
                // 登録後はログイン状態で注文フォームへリダイレクト
                if ($ret_member && $ret_pickup) {
                    $member = $this->membermodel->getByUsername($estimate_email);
                    // 登録完了メール送信
                    $email = new \SendGrid\Mail\Mail();
                    $email->setFrom(get_item('email_sender'), get_item('site_name'));
                    $email->setSubject(sprintf('【%s】会員登録ありがとうございます', get_item('site_name')));
                    $email->addTo($member->email);
                    $email->addContent("text/plain", $this->load->view('email/member_thanks', array(
                        'member' => $member,
                        'email_info' => get_item('info_mail'),
                    ), TRUE));
                    $tracking_settings = new \SendGrid\Mail\TrackingSettings();
                    $tracking_settings->setOpenTracking(
                        new \SendGrid\Mail\OpenTracking(false, null)
                    );
                    $email->setTrackingSettings($tracking_settings);
                    $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
                    $sendgrid->send($email);
                    // ログイン状態にする
                    $this->session->set_userdata('logged_in', TRUE);
                    $this->session->set_userdata('login_member', $member);
                    $this->membermodel->updateLoginedAt();
                    // 見積もり品目取得
                    $estimate_items = $this->db
                        ->where('estimate_id', $estimate['id'])
                        ->get('estimate_items')
                        ->result_array();
                    // 見積もり品目をセッションにセット
                    $step1 = array();
                    for ($i = 0; $i < get_item('max_order_item'); $i++) {
                        if (strlen($estimate_items[$i]['item']) > 0) {
                            if (in_array($estimate_items[$i]['item'], get_item('order_items'), true)) {
                                $step1['item_'.$i] = $estimate_items[$i]['item'];
                            } else {
                                $step1['item_'.$i] = 'その他';
                                $step1['item_etc_'.$i] = $estimate_items[$i]['item'];
                            }
                            $step1['item_num_'.$i] = $estimate_items[$i]['quantity'];
                        }
                    }
                    $this->session->set_userdata('order_step_1', $step1);
                    // 備考をセッションにセット
                    $step5 = array(
                        'message' => $estimate['remarks']
                    );
                    $this->session->set_userdata('order_step_5', $step5);
                    redirect('order/input/'.$estimate_id.'/'.$estimate_email);
                    return;
                } else {
                    die('会員登録に失敗しました。');
                }
            }
        }
        
        /* 描画
        -------------------------------------------------*/
        assign('email', $estimate_email);
        assign('form', $form);
        render('member/regist_after_estimate');
        return;
    }

    public function update_email()
    {
        $this->filter_auth();

        assign('is_static', true);
        assign('title', 'メールアドレスの変更');

        $member = $this->session->userdata('login_member');
        assign('current_email', $member->email);

        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $form = new MemberUpdateEmailForm();
            assign('form', $form);
            render('member/update_email');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new MemberUpdateEmailForm($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render('member/update_email');
                return;
            }

            $data = $form->cleaned_data;
            $member = $this->session->userdata('login_member');

            $member = $this->membermodel->get($member->id); // 最新情報取得
            if (!$this->membermodel->checkPassword($data['password'])) {
                assign('form', $form);
                notifier('failed', 'パスワードが一致しません。');
                redirect('member/update_email');
                return;
            }

            if (!$this->membermodel->checkDuplicateEmail($data['new_email'])) {
                assign('form', $form);
                notifier('failed', 'このメールアドレスは使用できません。');
                redirect('member/update_email');
                return;
            }

            $verify_code = $this->membermodel->createVerifyCode();

            $ret = $this->membermodel->saveTempData(array(
                'member_id' => $member->id,
                'new_email' => $data['new_email'],
                'verify_code' => $verify_code,
                'expired_at' => date('Y-m-d H:i:s', strtotime(get_item('email_verify_expire')))
            ));

            if (!$ret) {
                notifier('failed', 'メールアドレスの更新に失敗しました。');
                redirect('member/update_email');
                return;
            }

/*
                // メール送信
                $qdmail = new Qdmail('UTF-8', 'Base64');
                $qdmail->debug(get_item('email_debug'));
                $qdmail->to($data['new_email']);
                $qdmail->from(get_item('email_sender'));
                $qdmail->subject(sprintf('【%s】メールアドレス変更の確認', get_item('site_name')));
                $qdmail->text($this->load->view('email/member_update_email', array(
                    'member' => $member,
                    'new_email' => $data['new_email'],
                    'verify_code' => $verify_code,
                    'email_info' => get_item('info_mail'),
                ), TRUE));
                $qdmail->send();
*/
                $email = new \SendGrid\Mail\Mail();
                $email->setFrom(get_item('email_sender'), get_item('site_name'));
                $email->setSubject(sprintf('【%s】メールアドレス変更の確認', get_item('site_name')));
                $email->addTo($data['new_email']);
                $email->addContent("text/plain", $this->load->view('email/member_update_email', array(
                    'member' => $member,
                    'new_email' => $data['new_email'],
                    'verify_code' => $verify_code,
                    'email_info' => get_item('info_mail'),
                ), TRUE));
                $tracking_settings = new \SendGrid\Mail\TrackingSettings();
                $tracking_settings->setOpenTracking(
                    new \SendGrid\Mail\OpenTracking(false, null)
                );
                $email->setTrackingSettings($tracking_settings);
                $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
                $sendgrid->send($email);


            redirect('member/verify_email');

        }
    }

    public function verify_email()
    {
        $this->filter_auth();

        assign('is_static', true);
        assign('title', '確認コードの入力');

        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $form = new MemberVerifyEmailForm();
            assign('form', $form);
            render('member/verify_email');
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new MemberVerifyEmailForm($_POST);
            if (!$form->isValid()) {
                assign('form', $form);
                render('member/verify_email');
                return;
            }

            $data = $form->cleaned_data;
            $member = $this->session->userdata('login_member');
            $tmpdata = $this->membermodel->getTempData($member->id);

            if ($tmpdata === null) {
                notifier('failed', 'メールアドレスの変更は要求されていないか、有効期限が切れています。再度メールアドレスの変更を行なってください。');
                redirect('member/update_email');
                return;
            }

            if ($tmpdata->verify_code === $data['verify_code']) {
                $this->membermodel->get($member->id);
                $this->membermodel->updateEmail($tmpdata->new_email);
                $this->membermodel->clearTempData($member->id);

                $member = $this->membermodel->get($member->id);
                $this->session->set_userdata('login_member', $member);
/*
                // メール送信
                $qdmail = new Qdmail('UTF-8', 'Base64');
                $qdmail->debug(get_item('email_debug'));
                $qdmail->to(get_item('email_member_to'));
                $qdmail->bcc(get_item('email_bcc'));
                $qdmail->from($member->email);
                $qdmail->subject(sprintf('【%s】会員メールアドレス変更', get_item('site_name')));
                $qdmail->text($this->load->view('email/member_verify_email', array(
                    'member' => $member,
                    'email_info' => get_item('info_mail'),
                ), TRUE));
                $qdmail->send();
*/
                $email = new \SendGrid\Mail\Mail();
                $email->setFrom($member->email);
                $email->setSubject(sprintf('【%s】会員メールアドレス変更', get_item('site_name')));
                $email->addTo(get_item('email_member_to'));
                $email->addBcc(get_item('email_bcc'));
                $email->addContent("text/plain", $this->load->view('email/member_verify_email', array(
                    'member' => $member,
                    'email_info' => get_item('info_mail'),
                ), TRUE));
                $tracking_settings = new \SendGrid\Mail\TrackingSettings();
                $tracking_settings->setOpenTracking(
                    new \SendGrid\Mail\OpenTracking(false, null)
                );
                $email->setTrackingSettings($tracking_settings);
                $sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
                $sendgrid->send($email);
            } else {
                notifier('failed', 'メールアドレスの更新に失敗しました。確認コードが一致しません。');
                redirect('member/verify_email');
                return;
            }

            redirect('member/updated_email');

        }
    }

    public function updated_email()
    {
        $this->filter_auth();

        assign('title', 'メールアドレスの変更');
        render('member/updated_email');
    }

}

