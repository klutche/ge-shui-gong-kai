<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require_once APPPATH . 'libraries/qdmail.php';
require_once APPPATH . 'vendor/autoload.php';
require_once APPPATH . 'forms/MemberLoginForm.php';

class Order extends CI_Controller {

    protected $admin_email_subject;
    protected $thanks_email_subject;

    function __construct()
    {
        parent::__construct();

        assign('scripts', array('assets/js/order.js'));

        $this->load->model('membermodel');
        $this->load->model('pickupmodel');

        $login_form = new MemberLoginForm();
        assign('login_form', $login_form);
        assign('title', 'ご注文フォーム');
    }

    function index()
    {
        redirect('order/input');
    }

    /*=====================================================
    inputページ
    =====================================================*/
    function input($estimate_id = null, $estimate_email = null)
    {
        $logged_in = $this->session->userdata('logged_in');
        assign('logged_in', $logged_in);
        $member = null;
        $pickups = array();

        /* 見積もりデータの引き継ぎ
        -------------------------------------------------*/
        if (isset($estimate_id) && isset($estimate_email) && strlen($estimate_id) > 0 && strlen($estimate_email) > 0) {
            $estimate_query = $this->db
                ->where('id', $estimate_id)
                ->get('estimates');
            try {
                if ($estimate_query->num_rows() < 1) {
                    throw new Exception('IDが正しくありません');
                }
                $estimate = $estimate_query->row_array();
                if ($estimate_email != $estimate['email']) {
                    throw new Exception('メールアドレスが正しくありません');
                }
            } catch (Exception $e) {
                die($e->getMessage());
                return;
            }
            // 見積もりIDをセッションにセット
            $this->session->set_userdata('estimate_id', $estimate_id);
            // 見積もり品目取得
            $estimate_items = $this->db
                ->where('estimate_id', $estimate_id)
                ->get('estimate_items')
                ->result_array();
            // 見積もり品目をセッションにセット
            $step1 = array();
            for ($i = 0; $i < get_item('max_order_item'); $i++) {
                if (strlen($estimate_items[$i]['item']) > 0) {
                    if (in_array($estimate_items[$i]['item'], get_item('order_items'), true)) {
                        $step1['item_'.$i] = $estimate_items[$i]['item'];
                    } else {
                        $step1['item_'.$i] = 'その他';
                        $step1['item_etc_'.$i] = $estimate_items[$i]['item'];
                    }
                    $step1['item_num_'.$i] = $estimate_items[$i]['quantity'];
                }
            }
            $step1['is_expensive'] = $estimate['is_expensive'];
            $this->session->set_userdata('order_step_1', $step1);
            // 見積もり情報をセッションにセット
            $step2 = array(
                'name' => $estimate['name'],
                'kana' => $estimate['kana'],
                'email' => $estimate['email'],
                'tel' => $estimate['tel']
            );
            $this->session->set_userdata('order_step_2', $step2);
            // 備考をセッションにセット
            $step5 = array(
                'message' => $estimate['remarks']
            );
            $this->session->set_userdata('order_step_5', $step5);
        // 見積もりID・メールアドレスが無いURLの場合はセッションデータを破棄
        } else {
            $this->session->unset_userdata('estimate_id');
            $this->session->unset_userdata('order_step_1');
            $this->session->unset_userdata('order_step_2');
            $this->session->unset_userdata('order_step_3');
            $this->session->unset_userdata('order_step_5');
        }
        
        /* ログイン中の場合
        -------------------------------------------------*/
        if ($logged_in) {
            $_member = $this->session->userdata('login_member');

            // 最新データを取得
            $member = $this->membermodel->get($_member->id);
            if (strlen($member->name) == 0) {
                redirect('member/edit');
                return;
            }

            // step2
            $step2 = array(
                'name' => $member->name,
                'kana' => $member->kana,
                'zipcode' => $member->zipcode,
                'address' => $member->address,
                'tel' => $member->tel,
                'email' => $member->email
            );

            $this->session->set_userdata('order_step_2', $step2);

            $pickups = $this->pickupmodel->search(array(
                'member_id' => $member->id
            ), 0, 100, 'id desc');

            if (count($pickups) == 0) {
                redirect('pickup/add');
                return;
            }

            $step3 = null;
            if ($pickups) {
                $step3 = $pickups[0];
                if (isset($step3_box)) {
                    $step3->box = $step3_box['box'];
                }
            }

            $this->session->set_userdata('order_step_3', $step3);
            $this->session->set_userdata('pickups', $pickups);
            $this->session->set_userdata('selected_pickup', ($step3 !== null ? $step3->id : null));

        }
        assign('member', $member);
        render('order/input');

    }

    function partial($step)
    {
        $this->layout = '__NONE__';

        $step = (int) $step;

        $editable = $this->input->get('editable');

        $form_class = 'OrderStep' . $step . 'Form';

        $form_class_path = APPPATH . 'forms/' . $form_class . '.php';
        if (is_file($form_class_path)) {
            require_once $form_class_path;
        } else {
            show_error('Cannot load file');
        }


        $data = array();
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            if ($editable) {
                $inputed = null;

                $session_inputed = $this->session->userdata('order_step_' . $step);
                if ($session_inputed) {
                    $inputed = $session_inputed;
                }

                $form = new $form_class(null, $inputed);
                $data = array(
                    'form' => $form
                );
                $status = 'input';
                $template = 'order/partial_step_' . $step;
            } else {
                $status = 'bound';
                $template = 'order/partial_step_' . $step . '_bound';
            }
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new $form_class($_POST);
            if (!$form->isValid()) {
                $data = array(
                    'form' => $form
                );
                $status = 'input';
                $template = 'order/partial_step_' . $step;
            } else {
                $this->session->set_userdata('order_step_' . $step, $form->cleaned_data);
                $status = 'bound';
                $template = 'order/partial_step_' . $step . '_bound';
            }
        }

        $data['input'] = $this->session->userdata('order_step_' . $step);
        $ret = array(
            'status' => $status,
            'url' => site_url('order/partial_step_' . $step),
            'html' => $this->load->view($template, $data, true)
        );
        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($ret));
    }

    /*=====================================================
    メール送信・データベース登録
    =====================================================*/
    function send()
    {
        /* データベースに登録
        -------------------------------------------------*/
        
        // 注文情報の取得
        for ($i = 1; $i < 6; $i++) {
            $order_data['step' . $i] = $this->session->userdata('order_step_' . $i);
        }
        
        // 会員IDの取得
        $member_id = NULL;
        if ($this->session->userdata('logged_in')) {
            $member_id = $this->session->userdata('login_member')->id;
        }
        
        // 見積もりIDの取得
        $estimate_id = $this->session->userdata('estimate_id');
        
        // 注文情報の登録
        $set_data = array(
            'order_at' => date('Y-m-d H:i:s'),
            'estimate_id' => $estimate_id,
            'member_id' => $member_id,
            'is_expensive' => $order_data['step1']['is_expensive'],
            'name' => $order_data['step2']['name'],
            'kana' => $order_data['step2']['kana'],
            'email' => $order_data['step2']['email'],
            'tel' => $order_data['step2']['tel'],
            'zipcode' => $order_data['step2']['zipcode'],
            'address' => $order_data['step2']['address'],
            'pickup_name' => $order_data['step3']['name'],
            'pickup_tel' => $order_data['step3']['tel'],
            'pickup_zipcode' => $order_data['step3']['zipcode'],
            'pickup_address' => $order_data['step3']['address'],
            'pickup_method' => $order_data['step3']['method'],
            'pickup_date' => $order_data['step3']['date'],
            'pickup_time' => $order_data['step3']['time'],
            'pickup_boxes' => $order_data['step3']['box'],
            'payment' => $order_data['step4']['payment'],
            'remarks' => $order_data['step5']['message'],
        );
        $ret = $this->db->set($set_data)->insert('orders');
        if (!$ret) {
            throw new MemberRegisterException('注文情報の登録に失敗しました。');
        }
        $this->order_id = $this->db->insert_id();
        
        // 注文品目の登録
        $items_data = array();
        $max = get_item('max_order_item');
        for ($i = 0; $i < $max; $i++) {
            if (strlen($order_data['step1']['item_'.$i]) > 0) {
                $items_data = array(
                    'order_id' => $this->order_id,
                    'item' => $order_data['step1']['item_'.$i] == 'その他' ? $order_data['step1']['item_etc_'.$i] : $order_data['step1']['item_'.$i],
                    'quantity' => $order_data['step1']['item_num_'.$i],
                );
                $ret = $this->db->set($items_data)->insert('order_items');
                if (!$ret) {
                    throw new MemberRegisterException('注文品目の登録に失敗しました。');
                }
            }
        }
        
        // 注文IDを見積もりデータに登録
        if ($estimate_id !== NULL) {
            $this->db
                ->where('id', $estimate_id)
                ->set('order_id', $this->order_id)
                ->update('estimates');
        }
        
        // アンケート情報の登録
        if (isset($order_data['step5']['question_1'])) {
            $set_enquete_data = array(
                'order_id' => $this->order_id,
                'member_id' => $member_id,
                'question_1' => serialize($order_data['step5']['question_1']),
                'remarks_1' => $order_data['step5']['remarks_1'],
                'remarks_2' => $order_data['step5']['remarks_2'],
                'remarks_3' => $order_data['step5']['remarks_3'],
                'remarks_4' => $order_data['step5']['remarks_4'],
            );
            $this->db->set($set_enquete_data)->insert('enquete');
        }

        /* メール送信
        -------------------------------------------------*/
        $this->admin_email_subject = '注文';
        $this->thanks_email_subject = sprintf('【%s】ご注文の確認', get_item('site_name'));

        for ($i = 1; $i < 6; $i++) {
            $this->email_data['step' . $i] = $this->session->userdata('order_step_' . $i);
        }

        $this->email_data['is_member'] = $this->session->userdata('logged_in');

        $member_subject = '';
        if ($this->email_data['is_member']) {
            $member_subject = ' (会員)';
        }

        if (empty($this->email_data['step2']['email'])) {
            redirect('order/send_error');
            return;
        }

		$email = new \SendGrid\Mail\Mail();
		$email->setFrom($this->email_data['step2']['email'], $this->email_data['step2']['name']);
		$email->setSubject(sprintf('【%s】%s様', $this->admin_email_subject, $this->email_data['step2']['name'] . $member_subject));
		$email->addTo(get_item('email_order_to'));
		//$email->addBcc(get_item('email_bcc'));
		$email->addContent("text/plain", $this->load->view('email/order_admin', $this->email_data, TRUE));
		$tracking_settings = new \SendGrid\Mail\TrackingSettings();
		$tracking_settings->setOpenTracking(
		    new \SendGrid\Mail\OpenTracking(false, null)
		);
		$email->setTrackingSettings($tracking_settings);
		$sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
		$sendgrid->send($email);
        
        $email = new \SendGrid\Mail\Mail();
		$email->setFrom(get_item('email_sender'));
		$email->setSubject($this->thanks_email_subject);
		$email->addTo($this->email_data['step2']['email']);
		$email->addContent("text/plain", $this->load->view('email/order_thanks', $this->email_data, TRUE));
		$tracking_settings = new \SendGrid\Mail\TrackingSettings();
		$tracking_settings->setOpenTracking(
		    new \SendGrid\Mail\OpenTracking(false, null)
		);
		$email->setTrackingSettings($tracking_settings);
		$sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
		$sendgrid->send($email);

        // 新しい集配先を登録
        if ($this->session->userdata('logged_in') && $this->session->userdata('selected_pickup') == 'add') {
            $data = $this->session->userdata('order_step_3');

            // 集荷先から不要なデータを削除
            $unset_keys = array('method', 'date', 'time', 'box');
            foreach ($unset_keys as $key) {
                unset($data[$key]);
            }

            $member = $this->session->userdata('login_member');
            $data['member_id'] = $member->id;

            $this->load->model('pickupmodel');
            $this->pickupmodel->save($data);
        }

        if ($this->session->userdata('logged_in')) {
            $member = $this->session->userdata('login_member');
            $this->membermodel->setLastOrderAt($member->id);
        }

        if (strpos($this->email_data['step2']['email'], '@gmail.com') !== false) {
            $this->session->set_flashdata('use_gmail', true);
        }

        redirect('order/finish');

    }

    function send_error()
    {
        assign('message', "メール送信に失敗しました。\n" .
            "ブラウザを再起動後、再度ご入力ください。もしくは、お電話 ( 096-234-8677 / 平日9：00～18：00 )にてお問い合わせください。\n".
            "お手数をおかけしますが宜しくお願い申し上げます。"); 
        render('order/error');
    }

    function finish()
    {
        $logged_in = $this->session->userdata('logged_in');

        render('order/finish');

        /*
        if ($logged_in) {
            render('order/finish');
        } else {
            render('order/finish_and_register');
        }
         */
    }

    function get_step2_data()
    {
        $this->layout = '__NONE__';

        $step2 = $this->session->userdata('order_step_2');

        $ret = array(
            'data' => $step2
        );

        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($ret));

    }

    function get_step3_data()
    {
        $this->layout = '__NONE__';
        $pickups = $this->session->userdata('pickups');
        $selected_pickup = $this->input->get('id');

        $step3 = null;
        foreach ($pickups as $pickup) {
            if ($pickup->id == $selected_pickup) {
                $step3 = $pickup;
                break;
            }
        }

        $this->session->set_userdata('order_step_3', $step3);
        $this->session->set_userdata('selected_pickup', $selected_pickup);

        $ret = array(
            'data' => $step3
        );
        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($ret));

    }

}

