<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'libraries/qdmail.php';
require_once APPPATH . 'forms/MemberLoginForm.php';

class Ordertest extends CI_Controller {

    protected $admin_email_subject;
    protected $thanks_email_subject;

    function __construct()
    {
        parent::__construct();

        assign('scripts', array('assets/js/order.js'));

        $this->load->model('membermodel');
        $this->load->model('pickupmodel');

        $login_form = new MemberLoginForm();
        assign('login_form', $login_form);
        assign('title', 'ご注文フォーム');
    }

    function send()
    {
        exit;
        /*
        $this->admin_email_subject = '注文';
        $this->thanks_email_subject = sprintf('【%s】ご注文の確認', get_item('site_name'));

        $mail = new Qdmail('UTF-8', 'base64');
        $mail->debug(get_item('email_debug'));
        $mail->from('abfactory@docomo.ne.jp', 'あぶ テスト');
        $mail->to(get_item('email_order_to'));
        $mail->bcc(get_item('email_bcc'));
        $mail->subject('【革水】注文テスト #1');
        $mail->text('迷惑メールの判定テストです。');
        $mail->send();

        $mail = new Qdmail('UTF-8', 'base64');
        $mail->mtaOption('-f ' . get_item('email_sender'));
        $mail->debug(get_item('email_debug'));
        $mail->from('abfactory@docomo.ne.jp', 'あぶ テスト');
        $mail->to(get_item('email_order_to'));
        $mail->bcc(get_item('email_bcc'));
        $mail->subject('【革水】注文テスト #2');
        $mail->text('迷惑メールの判定テストです。');
        $mail->send();
         */

        $member = $this->membermodel->get(39);
        $pickups = $this->pickupmodel->search(array(
            'member_id' => $member->id
        ), 0, 1);

        // メール送信
        $qdmail = new Qdmail('UTF-8', 'Base64');
        $qdmail->debug(1);
        $qdmail->to(get_item('email_member_to'));
        $qdmail->bcc(get_item('email_bcc'));
        $qdmail->from($member->email, $member->name);
        $qdmail->subject(sprintf('【%s】会員登録', get_item('site_name')));
        $qdmail->text($this->load->view('email/member_admin2', array(
            'member' => $member,
            'pickup' => $pickups[0],
            'email_info' => get_item('info_mail'),
        ), TRUE));
        $qdmail->send();
        exit;

    }

}

