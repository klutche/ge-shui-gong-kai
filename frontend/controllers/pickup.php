<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'forms/PickupForm.php';
//require_once APPPATH . 'libraries/qdmail.php';
require_once APPPATH . 'vendor/autoload.php';

class Pickup extends CI_Controller {

  public function __construct()
  {
      parent::__construct();
      $this->load->model('membermodel');
      $this->load->model('pickupmodel');
      assign('is_static', true);
  }

  public function index()
  {
      filter_auth();

      assign('title', '集配先 (お届け先)');

      $login_member = $this->session->userdata('login_member');
      $pickups = $this->pickupmodel->search(array(
          'member_id' => $login_member->id
      ), 0, 100, 'id desc');

      assign('pickups', $pickups);

      $template = 'pickup/index';
      if (preg_match('/iPhone|Android/i', $this->input->server('HTTP_USER_AGENT'))) {
          $template = 'pickup/index_sp';
      }
      render($template);
  }

  public function add($continue=null)
  {
      filter_auth();

      assign('title', '集配先(お届け先)の登録');
      assign('btn_label', '保存する');
      assign('action', 'pickup/add');

      if ($this->input->server('REQUEST_METHOD') == 'GET') {
          $init = null;
          if (!is_null($continue)) {
              $login_member = $this->session->userdata('login_member');
              $init = $this->membermodel->get($login_member->id);
          }
          $form = new PickupForm(null, $init);
          assign('form', $form);
          render('pickup/input');
      } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
          $form = new PickupForm($_POST);
          if (!$form->isValid()) {
              assign('form', $form);
              render('pickup/input');
              return;
          }

          $data = $form->cleaned_data;

          $login_member = $this->session->userdata('login_member');
          $data['member_id'] = $login_member->id;

          $this->pickupmodel->save($data);

          // 初回登録ならメール送信
          if ($this->session->userdata('first_registered')) {
              $member = $this->membermodel->get($login_member->id);
              $pickups = $this->pickupmodel->search(array(
                  'member_id' => $member->id
              ), 0, 1);

              if (count($pickups) > 0) {
              /*
                  // メール送信
                  $qdmail = new Qdmail('UTF-8', 'Base64');
                  $qdmail->debug(get_item('email_debug'));
                  $qdmail->to(get_item('email_member_to'));
                  $qdmail->bcc(get_item('email_bcc'));
                  $qdmail->from($member->email, $member->name);
                  $qdmail->subject(sprintf('【%s】会員登録', get_item('site_name')));
                  $qdmail->text($this->load->view('email/member_admin2', array(
                      'member' => $member,
                      'pickup' => $pickups[0],
                      'email_info' => get_item('info_mail'),
                  ), TRUE));
                  $qdmail->send();
*/
					$email = new \SendGrid\Mail\Mail();
					$email->setFrom($member->email, $member->name);
					$email->setSubject(sprintf('【%s】会員登録', get_item('site_name')));
					$email->addTo(get_item('email_member_to'));
					$email->addBcc(get_item('email_bcc'));
					$email->addContent("text/plain", $this->load->view('email/member_admin2', array(
                      'member' => $member,
                      'pickup' => $pickups[0],
                      'email_info' => get_item('info_mail'),
                  ), TRUE));
					$tracking_settings = new \SendGrid\Mail\TrackingSettings();
					$tracking_settings->setOpenTracking(
					    new \SendGrid\Mail\OpenTracking(false, null)
					);
					$email->setTrackingSettings($tracking_settings);
					$sendgrid = new \SendGrid(get_item('sendgrid_api_key'));
					$sendgrid->send($email);

              }
          }

          $from = $this->session->userdata('from_order');
          if ($from === true) {
              $this->session->set_userdata('from_order', null);
              redirect('order/input');
              return;
          }

          redirect('pickup/index');
      }
  }

  public function edit($id)
  {
      filter_auth();

      assign('title', '集配先の編集');
      assign('btn_label', '変更する');
      assign('action', 'pickup/edit/' . $id);
      assign('has_delete_link', true);
      assign('id', $id);

      $member = $this->session->userdata('login_member');

      if ($this->input->server('REQUEST_METHOD') == 'GET') {
          $this->pickupmodel->setMemberId($member->id);
          $pickup = $this->pickupmodel->get($id);

          if (!$pickup) {
              redirect('pickup/add');
              return;
          }

          $form = new PickupForm(null, $pickup);
          assign('form', $form);
          render('pickup/input');
      } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
          $form = new PickupForm($_POST);
          if (!$form->isValid()) {
              assign('form', $form);
              render('pickup/input');
              return;
          }

          $data = $form->cleaned_data;

          $data['member_id'] = $member->id;

          $this->pickupmodel->save($data, $id);

          redirect('pickup/index');
      }
  }

  public function delete($id)
  {
      filter_auth();

      $pickup = $this->pickupmodel->get($id);
      $member = $this->session->userdata('login_member');

      if ($pickup->member_id != $member->id) {
          redirect('pickup/index');
          return;
      }

      $this->pickupmodel->delete($id);

      redirect('pickup/index');

  }
}


