<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Privacy extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        render('privacy/index');
    }

    function iframe()
    {
        $this->layout = 'iframe';
        render('privacy/iframe');
    }

}
