<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staticpage extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function show($page)
    {
        $titles = array(
            'specified_commercial_transactions_act' => '特定商取引法に基づく表記',
            'term_service' => '利用規約',
            'standards_compensation' => '事故賠償基準',
            'disclaimer' => '了承事項',
            'privacy' => 'プライバシーポリシー',
        );

        if (!isset($titles[$page])) {
            show_404();
        }

        assign('title', $titles[$page]);
        assign('is_static', true);
        assign('page', $page);
        render('static/page');
    }


}

