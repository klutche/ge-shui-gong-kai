<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'libraries/qdmail.php';
require_once APPPATH . 'forms/MemberLoginForm.php';

class Washstyle extends CI_Controller {

    protected $admin_email_subject;
    protected $thanks_email_subject;

    function __construct()
    {
        parent::__construct();

        assign('scripts', array('assets/js/washstyle.js'));

        $this->load->model('membermodel');
        $this->load->model('pickupmodel');

        $login_form = new MemberLoginForm();
        assign('login_form', $login_form);
        assign('title', 'ウォッシュスタイル ご注文フォーム');


    }

    function index()
    {
        redirect('washstyle/input');
    }

    function input()
    {
        $this->layout = 'washstyle';

        $logged_in = $this->session->userdata('logged_in');

        assign('logged_in', $logged_in);

        $member = null;
        $pickups = array();

        if ($logged_in) {
            $_member = $this->session->userdata('login_member');

            // 最新データを取得
            $member = $this->membermodel->get($_member->id);
            if (strlen($member->name) == 0) {
                echo '<script>window.parent.location.href = "https://www.kawasui.com/ap/member/edit";</script>';
                return;
            }

            // step1
            $step1 = array(
                'name' => $member->name,
                'kana' => $member->kana,
                'zipcode' => $member->zipcode,
                'address' => $member->address,
                'tel' => $member->tel,
                'email' => $member->email
            );

            $this->session->set_userdata('washstyle_step_1', $step1);

            $pickups = $this->pickupmodel->search(array(
                'member_id' => $member->id
            ), 0, 100, 'id desc');

            if (count($pickups) == 0) {
                echo '<script>window.parent.location.href = "https://www.kawasui.com/ap/pickup/add";</script>';
                return;
            }

            $step2 = null;
            if ($pickups) {
                $step2 = $pickups[0];
            }

            $this->session->set_userdata('washstyle_step_2', $step2);
            $this->session->set_userdata('pickups', $pickups);
            $this->session->set_userdata('selected_pickup', $step2->id);
        }
        assign('member', $member);
        render('washstyle/input');

    }

    function partial($step)
    {
        $this->layout = '__NONE__';

        $step = (int) $step;

        $editable = $this->input->get('editable');

        $form_class = 'WashstyleStep' . $step . 'Form';

        $form_class_path = APPPATH . 'forms/' . $form_class . '.php';
        if (is_file($form_class_path)) {
            require_once $form_class_path;
        } else {
            show_error('Cannot load file');
        }


        $data = array();
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            if ($editable) {
                $inputed = null;

                $session_inputed = $this->session->userdata('washstyle_step_' . $step);
                if ($session_inputed) {
                    $inputed = $session_inputed;
                }

                $form = new $form_class(null, $inputed);
                $data = array(
                    'form' => $form
                );
                $status = 'input';
                $template = 'washstyle/partial_step_' . $step;
            } else {
                $status = 'bound';
                $template = 'washstyle/partial_step_' . $step . '_bound';
            }
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form = new $form_class($_POST);
            if (!$form->isValid()) {
                $data = array(
                    'form' => $form
                );
                $status = 'input';
                $template = 'washstyle/partial_step_' . $step;
            } else {
                $this->session->set_userdata('washstyle_step_' . $step, $form->cleaned_data);
                $status = 'bound';
                $template = 'washstyle/partial_step_' . $step . '_bound';
            }
        }

        $data['input'] = $this->session->userdata('washstyle_step_' . $step);
        $ret = array(
            'status' => $status,
            'url' => site_url('washstyle/partial_step_' . $step),
            'html' => $this->load->view($template, $data, true)
        );
        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($ret));
    }

    function send()
    {
        $this->admin_email_subject = 'ウォッシュスタイル注文';
        $this->thanks_email_subject = sprintf('【%s】ウォッシュスタイルご注文の確認', get_item('site_name'));

        for ($i = 1; $i < 6; $i++) {
            $this->email_data['step' . $i] = $this->session->userdata('washstyle_step_' . $i);
        }

        $this->email_data['is_member'] = $this->session->userdata('logged_in');

        $member_subject = '';
        if ($this->email_data['is_member']) {
            $member_subject = ' (会員)';
        }

        $mail = new Qdmail('UTF-8', 'base64');
        $mail->debug(get_item('email_debug'));
        $mail->from($this->email_data['step1']['email'], $this->email_data['step1']['name']);
        $mail->to(get_item('email_order_to'));
        $mail->bcc(get_item('email_bcc'));
        $mail->subject(sprintf('【%s】%s様', $this->admin_email_subject, $this->email_data['step1']['name'] . $member_subject));
        $mail->text($this->load->view('email/washstyle_admin', $this->email_data, TRUE));
        $mail->send();


        $mail = new Qdmail('UTF-8', 'base64');
        $mail->debug(get_item('email_debug'));
        $mail->from(get_item('email_sender'));
        $mail->to($this->email_data['step1']['email']);
        $mail->subject($this->thanks_email_subject);
        $mail->text($this->load->view('email/washstyle_thanks', $this->email_data, TRUE));
        $mail->send();


        // 新しい集配先を登録
        if ($this->session->userdata('logged_in') && $this->session->userdata('selected_pickup') == 'add') {
            $data = $this->session->userdata('washstyle_step_2');

            // 集荷先から不要なデータを削除
            $unset_keys = array('method', 'date', 'time', 'box');
            foreach ($unset_keys as $key) {
                unset($data[$key]);
            }

            $member = $this->session->userdata('login_member');
            $data['member_id'] = $member->id;

            $this->load->model('pickupmodel');
            $this->pickupmodel->save($data);
        }

        if (strpos($this->email_data['step1']['email'], '@gmail.com') !== false) {
            $this->session->set_flashdata('use_gmail', true);
        }

        redirect('washstyle/finish');

    }

    function finish()
    {
        $this->layout = 'washstyle';
        $logged_in = $this->session->userdata('logged_in');

        render('washstyle/finish');
        /*
        if ($logged_in) {
            render('washstyle/finish');
        } else {
            render('washstyle/finish_and_register');
        }
         */
    }

    function get_step1_data()
    {
        $this->layout = '__NONE__';

        $step1 = $this->session->userdata('washstyle_step_1');

        $ret = array(
            'data' => $step1
        );

        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($ret));

    }

    function get_step2_data()
    {
        $this->layout = '__NONE__';
        $pickups = $this->session->userdata('pickups');
        $selected_pickup = $this->input->get('id');

        $step2 = null;
        foreach ($pickups as $pickup) {
            if ($pickup->id == $selected_pickup) {
                $step2 = $pickup;
                break;
            }
        }

        $this->session->set_userdata('washstyle_step_2', $step2);
        $this->session->set_userdata('selected_pickup', $selected_pickup);

        $ret = array(
            'data' => $step2
        );
        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($ret));

    }

}

