<?php

require_once APPPATH . 'libraries/Form.php';

class CareStep1Form extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();

        for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
            $this->addField('item_' . $i,
                new Form_DropdownField(array(
                    'required' => ($i == 0 ? true : false),
                    'label' => 'クリーニング品(' . ($i+1) . ')',
                    'rules' => 'normalize|max_length[50]',
                    'choices' => get_item('care_items')
                ))
            );

            $opt = get_item('order_item_nums');

            if ($i == 0) {
                unset($opt[0]);
            }

            $this->addField('item_num_' . $i,
                new Form_DropdownField(array(
                    'required' => ($i == 0 ? true : false),
                    'label' => 'クリーニング品数(' . ($i+1) . ')',
                    'rules' => 'normalize|max_length[2]|numeric|check_item_num[item_' . $i . ']',
                    'choices' => $opt,
                    'extra' => array(
                        'class' => 'input-mini'
                    )

                ))
            );
        }

    }
}

