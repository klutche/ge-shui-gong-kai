<?php

require_once APPPATH . 'libraries/Form.php';

class CareStep3Form extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $this->addField('name',
            new Form_TextField(array(
                'required' => true,
                'label' => 'お名前',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'maxlength' => 50,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('zipcode',
            new Form_TextField(array(
                'required' => true,
                'label' => '郵便番号',
                'rules' => 'normalize|max_length[10]',
                'prefix' => '〒',
                'extra' => array(
                    'maxlength' => 10,
                    'class' => 'input-small address-search'
                )
            ))
        );

        $this->addField('address',
            new Form_TextField(array(
                'required' => true,
                'label' => '住所',
                'rules' => 'normalize|max_length[200]',
                'extra' => array(
                    'maxlength' => 200,
                    'class' => 'input-xxlarge'
                )
            ))
        );

        $this->addField('tel',
            new Form_TextField(array(
                'required' => true,
                'label' => '電話番号',
                'rules' => 'normalize|valid_tel',
                'extra' => array(
                    'maxlength' => 13,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('method',
            new Form_RadioField(array(
                'required' => true,
                'label' => '集配方法',
                'initial' => 'ヤマト運輸による集配',
                'rules' => 'normalize|max_length[50]',
                'help_text' => '<span class="help-block">※「当社集配サービス」は<strong>熊本市内のみ</strong>のサービスです。</span>',
                'choices' => get_item('pickup_methods')
            ))
        );

        $this->addField('date',
            new Form_DropdownField(array(
                'required' => true,
                'label' => 'ご希望の集荷日',
                'rules' => 'normalize|max_length[50]',
                'help_text' => '<span class="help-block">※ご希望に添えない場合がございます。</span>',
                'choices' => get_pickup_dates()
            ))
        );

        $this->addField('time',
            new Form_DropdownField(array(
                'required' => true,
                'label' => 'ご希望の集荷時間',
                'rules' => 'normalize|max_length[50]',
                'help_text' => '<span class="help-block" style="color: #c33">※当社集配サービスの場合は<strong>18時</strong>までとなります。</span>',
                'choices' => get_item('pickup_times')
            ))
        );

        $this->addField('box',
            new Form_RadioField(array(
                'required' => true,
                'label' => '梱包箱を希望',
                'initial' => '希望しない',
                'rules' => 'normalize|max_length[50]',
                'help_text' => '<span class="help-block">※集荷の際にドライバーがお持ちします。</span>',
                'choices' => get_item('pickup_boxes')
            ))
        );

    }


}

