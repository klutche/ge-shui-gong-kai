<?php

require_once APPPATH . 'libraries/Form.php';

class CareStep4Form extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();

        $this->addField('payment',
            new Form_RadioField(array(
                'required' => true,
                'label' => 'お支払い方法',
                'rules' => 'normalize|max_length[50]',
                'initial' => '代金引換',
                'choices' => get_item('payments')
            ))
        );

    }
}


