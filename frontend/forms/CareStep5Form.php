<?php

require_once APPPATH . 'libraries/Form.php';

class CareStep5Form extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();

        $this->addField('message',
            new Form_TextareaField(array(
                'required' => false,
                'label' => 'ご要望・備考',
                'rules' => 'normalize|max_length[2000]',
                'extra' => array(
                    'class' => 'input-xxlarge',
                    'row' => 5
                )
            ))
        );

    }
}


