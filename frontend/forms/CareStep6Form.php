<?php

require_once APPPATH . 'libraries/Form.php';

class CareStep6Form extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();

        $this->addField('apply',
            new Form_CheckboxField(array(
                'required' => true,
                'label' => '了承事項・クリーニング事故賠償基準への同意',
                'rules' => 'normalize|max_length[50]',
                'choices' => array(
                    1 => '了承事項・クリーニング事故賠償基準に同意する'
                )
            ))
        );

    }
}


