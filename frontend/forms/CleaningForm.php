<?php

require_once APPPATH . 'libraries/Form.php';

class CleaningForm extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();

        $this->addField('name',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => 'お名前',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'maxlength' => 50,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('kana',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => 'フリガナ',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'maxlength' => 50,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('email',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => 'メールアドレス',
                'rules' => 'normalize|max_length[100]|valid_email',
                'help_text' => '※携帯のメールアドレスをお使いの方は、「 <span style="color:#c33">' . get_item('email_domain') . '</span> 」からのメールを受信許可(ドメイン指定)してください。<br>※「.」は「..」などのように連続で使用することや@マークの直前で使用することはできません。',
                'extra' => array(
                    'maxlength' => 100,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('tel',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => '電話番号',
                'rules' => 'normalize|valid_tel',
                'extra' => array(
                    'maxlength' => 13,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('item',
            new Form_TextareaField(array(
                'required' => TRUE,
                'label' => 'クリーニング品',
                'rules' => 'normalize|max_length[2000]',
                'help_text' => '※クリーニングしたい商品と数量をお書きください。<br>例) ハンドバッグ(ヴィトン) 1点<br>ブーツ 1点',
                'extra' => array(
                    'cols' => 50,
                    'rows' => 10,
                    'class' => 'input-xxlarge'
                )
            ))
        );


        $this->addField('message',
            new Form_TextareaField(array(
                'required' => TRUE,
                'label' => 'ご要望・汚れの程度',
                'rules' => 'normalize|max_length[2000]',
                'extra' => array(
                    'cols' => 50,
                    'rows' => 10,
                    'class' => 'input-xxlarge'
                )
            ))
        );

        $this->addField('apply',
            new Form_CheckboxField(array(
                'required' => TRUE,
                'label' => 'プライバシーポリシーの確認',
                'rules' => '',
                'initial' => '',
                'choices' => array(
                    '1' => 'プライバシーポリシーに同意する',
                )
            ))
        );

    }
}
