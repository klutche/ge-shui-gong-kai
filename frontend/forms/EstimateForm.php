<?php
require_once APPPATH.'libraries/Form.php';

class EstimateForm extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $this->addField('name', new Form_TextField(array(
            'required' => TRUE,
            'label' => 'お名前',
            'rules' => 'normalize|max_length[50]',
            'extra' => array(
                'maxlength' => 50,
                'class' => 'input-xlarge'
            )
        )));
        
        $this->addField('kana', new Form_TextField(array(
            'required' => TRUE,
            'label' => 'フリガナ',
            'rules' => 'normalize|max_length[50]',
            'extra' => array(
                'maxlength' => 50,
                'class' => 'input-xlarge'
            )
        )));
        
        $this->addField('email', new Form_TextField(array(
            'required' => TRUE,
            'label' => 'メールアドレス',
            'rules' => 'normalize|max_length[100]|valid_email',
            'help_text' => '<span class="help-block">携帯のメールアドレスをお使いの方は、「 <span style="color:#c33">'.get_item('email_domain').'</span> 」からのメールを受信許可(ドメイン指定)してください。</span>',
            'extra' => array(
                'maxlength' => 100,
                'class' => 'input-xlarge'
            )
        )));
        
        $this->addField('tel', new Form_TextField(array(
            'required' => TRUE,
            'label' => '電話番号',
            'rules' => 'normalize|valid_tel',
            'extra' => array(
                'maxlength' => 13,
                'class' => 'input-xlarge'
            )
        )));
        
        $this->addField('prefcode', new Form_DropdownField(array(
            'required' => TRUE,
            'label' => '集荷先',
            'choices' => get_item('prefcode'),
            'help_text' => '<span class="help-block">都道府県名をご入力ください。送料の目安となります。</span>',
        )));
        
        for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
            $this->addField('item_'.$i, new Form_DropdownField(array(
                'required' => ($i == 0 ? true : false),
                'label' => 'クリーニング・修理品('.($i+1).')',
                'rules' => 'normalize|max_length[50]',
                'choices' => get_item('order_items'),
                'extra' => array(
                    'class' => 'item'
                )
            )));
            $style = 'display: none';
            if ((isset($initial['item_'.$i]) && $initial['item_'.$i] == 'その他') || (isset($data['item_'.$i]) && $data['item_'.$i] == 'その他')) {
                $style = '';
            }
            $this->addField('item_etc_'.$i, new Form_TextField(array(
                'required' => false,
                'label' => 'クリーニング・修理品その他('.($i+1).')',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'class' => 'item-etc',
                    'style' => $style,
                    'maxlength' => 50,
                    'placeholder' => 'ブランド・アイテム名・素材'
                )
            )));
            $opt = get_item('order_item_nums');
            if ($i == 0) {
                unset($opt[0]);
            }
            $this->addField('item_num_'.$i, new Form_DropdownField(array(
                'required' => ($i == 0 ? true : false),
                'label' => 'クリーニング・修理品数('.($i+1).')',
                'rules' => 'normalize|max_length[2]|numeric|check_item_num[item_'.$i.']',
                'choices' => $opt,
                'extra' => array(
                    'class' => 'input-mini'
                )
            )));
        }
        
        $this->addField('is_expensive', new Form_CheckboxField(array(
            'required' => false,
            'label' => '高級品の有無',
            'rules' => 'normalize|max_length[20]',
            'help_text' => '<span class="help-block">高価な商品は色を鮮やかに出したり、色のくすみを防ぐ為にほとんどの商品が色止めをされていません。私どもの作業も、特別な作業になります。<br>
当社のミスで起こったトラブルは、「クリーニング事故賠償基準」により、対応しております。<br>
ご注文フォームの「高級品の有無」にチェックが入っていない場合、賠償基準における「物品の再取得価格」は、20万円以下の品物として評価させて頂きます。</span>',
            'choices' => array(
                1 => '評価額又は購入価格が20万円以上の品物がある場合はチェックを付けてください'
            )
        )));
        
        $this->addField('pickup_boxes', new Form_RadioField(array(
            'required' => true,
            'label' => '依頼品のサイズ',
            'initial' => 'バッグ以外',
            'rules' => 'normalize|max_length[50]',
            'choices' => get_item('pickup_size'),
            'help_text' => '<span class="help-block">※バッグ以外のご依頼の場合は、「バッグ以外」を選択ください。</span>',
        )));
        
        $this->addField('message', new Form_TextareaField(array(
            'required' => TRUE,
            'label' => 'ご要望・汚れの程度',
            'rules' => 'normalize|max_length[2000]',
            'help_text' => '<span class="help-block">例）バッグの取っ手が手垢で汚れており、内側の底に5cm×5cmくらいのカビが生えています。<br>
どちらも目立たなくして欲しいです。</span>',
            'extra' => array(
                'cols' => 50,
                'rows' => 10,
                'class' => 'input-xxlarge'
            )
        )));
        
        $this->addField('upload', new Form_UploadField(array(
            'required' => TRUE,
            'label' => '画像ファイル',
            'help_text' => '<span class="help-block">※可能であれば、汚れ、破損箇所が分かるような写真を送信ください。<br>
※ファイルサイズが5MByteまでのJPEG画像(*.jpg, *.jpeg)のみ送信可能です。(送信時に2400px以内に縮小されます)<br>
※5MByte以上の画像や複数枚の画像を送りたい方は、info@kawasui.com宛にメールに添付し、複数回に分けてお送りください。<br>
※画像が添付できない方は、<a href="/ap/contact/input" target="_blank">問い合わせフォーム</a>をご利用くださいませ。<br>
※動画の添付は出来ませんのでご了承ください。</span>',
            'extra' => array(
                'class' => 'input-xxlarge',
                'size' => 50
            )
        )));
        
        $this->addField('apply', new Form_CheckboxField(array(
            'required' => TRUE,
            'label' => 'プライバシーポリシーの同意',
            'rules' => '',
            'initial' => '',
            'choices' => array(
                '1' => 'プライバシーポリシーに同意する',
            )
        )));
    }
}
