<?php

require_once APPPATH . 'libraries/Form.php';

class MemberForgotPasswordForm extends Form
{
    public function config($data=NULL, $init=NULL, $opts=NULL)
    {

        $this->addField('username',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => 'メールアドレス',
                'rules' => 'normalize|max_length[50]|xss_clean',
                'extra' => array(
                    'class' => 'span3',
                    'maxlength' => 50))));

    }

}



