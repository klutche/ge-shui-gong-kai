<?php

require_once APPPATH . 'libraries/Form.php';

class MemberForm extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();

        $this->addField('name',
            new Form_TextField(array(
                'required' => true,
                'label' => 'お名前',
                'rules' => 'normalize|max_length[50]',
                'help_text' => '<span class="help-block">漢字のフルネームでご入力ください。</span>',
                'extra' => array(
                    'maxlength' => 50,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('kana',
            new Form_TextField(array(
                'required' => true,
                'label' => 'フリガナ',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'maxlength' => 50,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('zipcode',
            new Form_TextField(array(
                'required' => true,
                'label' => '郵便番号',
                'rules' => 'normalize|max_length[10]',
                'prefix' => '〒',
                'extra' => array(
                    'maxlength' => 10,
                    'class' => 'input-small address-search'
                )
            ))
        );

        $this->addField('address',
            new Form_TextField(array(
                'required' => true,
                'label' => '住所',
                'rules' => 'normalize|max_length[200]',
                'extra' => array(
                    'maxlength' => 200,
                    'class' => 'input-xxlarge'
                )
            ))
        );

        $this->addField('tel',
            new Form_TextField(array(
                'required' => true,
                'label' => '電話番号',
                'rules' => 'normalize|valid_tel',
                'extra' => array(
                    'maxlength' => 13,
                    'class' => 'input-xlarge'
                )
            ))
        );

        /*
        $this->addField('remarks',
            new Form_TextareaField(array(
                'required' => false,
                'label' => '備考',
                'rules' => 'normalize|max_length[2000]',
                'extra' => array(
                    'cols' => 50,
                    'rows' => 10,
                    'class' => 'input-xxlarge'
                )
            ))
        );
         */


    }
}

