<?php
require_once APPPATH . 'libraries/Form.php';

class MemberFullForm extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();
        $this->addField('password',
            new Form_PasswordField(
                array(
                    'required' => TRUE,
                    'label' => 'パスワード',
                    'rules' => 'normalize|max_length[32]|min_length[6]|valid_password',
                    'help_text' => '<span class="help-block">文字数: 6〜32文字/半角のアルファベット、数字(0〜9)、 記号(!"#$%&\'()=~|-^\@;:[],./`+*{}<>?_)</span>',
                    'extra' => array(
                        'class' => 'input-large',
                        'maxlength' => 50
                    )
                )
            )
        );
        $this->addField('password_check',
            new Form_PasswordField(
                array(
                    'required' => TRUE,
                    'label' => 'パスワード(確認用)',
                    'rules' => 'normalize|max_length[32]|min_length[6]|matches[password]',
                    'help_text' => '<span class="help-block">同じパスワードをもう一度入力してください。</span>',
                    'extra' => array(
                        'class' => 'input-large',
                        'maxlength' => 50
                    )
                )
            )
        );
        $this->addField('name',
            new Form_TextField(
                array(
                    'required' => true,
                    'label' => 'お名前',
                    'rules' => 'normalize|max_length[50]',
                    'help_text' => '<span class="help-block">漢字のフルネームでご入力ください。</span>',
                    'extra' => array(
                        'maxlength' => 50,
                        'class' => 'input-xlarge'
                    )
                )
            )
        );
        $this->addField('kana',
            new Form_TextField(
                array(
                    'required' => true,
                    'label' => 'フリガナ',
                    'rules' => 'normalize|max_length[50]',
                    'extra' => array(
                        'maxlength' => 50,
                        'class' => 'input-xlarge'
                    )
                )
            )
        );
        $this->addField('zipcode',
            new Form_TextField(
                array(
                    'required' => true,
                    'label' => '郵便番号',
                    'rules' => 'normalize|max_length[10]',
                    'prefix' => '〒',
                    'extra' => array(
                        'maxlength' => 10,
                        'class' => 'input-small address-search'
                    )
                )
            )
        );
        $this->addField('address',
            new Form_TextField(
                array(
                    'required' => true,
                    'label' => '住所',
                    'rules' => 'normalize|max_length[200]',
                    'extra' => array(
                        'maxlength' => 200,
                        'class' => 'input-xxlarge'
                    )
                )
            )
        );
        $this->addField('tel',
            new Form_TextField(
                array(
                    'required' => true,
                    'label' => '電話番号',
                    'rules' => 'normalize|valid_tel',
                    'extra' => array(
                        'maxlength' => 13,
                        'class' => 'input-xlarge'
                    )
                )
            )
        );
    }
}
