<?php

require_once APPPATH . 'libraries/Form.php';

class MemberLoginForm extends Form
{
    public function config($data=NULL, $init=NULL, $opts=NULL)
    {

        $ci =& get_instance();

        $this->addField('username',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => 'メールアドレス',
                'rules' => 'normalize|max_length[50]|xss_clean',
                'extra' => array(
                    'class' => 'input-large',
                    'maxlength' => 50))));

        $this->addField('password',
            new Form_PasswordField(array(
                'required' => TRUE,
                'label' => 'パスワード',
                'rules' => 'normalize|max_length[50]|xss_clean',
                'extra' => array(
                    'class' => 'input-large',
                    'maxlength' => 50))));

        $this->addField('redirect_url',
            new Form_HiddenField(array(
                'label' => 'リダイレクト先',
                'initial' => ($ci->input->server('HTTPS') ? 'https' : 'http') . '://' . $ci->input->server('HTTP_HOST') . $ci->input->server('REQUEST_URI')
            )));


    }

}


