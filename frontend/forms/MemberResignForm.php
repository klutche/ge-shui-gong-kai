<?php

require_once APPPATH . 'libraries/Form.php';

class MemberResignForm extends Form
{
    public function config($data=NULL, $init=NULL, $opts=NULL)
    {
        $this->addField('password',
            new Form_PasswordField(array(
                'required' => TRUE,
                'label' => 'パスワード',
                'rules' => 'normalize|max_length[50]|xss_clean',
                'extra' => array(
                    'class' => 'span3',
                    'maxlength' => 50))));

        $this->addField('reason',
            new Form_RadioField(array(
                'required' => TRUE,
                'label' => '主な退会理由を教えていただけますか',
                'initial' => '利用機会がなくなったから',
                'choices' => array(
                    '利用機会がなくなったから' => '利用機会がなくなったから',
                    'サービスに満足いかなかったから' => 'サービスに満足いかなかったから',
                    '他社を利用するようになったから' => '他社を利用するようになったから',
                    'その他' => 'その他',
                ))));

    }
}

