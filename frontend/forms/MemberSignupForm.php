<?php

require_once APPPATH . 'libraries/Form.php';

class MemberSignupForm extends Form
{
    public function config($data=NULL, $init=NULL, $opts=NULL)
    {
        $this->addField('email',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => 'メールアドレス',
                'rules' => 'normalize|max_length[50]|valid_email',
                'extra' => array(
                    'class' => 'input-large',
                    'maxlength' => 50))));

        $this->addField('password',
            new Form_PasswordField(array(
                'required' => TRUE,
                'label' => 'パスワード',
                'rules' => 'normalize|max_length[32]|min_length[6]|valid_password',
                'help_text' => '<span class="help-block">文字数: 6〜32文字/半角のアルファベット、数字(0〜9)、 記号(!"#$%&\'()=~|-^\@;:[],./`+*{}<>?_)</span>',
                'extra' => array(
                    'class' => 'input-large',
                    'maxlength' => 50))));

        $this->addField('password_check',
            new Form_PasswordField(array(
                'required' => TRUE,
                'label' => 'パスワード(確認用)',
                'rules' => 'normalize|max_length[32]|min_length[6]|matches[password]',
                'help_text' => '<span class="help-block">同じパスワードをもう一度入力してください。</span>',
                'extra' => array(
                    'class' => 'input-large',
                    'maxlength' => 50))));
    }
}
