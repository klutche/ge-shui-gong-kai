<?php

require_once APPPATH . 'libraries/Form.php';

class MemberUpdateEmailForm extends Form
{
    public function config($data=NULL, $init=NULL, $opts=NULL)
    {

        $this->addField('new_email',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => '新しいメールアドレス',
                'rules' => 'normalize|max_length[100]|valid_email',
                'extra' => array(
                    'class' => 'span3',
                    'maxlength' => 50))));

        $this->addField('password',
            new Form_PasswordField(array(
                'required' => TRUE,
                'label' => 'パスワード',
                'rules' => 'normalize|max_length[50]|xss_clean',
                'extra' => array(
                    'class' => 'span3',
                    'maxlength' => 50))));

    }
}

