<?php

require_once APPPATH . 'libraries/Form.php';

class MemberVerifyEmailForm extends Form
{
    public function config($data=NULL, $init=NULL, $opts=NULL)
    {

        $this->addField('verify_code',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => '確認コード',
                'rules' => 'normalize|max_length[10]',
                'extra' => array(
                    'class' => 'span3',
                    'maxlength' => 10))));


    }
}

