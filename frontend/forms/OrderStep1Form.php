<?php
require_once APPPATH . 'libraries/Form.php';

class OrderStep1Form extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();
        
        for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
            $this->addField('item_' . $i,
                new Form_DropdownField(array(
                    'required' => ($i == 0 ? true : false),
                    'label' => 'クリーニング・修理品(' . ($i+1) . ')',
                    'rules' => 'normalize|max_length[50]',
                    'choices' => get_item('order_items'),
                    'extra' => array(
                        'class' => 'item')
                ))
            );
            
            $style = 'display: none';
            if (isset($initial['item_' . $i]) && $initial['item_' . $i] == 'その他') {
                $style = '';
            }
            
            $this->addField('item_etc_' . $i,
                new Form_TextField(array(
                    'required' => false,
                    'label' => 'クリーニング・修理品その他(' . ($i+1) . ')',
                    'rules' => 'normalize|max_length[50]',
                    'extra' => array(
                        'class' => 'item-etc',
                        'style' => $style,
                        'maxlength' => 50,
                        'placeholder' => 'ブランド・アイテム名・素材'
                    )
                ))
            );
            
            $opt = get_item('order_item_nums');
            
            if ($i == 0) {
                unset($opt[0]);
            }
            
            $this->addField('item_num_' . $i,
                new Form_DropdownField(array(
                    'required' => ($i == 0 ? true : false),
                    'label' => 'クリーニング・修理品数(' . ($i+1) . ')',
                    'rules' => 'normalize|max_length[2]|numeric|check_item_num[item_' . $i . ']',
                    'choices' => $opt,
                    'extra' => array(
                        'class' => 'input-mini'
                    )

                ))
            );
        }
        
        $this->addField('is_expensive', new Form_CheckboxField(array(
            'required' => false,
            'label' => '高級品の有無',
            'rules' => 'normalize|max_length[20]',
            'help_text' => '<span class="help-block">高価な商品は色を鮮やかに出したり、色のくすみを防ぐ為にほとんどの商品が色止めをされていません。私どもの作業も、特別な作業になります。<br>
    当社のミスで起こったトラブルは、「クリーニング事故賠償基準」により、対応しております。<br>
    ご注文フォームの「高級品の有無」にチェックが入っていない場合、賠償基準における「物品の再取得価格」は、20万円以下の品物として評価させて頂きます。</span>',
            'choices' => array(
                1 => '評価額又は購入価格が20万円以上の品物がある場合は、チェックを付けてください'
            )
        )));
        
    }
}
