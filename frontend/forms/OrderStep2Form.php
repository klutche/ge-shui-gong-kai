<?php

require_once APPPATH . 'libraries/Form.php';

class OrderStep2Form extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();

        $this->addField('name',
            new Form_TextField(array(
                'required' => true,
                'label' => 'お名前',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'maxlength' => 50,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('kana',
            new Form_TextField(array(
                'required' => true,
                'label' => 'フリガナ',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'maxlength' => 50,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('zipcode',
            new Form_TextField(array(
                'required' => true,
                'label' => '郵便番号',
                'rules' => 'normalize|max_length[10]',
                'prefix' => '〒',
                'extra' => array(
                    'maxlength' => 10,
                    'class' => 'input-small address-search'
                )
            ))
        );

        $this->addField('address',
            new Form_TextField(array(
                'required' => true,
                'label' => '住所',
                'rules' => 'normalize|max_length[200]',
                'extra' => array(
                    'maxlength' => 200,
                    'class' => 'input-xxlarge'
                )
            ))
        );

        $this->addField('tel',
            new Form_TextField(array(
                'required' => true,
                'label' => '電話番号',
                'rules' => 'normalize|valid_tel',
                'extra' => array(
                    'maxlength' => 13,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('email',
            new Form_TextField(array(
                'required' => true,
                'label' => 'メールアドレス',
                'rules' => 'normalize|valid_email',
                'help_text' => '<span class="help-block">※ご注文後、自動返信メールが送信されますので、パソコンからのメールを受信できるよう設定をお願い致します。<br>詳細は、<a href="/news/3215.html" target="_blank">自動返信メールが届かない方へ</a>をご覧ください。<br>
                ※「.」は「..」などのように連続で使用することや@マークの直前で使用することはできません。</span>',
                'extra' => array(
                    'maxlength' => 100,
                    'class' => 'input-xlarge'
                )
            ))
        );

    }
}

