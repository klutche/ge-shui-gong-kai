<?php

require_once APPPATH . 'libraries/Form.php';

class OrderStep5Form extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();

        $this->addField('message', new Form_TextareaField(array(
            'required' => false,
            'label' => 'ご要望・備考',
            'rules' => 'normalize|max_length[2000]',
            'help_text' => '<div class="help-block">例1)ジャケットの胸元にパスタのソースがついてしまいました。すぐに拭きとりましたが5cmくらいのシミになってしまいました。目立たなくしてほしいです。<br>
            例2)エルメスのケリーですが、特に濡らしたりはしていませんが全体的に黒ずんでいます。またフチの部分が削れて色が落ちています。キズの補修も含めてキレイにしてほしいです。<br>
            例3)1年ぶりにだした革のブーツがカビだらけになっていました。かるく拭きましたが跡が残っています。キズは直さなくていいです。
            </div>',
            'extra' => array(
                'class' => 'input-xxlarge',
                'rows' => 5,
                'placeholder' => '汚れの状態や原因を分る範囲でご記入ください。'
            )
        )));
        
        $this->addField('question_1', new Form_MultiCheckboxField(array(
            'required' => FALSE,
            'label' => '当サイトをどこで知りましたか？',
            'choices' => array(
                '過去依頼した経験から' => '過去依頼した経験から',
                'Web検索' => 'Web検索',
                'Instagram' => 'Instagram',
                'facebook' => 'facebook',
                'twitter' => 'twitter',
                'youtube' => 'youtube',
                'googleマップ' => 'googleマップ(googleの評価)',
                'その他SNS' => 'その他SNS',
                '知人からの口コミ' => '知人からの(アナログ的な)口コミ',
                '口コミサイトを見て' => '口コミサイトを見て',
                'その他' => 'その他'
            ),
            'extra' => array(
                'class' => 'question',
            )
        )));
        
        $this->addField('remarks_1', new Form_TextareaField(array(
            'required' => false,
            'label' => 'その他SNS',
            'rules' => 'normalize|max_length[2000]',
            'extra' => array(
                'class' => 'input-xxlarge',
                'rows' => 1,
                'style' => 'max-width: calc(100% - 20px); margin-left: 20px;',
                'placeholder' => 'その他SNSをご記入ください'
            )
        )));
        
        $this->addField('remarks_2', new Form_TextareaField(array(
            'required' => false,
            'label' => '知人からの口コミ',
            'rules' => 'normalize|max_length[2000]',
            'extra' => array(
                'class' => 'input-xxlarge',
                'rows' => 1,
                'style' => 'max-width: calc(100% - 20px); margin-left: 20px;',
                'placeholder' => '知人の方のお名前をご記入ください'
            )
        )));
        
        $this->addField('remarks_3', new Form_TextareaField(array(
            'required' => false,
            'label' => '口コミサイトを見て',
            'rules' => 'normalize|max_length[2000]',
            'extra' => array(
                'class' => 'input-xxlarge',
                'rows' => 1,
                'style' => 'max-width: calc(100% - 20px); margin-left: 20px;',
                'placeholder' => 'サイト名またはURLをご記入ください'
            )
        )));
        
        $this->addField('remarks_4', new Form_TextareaField(array(
            'required' => false,
            'label' => 'その他',
            'rules' => 'normalize|max_length[2000]',
            'extra' => array(
                'class' => 'input-xxlarge',
                'rows' => 1,
                'style' => 'max-width: calc(100% - 20px); margin-left: 20px;',
                'placeholder' => 'サイト名またはURLをご記入ください'
            )
        )));
        
    }
}
