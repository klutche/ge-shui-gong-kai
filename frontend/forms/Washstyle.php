<?php

require_once APPPATH . 'libraries/Form.php';

class ContactForm extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();

        $this->addField('name',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => 'お名前',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'maxlength' => 50,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('kana',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => 'フリガナ',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'maxlength' => 50,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('zipcode',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => '郵便番号',
                'rules' => 'normalize|max_length[10]',
                'extra' => array(
                    'maxlength' => 13,
                    'class' => 'input-large'
                )
            ))
        );

        $this->addField('address',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => '住所',
                'rules' => 'normalize|max_length[200]',
                'extra' => array(
                    'maxlength' => 200,
                    'class' => 'input-xxlarge'
                )
            ))
        );

        $this->addField('email',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => 'メールアドレス',
                'rules' => 'normalize|max_length[100]|valid_email',
                'help_text' => '携帯のメールアドレスをお使いの方は、「 <span style="color:#c33">xxxxxxxxx.xxx</span> 」からのメールを受信許可(ドメイン指定)してください。',
                'extra' => array(
                    'maxlength' => 100,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('tel',
            new Form_TextField(array(
                'required' => TRUE,
                'label' => '電話番号',
                'rules' => 'normalize|valid_tel',
                'extra' => array(
                    'maxlength' => 13,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('message',
            new Form_TextareaField(array(
                'required' => TRUE,
                'label' => 'お問い合わせ内容',
                'rules' => 'normalize|max_length[2000]',
                'extra' => array(
                    'cols' => 50,
                    'rows' => 10,
                    'class' => 'input-xxlarge'
                )
            ))
        );

        $this->addField('apply',
            new Form_CheckboxField(array(
                'required' => TRUE,
                'label' => 'プライバシーポリシーの確認',
                'rules' => '',
                'initial' => '',
                'choices' => array(
                    '1' => 'プライバシーポリシーに同意する',
                )
            ))
        );

    }
}
