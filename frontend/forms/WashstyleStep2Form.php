<?php

require_once APPPATH . 'libraries/Form.php';

class WashstyleStep2Form extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();

        $this->addField('name',
            new Form_TextField(array(
                'required' => true,
                'label' => 'お名前',
                'rules' => 'normalize|max_length[50]',
                'extra' => array(
                    'maxlength' => 50,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('zipcode',
            new Form_TextField(array(
                'required' => true,
                'label' => '郵便番号',
                'rules' => 'normalize|max_length[10]',
                'prefix' => '〒',
                'extra' => array(
                    'maxlength' => 10,
                    'class' => 'input-small address-search'
                )
            ))
        );

        $this->addField('address',
            new Form_TextField(array(
                'required' => true,
                'label' => '住所',
                'rules' => 'normalize|max_length[200]',
                'extra' => array(
                    'maxlength' => 200,
                    'class' => 'input-xxlarge'
                )
            ))
        );

        $this->addField('tel',
            new Form_TextField(array(
                'required' => true,
                'label' => '電話番号',
                'rules' => 'normalize|valid_tel',
                'extra' => array(
                    'maxlength' => 13,
                    'class' => 'input-xlarge'
                )
            ))
        );

        $this->addField('method',
            new Form_RadioField(array(
                'required' => true,
                'label' => '集配方法',
                'initial' => 'ヤマト運輸による集配',
                'rules' => 'normalize|max_length[50]',
                'help_text' => '<span class="help-block">※「当社集配サービス」は<strong>熊本市内のみ</strong>のサービスです。</span>',
                'choices' => get_item('pickup_methods')
            ))
        );

        $this->addField('pickup_date',
            new Form_DropdownField(array(
                'required' => true,
                'label' => 'ご希望の集荷日',
                'rules' => 'normalize|max_length[50]',
                'help_text' => '<span class="help-block">※ご希望に添えない場合がございます。</span>',
                'choices' => get_pickup_dates()
            ))
        );

        $this->addField('pickup_time',
            new Form_DropdownField(array(
                'required' => true,
                'label' => 'ご希望の集荷時間',
                'rules' => 'normalize|max_length[50]',
                'help_text' => '<span class="help-block" style="color: #c33">※当社集配サービスの場合は<strong>18時</strong>までとなります。</span>',
                'choices' => get_item('pickup_times')
            ))
        );

        $this->addField('delivery_date',
            new Form_TextField(array(
                'required' => true,
                'label' => 'ご希望の配送日',
                'rules' => 'normalize|max_length[20]',
                'help_text' => '<span class="help-block">※配送日を含みますので、集荷日から3日後以降の指定をして下さい。</span>',
                'extra' => array(
                    'class' => 'input-large'
                )
            ))
        );

        $this->addField('delivery_time',
            new Form_DropdownField(array(
                'required' => true,
                'label' => 'ご希望の配送時間',
                'rules' => 'normalize|max_length[50]',
                'help_text' => '<span class="help-block">※当社集配サービスの場合は18時までとなります。</span>',
                'choices' => get_item('pickup_times')
            ))
        );

        $this->addField('bag_large',
            new Form_TextField(array(
                'required' => false,
                'label' => '注文バッグ数 (大: 6〜7kg)',
                'rules' => 'normalize|max_length[10]',
                'help_text' => '<span class="help-block">※送料はランドリーバッグ1個に対しての料金となります。</span>',
                'suffix' => '個',
                'extra' => array(
                    'class' => 'input-small'
                )
            ))
        );

        $this->addField('bag_small',
            new Form_TextField(array(
                'required' => false,
                'label' => '注文バッグ数 (小: 2〜3kg)',
                'rules' => 'normalize|max_length[10]',
                'help_text' => '<span class="help-block">※送料はランドリーバッグ1個に対しての料金となります。</span>',
                'suffix' => '個',
                'extra' => array(
                    'class' => 'input-small'
                )
            ))
        );

    }

}

