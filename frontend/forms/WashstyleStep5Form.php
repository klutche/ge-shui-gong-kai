<?php

require_once APPPATH . 'libraries/Form.php';

class WashstyleStep5Form extends Form
{
    public function config($data=NULL, $initial=NULL, $opts=NULL)
    {
        $ci =& get_instance();

        $this->addField('apply',
            new Form_CheckboxField(array(
                'required' => true,
                'label' => '利用規約への同意',
                'rules' => 'normalize|max_length[50]',
                'choices' => array(
                    1 => '利用規約に同意する'
                )
            ))
        );
    }
}


