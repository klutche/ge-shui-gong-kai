<?php

if (!function_exists('get_item')) {
    function get_item($key, $default=null)
    {
        $ci =& get_instance();

        $cache_key = 'config_' . $key;
        $ttl = 3600;

        if ( ! $value = $ci->cache->get($cache_key)) {
            $config = $ci->configmodel->getByKey($key);
            if (!$config) {
                return null;
            }

            $value = $config->value;

            $ci->cache->save($cache_key, $value, $ttl);
        }

        return $value;
    }
}

if (!function_exists('set_item')) {
    function set_item($key, $value)
    {
        $ci =& get_instance();

        $ci->configmodel->saveAtKey($key, $value);

        $ci->cache->clean();
    }
}
