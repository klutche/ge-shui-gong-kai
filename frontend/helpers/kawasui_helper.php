<?php
function get_pickup_dates()
{
    return get_pickup_dates_v3();

    /*
    $ci =& get_instance();

    if (($gcal = $ci->cache->get('google_calendar_test')) === FALSE) {
        $cal_url = 'http://www.google.com/calendar/feeds/webmaster@kawasui.com/public/full?alt=json&orderby=starttime&sortorder=ascend&max-results=100&futureevents=true';
        $gcal = file_get_contents($cal_url);
        $ci->cache->save($gcal, 'google_calendar_test');
    }

    $holiday = array();
    $json = json_decode($gcal);
    foreach ($json->feed->entry as $entry) {
        foreach ($entry->{'gd$when'} as $d) {
            $holiday[] = date('Y/m/d', strtotime($d->startTime));
        }
    }

    sort($holiday);

    $business_dates = array();

    $today = time();

    $s = strtotime(date('Y/m/d', $today));
    $e = strtotime(date('Y/m/d', $today)) + 30 * 86400;

    $week = explode(' ', '日 月 火 水 木 金 土');
    for ($t = $s; $t <= $e; $t += 86400) {
        $formatted_date = date('Y/m/d', $t);
        if (!in_array($formatted_date, $holiday)) {
            $business_dates[] = $formatted_date;
        }
    }
    // 翌営業日後から
    array_shift($business_dates);


    $available_dates = array();
    $s = strtotime($business_dates[0]) + 1 * 86400;
    $e = strtotime($business_dates[0]) + 30 * 86400;

    $exclude = get_item('pickup_date_exclude');
    for ($t = $s; $t <= $e; $t += 86400) {
        $formatted_date = date('Y/m/d', $t);
        if (in_array($formatted_date, $exclude)) {
            continue;
        }
        $d = sprintf('%s (%s)', $formatted_date, $week[date('w', $t)]);
        $available_dates[$d] = $d;
    }

    return $available_dates;
     */
}

function get_pickup_dates_v3()
{
    $ci =& get_instance();

    $calendar_id = 'webmaster%40kawasui.com';
    $api_key = 'AIzaSyCzZxnEpiqq1ELG1M20PFBYaQtvXDzRbxs';
    $timeMin = date('Y-m-d') . 'T00:00:00Z';
    $orderBy = 'startTime';

    if (($gcal = $ci->cache->get('google_calendar_test')) === FALSE) {
        $cal_url = 'https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events?key=' . $api_key;
        $cal_url .= '&timeMin=' . $timeMin . '&orderBy=' . $orderBy . '&singleEvents=true';
        $gcal = file_get_contents($cal_url);
        $ci->cache->save('google_calendar_test', $gcal, 86400);
    }

    $holiday = array();
    $json = json_decode($gcal);
    foreach ($json->items as $entry) {
        if (isset($entry->start)) {
            $holiday[] = date('Y/m/d', strtotime($entry->start->date));
        }
    }

    sort($holiday);

    $business_dates = array();

    $today = time();

    $s = strtotime(date('Y/m/d', $today));
    $e = strtotime(date('Y/m/d', $today)) + 30 * 86400;

    $week = explode(' ', '日 月 火 水 木 金 土');
    for ($t = $s; $t <= $e; $t += 86400) {
        $formatted_date = date('Y/m/d', $t);
        if (!in_array($formatted_date, $holiday)) {
            $business_dates[] = $formatted_date;
        }
    }
    // 翌営業日後から
    array_shift($business_dates);

    if ($today < strtotime('2017-08-07')) {
        array_shift($business_dates);
    }


    $available_dates = array();
    $s = strtotime($business_dates[0]) + 1 * 86400;
    $e = strtotime($business_dates[0]) + 30 * 86400;

    $exclude = get_item('pickup_date_exclude');
    for ($t = $s; $t <= $e; $t += 86400) {
        $formatted_date = date('Y/m/d', $t);
        if (in_array($formatted_date, $exclude)) {
            continue;
        }
        $d = sprintf('%s (%s)', $formatted_date, $week[date('w', $t)]);
        $available_dates[$d] = $d;
    }

    return $available_dates;
}
