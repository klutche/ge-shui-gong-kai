<?php

/**
 * Assign parameter to view.
 */
function assign($key, $val=NULL)
{
    $CI =& get_instance();

    if (!isset($CI->template_params)) {
        $CI->template_params = array();
    }

    if (is_array($key)) {
        $CI->template_params = array_merge($CI->template_params, $key);
    } else {
        $CI->template_params[$key] = $val;
    }

}

/**
 * Unassign parameter to view.
 */
function unassign($key)
{
    $CI =& get_instance();

    if (isset($CI->template_params[$key])) {
        unset($CI->template_params[$key]);
    }

}

/**
 * Get assigned parameters.
 */
function get_p($key=NULL)
{
    $CI =& get_instance();

    if (isset($CI->template_params)) {
        if (!is_null($key) && isset($CI->template_params[$key])) {
            return $CI->template_params[$key];
        }

        return $CI->template_params;

    }

}

/**
 * Render :: wrapped '$CI->load->view($template, $CI->template_params)'
 */
function render($template, $return=FALSE)
{
    $CI =& get_instance();

    return $CI->load->view($template, get_p(), $return);
}


/**
 * Return static url. e.g) http://www.example.com/static/***
 */
function static_url($url)
{
    $CI =& get_instance();
    $base_url = $CI->config->item('base_url');
    $index_page = $CI->config->item('index_page');

    if (strlen($index_page) > 0) {
        $base_url = strtr($base_url, array($index_page => ''));
    }
    $ret = rtrim($base_url, '/') . '/static/'. ltrim($url, '/');

    return $ret;
}

/**
 * site_url wrapper
 */
function u($url)
{
    return site_url($url);
}

function price_format($price)
{
    $price = preg_replace('/[^0-9]/', '', $price);

    $oku = floor($price / 100000000);
    $man = floor(($price % 100000000) / 10000);
    $hasu = ($price % 100000000) % 10000;

    $ret = '';
    if ($oku > 0) {
        $ret .= $oku . '億';
    }

    if ($man > 0) {
        $ret .= $man . '万';
    }

    if ($hasu > 0) {
        $ret .= $hasu;
    }
    $ret .= '円';

    return $ret;

}

function notifier()
{
    $args = func_get_args();

    $ci =& get_instance();

    if (count($args) == 0) {
        return $ci->session->flashdata('notifier');
    } elseif (count($args) == 2) {
        $notice = new stdClass;
        $notice->type = $args[0];
        $notice->msg = $args[1];
        $ci->session->set_flashdata('notifier', $notice);
    }
}

function h($str)
{
    if (is_array($str)) {
        return array_map('h', $str);
    } else {
        return htmlspecialchars($str, ENT_QUOTES, config_item('charset'));
    }
}

function rh($str)
{
    if (is_array($str)) {
        return array_map('rh', $str);
    } else {
        return html_entity_decode($str, ENT_QUOTES, config_item('charset'));
    }
}

function date_short($date)
{
    $ts = strtotime($date);
    $now = time();

    if (date('Ymd', $now) == date('Ymd', $ts)) { // today
        return date('H:i', $ts);
    }

    return date('y/m/d', $ts);
}

function str_short($str, $max_length)
{
    $len = mb_strlen($str);

    if ($len <= $max_length) {
        return $str;
    }

    $limit = floor($max_length / 2);
    return mb_substr($str, 0, $limit) . '...' . mb_substr($str, (-1 * $limit));
}

function format_filesize($size)
{
    $sizes = array('Bytes', 'KB', 'MB', 'GB');
    $ext = $sizes[0];

    for ($i = 1; (($i < count($sizes)) && ($size >= 1024)); $i++) {
        $size = $size / 1024;
        $ext = $sizes[$i];
    }

    return round($size, 2) . $ext;

}
