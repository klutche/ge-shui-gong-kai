<?php

class PrivilegeHook
{
    private $invalid_roles = array(
        'admin' => array(),
        'consultee' => array('user:*'),
        'editor' => array('user:*', 'email:*'),
    );

    public function check()
    {
        $ci =& get_instance();
        if ($ci->session->userdata('logged_in')) {
            $role = $ci->session->userdata('role');
            $roles = $this->invalid_roles[$role];

            $class = $ci->router->class;
            $method = $ci->router->method;
            $class_method = $class . ':' . $method;
            $class_any = $class . ':' . '*';

            $is_redirect = FALSE;

            if (in_array($class_method, $roles)) {
                $is_redirect = TRUE;
                $redirect_url = 'error/invalid_role';
            }

            if (in_array($class_any, $roles)) {
                $is_redirect = TRUE;
                $redirect_url = 'error/invalid_role';
            }

            if ($is_redirect) {
                redirect($redirect_url);
            }
        }
    }
}
