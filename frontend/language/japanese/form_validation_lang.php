<?php

$lang['required']           = "%s は必ず入力してください。";
$lang['isset']              = "%s は必ず入力してください。";
$lang['valid_email']        = "%s には正しいメールアドレスを入力してください。";
$lang['valid_emails']       = "%s には正しいメールアドレスを入力してください。";
$lang['valid_url']          = "%s には正しいURLを入力してください。";
$lang['valid_ip']           = "%s には正しいIPアドレスを入力してください。";
$lang['min_length']         = "%s は %s 文字以上にしてください。";
$lang['max_length']         = "%s は %s 文字以下にしてください。";
$lang['exact_length']       = "%s は %s 文字にしてください。";
$lang['alpha']              = "%s はアルファベットで入力してください。";
$lang['alpha_numeric']      = "%s は英数字で入力してください。";
$lang['alpha_dash']         = "%s は英数字、アンダースコア(_)、ハイフン(-)で入力してください。";
$lang['numeric']            = "%s は数字で入力してください。";
$lang['is_numeric']         = "%s は数字で入力してください。";
$lang['integer']            = "%s は整数で入力してください。";
$lang['matches']            = "%s は %s と同じものを入力してください。";
$lang['is_natural']         = "%s は 自然数で入力してください。";
$lang['is_natural_no_zero'] = "%s は 0以上の自然数で入力してください。";

$lang['gt']                 = "%s は %s より大きい数字を入力してください。";
$lang['lt']                 = "%s は %s より小さい数字を入力してください。";
$lang['max_count']          = "%s は %s 個以下お選びください。";
$lang['min_count']          = "%s は %s 個以上お選びください。";
$lang['valid_date']         = "%s には YYYY-MM-DD形式で入力してください。";
$lang['contain']            = "%s には %s を含めてください。";
$lang['hiragana']           = "%s は「ひらがな」で入力してください。";
$lang['katakana']           = "%s は「カタカナ」で入力してください。";
$lang['valid_tel']          = "%s には電話番号を入力してください。";
$lang['valid_zip']          = "%s には郵便番号を入力してください。";
$lang['valid_pref']         = "%s を選択してください。";
$lang['valid_password']     = "%s に使用できない文字が含まれています。";
$lang['check_item_num']     = "%s は「1」以上を選択してください。";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */
