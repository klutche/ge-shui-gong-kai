<?php
class MY_Form_validation extends CI_Form_validation
{
    public function valid_password($str)
    {
        return (bool) preg_match('/^[!-~]+$/i', $str);
    }

    public function check_item_num($str, $target)
    {
        $ci =& get_instance();
        $item = $ci->input->post($target);

        if (strlen($item) > 0) {
            return ((int) $str > 0 ? true : false);
        } else {
            return true;
        }
    }
}
