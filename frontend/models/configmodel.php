<?php

require_once dirname(__FILE__) . '/basemodel.php';

class Configmodel extends Basemodel
{

    public $insert_id;

    public function __construct()
    {
        parent::__construct();
        $this->table = 'configs';
    }

    public function saveAtKey($key, $value, $label=null)
    {
        $data = $this->getByKey($key);
        if (!$data) {
            $data = array(
                'key' => $key,
                'value' => $value,
                'label' => (!is_null($label) ? $label : $key)
            );

            $id = null;
        } else {
            $id = $data->id;
            $data->value = $value;
        }

        $this->save($data, $id);
    }

    public function getByKey($key)
    {
        $results = $this->search(array('key' => $key), 0, 1);

        if (isset($results[0])) {
            return $results[0];
        }

        return null;
    }

    protected function appendCondition($condition)
    {
        if (isset($condition['key'])) {
            $this->db->where('key', $condition['key']);
        }
    }

    protected function afterResult(&$row)
    {
        if (isset($row->value) && @unserialize($row->value) !== false) {
            $row->value = unserialize($row->value);
        }
    }

    protected function beforeSave(&$data)
    {
        if (is_object($data)) {
            $data = (array) $data;
        }

        if (isset($data['value'])
            && (is_array($data['value']) || is_object($data['value']))) {

                $data['value'] = serialize($data['value']);
        }
    }

    protected function afterInsert()
    {
        $this->insert_id = $this->db->insert_id();
    }

}


