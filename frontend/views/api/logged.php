<div style="margin-bottom: 10px"><img src="/wordpress/wp-content/themes/wholesell/images/mn_member_nav.png" alt="会員メニュー" /></div>
<p><?= $member->name ?>様</p>
<ul id="right_menu">
<li><a href="<?= site_url('member/edit') ?>">お客様情報の変更</a></li>
<li><a href="<?= site_url('pickup/index') ?>">集配先の管理</a></li>
<li><a href="<?= site_url('member/update_email') ?>">メールアドレスの確認・変更</a></li>
<li><a href="<?= site_url('member/update_password') ?>">パスワードの変更</a></li>
<li><a href="<?= site_url('member/resign') ?>">退会</a></li>
<li><a href="<?= site_url('member/logout') ?>">ログアウト</a></li>
</ul>
