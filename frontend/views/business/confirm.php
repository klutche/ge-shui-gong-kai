<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <?= form_open('business/send', array('class' => 'form-vertical')) ?>
    <div class="control-group">
      <label class="control-label"><?= $form->name->labelTag ?></label>
      <div class="controls">
        <?= $form->name->error ?>
        <?= $form->name ?></div>
    </div>
    <div class="control-group">
      <label class="control-label"><?= $form->kana->labelTag ?></label>
      <div class="controls">
        <?= $form->kana->error ?>
        <?= $form->kana ?></div>
    </div>
    <div class="control-group">
      <label class="control-label"><?= $form->email->labelTag ?></label>
      <div class="controls">
        <?= $form->email->error ?>
        <?= $form->email ?>
        <?= $form->email->help_text ?>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label"><?= $form->tel->labelTag ?></label>
      <div class="controls">
        <?= $form->tel->error; ?>
        <?= $form->tel ?></div>
    </div>
    <div class="control-group">
      <label class="control-label"><?= $form->message->labelTag ?></label>
      <div class="controls">
        <?= $form->message->error ?>
        <?= $form->message ?></div>
    </div>

    <div class="actions text-center">
      <?= form_hidden('apply', 1) ?>
      <input type="submit" class="btn" name="back" value="戻る" />
      <input type="submit" class="btn btn-primary" name="send" value="送信する" />
    </div>

    <?= form_close() ?>
  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>


