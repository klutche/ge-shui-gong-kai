このメールは、<?= $this->input->server('HTTP_HOST') ?>から送信されました。

<?= $this->load->view('business/email_common', array('data' => $data), TRUE) ?>

<?/*
●ユーザー環境
UserAgent: <?= $this->input->server('HTTP_USER_AGENT') ?>

Remote Address: <?= $this->input->server('REMOTE_ADDR') ?>

Request Time: <?= date('Y/m/d H:i:s', $this->input->server('REQUEST_TIME')) ?>
 */?>
