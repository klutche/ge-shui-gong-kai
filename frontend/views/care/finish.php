<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>
    <h2>ご注文ありがとうございました。</h2>
    <p>ご入力頂いたメールアドレス宛に確認のメールを送信しました。</p>
    <?= $this->load->view('gmail', null, true) ?>
  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>

<?= $this->load->view('conversion', null, true) ?>
