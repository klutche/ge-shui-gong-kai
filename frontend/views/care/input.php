<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <?= $this->load->view('secure_seal', null, true) ?>

    <div id="partial-form-group">
      <section class="section-form active" id="section-form-1" data-id="1" data-url="<?= site_url('care/partial/1') ?>" data-bound="0">
      <h2><span class="header-number"><img src="<?= site_url('assets/img/label_step1.png') ?>" alt="1"></span>クリーニング品 <button type="button" class="btn btn-edit-form" style="display: none">編集</button></h2>
      <div class="section-content"></div>
      </section>

      <section class="section-form" id="section-form-2" data-id="2" data-url="<?= site_url('care/partial/2') ?>" data-bound="0">
      <h2><span class="header-number"><img src="<?= site_url('assets/img/label_step2.png') ?>" alt="2"></span>お客様情報 <button type="button" class="btn btn-edit-form" style="display: none">編集</button></h2>
      <div class="section-content"></div>
      </section>

      <section class="section-form" id="section-form-3" data-id="3" data-url="<?= site_url('care/partial/3') ?>" data-bound="0">
      <h2><span class="header-number"><img src="<?= site_url('assets/img/label_step3.png') ?>" alt="3"></span>集配 <button type="button" class="btn btn-edit-form" style="display: none">編集</button></h2>
      <div class="section-content"></div>
      </section>

      <section class="section-form" id="section-form-4" data-id="4" data-url="<?= site_url('care/partial/4') ?>" data-bound="0">
      <h2><span class="header-number"><img src="<?= site_url('assets/img/label_step4.png') ?>" alt="4"></span>お支払い <button type="button" class="btn btn-edit-form" style="display: none">編集</button></h2>
      <div class="section-content"></div>
      </section>

      <section class="section-form" id="section-form-5" data-id="5" data-url="<?= site_url('care/partial/5') ?>" data-bound="0">
      <h2><span class="header-number"><img src="<?= site_url('assets/img/label_step5.png') ?>" alt="5"></span>ご要望・備考 <button type="button" class="btn btn-edit-form" style="display: none">編集</button></h2>
      <div class="section-content"></div>
      </section>

      <section class="section-form" id="section-form-6" data-id="6" data-url="<?= site_url('care/partial/6') ?>" data-bound="0">
      <h2><span class="header-number"><img src="<?= site_url('assets/img/label_step6.png') ?>" alt="6"></span>ご注文の確定 <button type="button" class="btn btn-edit-form" style="display: none">編集</button></h2>
      <div class="section-content"></div>
      </section>
    </div><!-- /#partial-form-grop -->
  </div><!-- /#main -->

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>


</div><!-- /.row-fluid -->
