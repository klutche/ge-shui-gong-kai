<?= form_open('care/partial/1', array('class' => 'partial')) ?>
<div class="control-group">
<?for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i): ?>
<div class="controls">
<?= $form->{'item_' . $i}->error ?>
<?= $form->{'item_num_' . $i}->error ?>
<?= $form->{'item_' . $i} ?><span class="item-num-label">数量:</span>
<?= $form->{'item_num_' . $i} ?>
</div>
<?endfor?>
</div>

<div class="actions">
<button type="submit" class="btn btn-primary">続ける</button>
</div>
<?= form_close() ?>
