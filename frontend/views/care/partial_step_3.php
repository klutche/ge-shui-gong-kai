<?= form_open('care/partial/3', array('class' => 'partial')) ?>
<?if ($this->session->userdata('logged_in')): ?>
<p>集配先をお選びください。</p>
<div class="pickups">
<?foreach($this->session->userdata('pickups') as $pickup): ?>
<label class="radio">
<input type="radio" name="pickup" data-pickup-id="<?= $pickup->id ?>" <?if ($pickup->id == $this->session->userdata('selected_pickup')): ?>checked<?endif?>><?= $pickup->name ?> / <?= $pickup->address ?>
</label>
<?endforeach?>
<label class="radio">
<input type="radio" name="pickup" data-pickup-id="add" <?if ($this->session->userdata('selected_pickup') == 'add'): ?>checked<?endif?>>新しい集配先
</label>
</div>
<?else:?>
<button type="button" class="btn btn-small" id="btn-copy-step2-data">お客様情報をコピーする</button>
<?endif?>
<?= $form ?>
<div class="actions">
<button type="submit" class="btn btn-primary">続ける</button>
</div>
<?= form_close() ?>

