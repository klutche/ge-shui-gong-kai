<div class="row-fluid">
<div class="inline-content span6">
<article>
<h1>了承事項</h1>
<?= $this->load->view('static/disclaimer_common', null, true) ?>
</article>
</div>

<div class="inline-content span6">
<article>
<h1>クリーニング事故賠償基準</h1>
<?= $this->load->view('static/standards_compensation_common', null, true) ?>
</article>
</div>
</div>

<?= form_open('care/send') ?>
<?= $form ?>


<div class="actions">
<button id="btn-send" disabled type="submit" class="btn btn-primary">同意して注文する</button>
</div>
<?= form_close() ?>

<div class="alert">
見積もり後のキャンセルの場合、商品は着払いにてお送り致します。<br>
クリーニング工程に入ってからのキャンセルはお受けできませんのでご了承ください。
</div>
