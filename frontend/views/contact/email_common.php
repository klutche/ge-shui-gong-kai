●お客様情報
◇お名前
  <?= $data['name'] . "\r\n" ?>

◇フリガナ
  <?= $data['kana'] . "\r\n" ?>

◇メールアドレス
  <?= $data['email'] . "\r\n" ?>

◇電話番号
  <?= $data['tel'] . "\r\n" ?>

◇お問い合わせ内容
  <?= $data['message'] . "\r\n" ?>

