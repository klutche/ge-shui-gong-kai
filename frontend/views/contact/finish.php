<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>


    <p>お問い合わせいただき、誠にありがとうございます。</p>
    <p>担当者が確認次第、ご返答させていただきます。<br />
    <?= get_item('email_reply_day') ?>営業日を経過しても返信がない場合は、お手数ですが下記までご連絡ください。</p>

    <div class="alert alert-info">TEL: <?= get_item('tel') ?> / Email: <?= get_item('info_mail') ?></div>

    <?= $this->load->view('gmail', null, true) ?>
  </div>


  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>


<?= $this->load->view('conversion', null, true) ?>
