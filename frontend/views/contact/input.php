<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <p>お客様から頂いたいよくあるご質問とご回答を「<a href="/lib" target="_blank">皮革Q&Aサイトひかくらし</a>」にてご紹介しております。お問い合わせの前に是非ご覧ください。</p>
    <?= $this->load->view('secure_seal', null, true) ?>

    <?= form_open('contact/input', array('class' => 'form-vertical')) ?>
    <div class="control-group">
      <label class="control-label"><?= $form->name->labelTag ?></label>
      <div class="controls">
        <?= $form->name->error ?>
        <?= $form->name ?></div>
    </div>
    <div class="control-group">
      <label class="control-label"><?= $form->kana->labelTag ?></label>
      <div class="controls">
        <?= $form->kana->error ?>
        <?= $form->kana ?></div>
    </div>
    <div class="control-group">
      <label class="control-label"><?= $form->email->labelTag ?></label>
      <div class="controls">
        <?= $form->email->error ?>
        <?= $form->email ?>
        <?= $form->email->help_text ?>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label"><?= $form->tel->labelTag ?></label>
      <div class="controls">
        <?= $form->tel->error; ?>
        <?= $form->tel ?></div>
    </div>
    <div class="control-group">
      <label class="control-label"><?= $form->message->labelTag ?></label>
      <div class="controls">
        <?= $form->message->error ?>
        <?= $form->message ?></div>
    </div>

    <div class="control-group">
      <label class="control-label"><?= $form->apply->labelTag ?></label>
      <div class="controls">
        <div class="inline-content">
          <article>
          <?= $this->load->view('static/privacy_common', null, true) ?>
          </article>
        </div>
        <?= $form->apply->error ?>
        <?= $form->apply ?>　
      </div>
    </div>


    <div class="actions text-center">
      <?= $form->hiddenfields ?>
      <button type="submit" class="btn btn-primary">入力内容を確認する</button>
    </div>

    <?= form_close() ?>
  </div>


  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>


