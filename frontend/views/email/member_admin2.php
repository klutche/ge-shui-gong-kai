このメールは、<?= $this->input->server('HTTP_HOST') ?>から送信されました。

●お客様情報
お名前: <?= $member->name . "\r\n" ?>
フリガナ: <?= $member->kana . "\r\n" ?>
郵便番号: <?= $member->zipcode . "\r\n" ?>
住所: <?= $member->address . "\r\n" ?>
電話番号: <?= $member->tel . "\r\n" ?>
メールアドレス: <?= $member->email . "\r\n" ?>
備考: <?= $member->remarks . "\r\n" ?>

●集配先
お名前: <?= $pickup->name . "\r\n" ?>
郵便番号: <?= $pickup->zipcode . "\r\n" ?>
住所: <?= $pickup->address . "\r\n" ?>
電話番号: <?= $pickup->tel . "\r\n" ?>
