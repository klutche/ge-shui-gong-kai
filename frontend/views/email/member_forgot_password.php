<?= $member->name ?> 様

パスワードの再送信が要求されましたので、ご確認ください。

●パスワード
<?= $this->encrypt->decode($member->password) ?>


なお、このメールにお心当たりのない方は、<?= $email_info ?> 宛までご連絡ください。

<?= $this->load->view('signature', NULL, TRUE) ?>

