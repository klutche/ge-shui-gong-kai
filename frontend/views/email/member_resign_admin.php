このメールは、<?= $this->input->server('HTTP_HOST') ?>から送信されました。

<?= $member->name ?> 様が退会されました。

【重要】数日以内に会員管理より <?= $member->name ?>様の削除処理を行ってください。

退会理由: <?= $member->resign_reason ?>

<?/*
●ユーザー環境
UserAgent: <?= $this->input->server('HTTP_USER_AGENT') ?>

Remote Address: <?= $this->input->server('REMOTE_ADDR') ?>

Request Time: <?= date('Y/m/d H:i:s', $this->input->server('REQUEST_TIME')) ?>
 */?>
