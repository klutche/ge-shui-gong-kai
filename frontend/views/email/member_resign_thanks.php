<?= $member->name ?> 様

平素は、革水をご利用いただき誠にありがとうございます。

退会の手続きを承りましたので、5営業日以内にお客様の情報を削除致します。

会員の再登録は、会員登録より承っております。
またご利用の機会がありましたら、ぜひご登録ください。

<?= $this->load->view('signature', null, true) ?>

