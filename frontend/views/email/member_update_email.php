革水のホームページにご登録いただいているメールアドレスの変更リクエストを受け付けました。
メールアドレスの変更を完了させるにはまず下記のリンクをクリックして、メールに記載してある確認コードをご入力ください。

●確認コード入力ページ
<?= get_item('ssl_url') ?>ap/member/verify_email
※ログインした状態でアクセスしてください。

●確認コード
<?= $verify_code ?>


なお、このメールにお心当たりのない方は、<?= $email_info ?> 宛までご連絡ください。

<?= $this->load->view('signature', NULL, TRUE) ?>

