●クリーニング・修理品
<?for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i): ?>
<?if (strlen($step1['item_'.$i]) > 0 && $step1['item_num_'.$i] > 0): ?>
<?= $step1['item_'.$i] ?>
<?= (($step1['item_'.$i] == 'その他' && !empty($step1['item_etc_'.$i])) ? '('.$step1['item_etc_'.$i].')' : '') ?> : <?= $step1['item_num_'.$i] ?>点
<?endif?>
<?endfor?>
高級品: <?= isset($step1['is_expensive']) ? 'あり' : 'なし'."\r\n" ?>


●お客様情報
お名前: <?= $step2['name'].(isset($is_member) && $is_member === true ? ' (会員)' : ' (非会員)')."\r\n" ?>
フリガナ: <?= $step2['kana']."\r\n" ?>
郵便番号: <?= $step2['zipcode']."\r\n" ?>
住所: <?= $step2['address']."\r\n" ?>
電話番号: <?= $step2['tel']."\r\n" ?>
メールアドレス: <?= $step2['email']."\r\n" ?>

●集配
お名前: <?= $step3['name']."\r\n" ?>
郵便番号: <?= $step3['zipcode']."\r\n" ?>
住所: <?= $step3['address']."\r\n" ?>
電話番号: <?= $step3['tel']."\r\n" ?>
集配方法: <?= $step3['method']."\r\n" ?>
<?if ($step3['method'] == 'ヤマト運輸による集配'): ?>
ご希望の集荷日: <?= $step3['date']."\r\n" ?>
ご希望の集荷時間: <?= $step3['time']."\r\n" ?>
梱包箱を希望: <?= $step3['box']."\r\n" ?>
<?endif?>

●お支払い
<?= $step4['payment']."\r\n" ?>

●ご要望/備考
<?= $step5['message']."\r\n" ?>

<?php if (isset($step5['question_1'])) { ?>
●当サイトをどこで知りましたか？
<?php foreach ($step5['question_1'] as $val) { ?>
<?= $val."\r\n" ?>
<?php } ?>

<?php if (strlen($step5['remarks_1']) > 0) { ?>
●その他SNS
<?= $step5['remarks_1']."\r\n" ?>

<?php } ?>
<?php if (strlen($step5['remarks_2']) > 0) { ?>
●知人からの口コミ
<?= $step5['remarks_2']."\r\n" ?>

<?php } ?>
<?php if (strlen($step5['remarks_3']) > 0) { ?>
●口コミサイトを見て
<?= $step5['remarks_3']."\r\n" ?>

<?php } ?>
<?php if (strlen($step5['remarks_4']) > 0) { ?>
●その他
<?= $step5['remarks_4']."\r\n" ?>

<?php } ?>
<?php  } ?>