●お客様情報
お名前: <?= $step1['name'] . (isset($is_member) && $is_member === true ? ' (会員)' : ' (非会員)') . "\r\n" ?>
フリガナ: <?= $step1['kana'] . "\r\n" ?>
郵便番号: <?= $step1['zipcode'] . "\r\n" ?>
住所: <?= $step1['address'] . "\r\n" ?>
電話番号: <?= $step1['tel'] . "\r\n" ?>
メールアドレス: <?= $step1['email'] . "\r\n" ?>

●集配
お名前: <?= $step2['name'] . "\r\n" ?>
郵便番号: <?= $step2['zipcode'] . "\r\n" ?>
住所: <?= $step2['address'] . "\r\n" ?>
電話番号: <?= $step2['tel'] . "\r\n" ?>
集配方法: <?= $step2['method'] . "\r\n" ?>
<?if ($step2['method'] != '手配必要なし'): ?>
ご希望の集荷日: <?= $step2['pickup_date'] . "\r\n" ?>
ご希望の集荷時間: <?= $step2['pickup_time'] . "\r\n" ?>
<?endif?>
ご希望の配送日: <?= $step2['delivery_date'] . "\r\n" ?>
ご希望の配送時間: <?= $step2['delivery_time'] . "\r\n" ?>
注文バッグ(大): <?= $step2['bag_large'] . "\r\n" ?>
注文バッグ(小): <?= $step2['bag_small'] . "\r\n" ?>

●お支払い
<?= $step3['payment'] . "\r\n" ?>

●ご要望/備考
<?= $step4['message'] . "\r\n" ?>

