<div class="row-fluid">
    <div class="span9" id="main" role="main">
        <h1><?= $title ?></h1>
        <?= form_open('estimate/send', array('class' => 'form-vertical')) ?>
            <div class="control-group">
                <label class="control-label"><?= $form->name->label ?></label>
                <div class="controls">
                    <?= $form->name ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?= $form->kana->label ?></label>
                <div class="controls">
                    <?= $form->kana ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?= $form->email->label ?></label>
                <div class="controls">
                    <?= $form->email ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?= $form->tel->label ?></label>
                <div class="controls">
                    <?= $form->tel ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?= $form->prefcode->label ?></label>
                <div class="controls">
                    <span class="cleaned_data"><?= get_item('prefcode')[$form->cleaned_data['prefcode']] ?></span>
                    <span style="display: none;"><?= $form->prefcode ?></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">クリーニング・修理品</label>
<?php
for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
    if (strlen($form->cleaned_data['item_'.$i]) > 0) {
?>
                <div class="controls">
                    <?= $form->{'item_'.$i} ?>
<?php if ($form->cleaned_data['item_'.$i] == 'その他') { ?>
                    （<?= $form->cleaned_data['item_etc_'.$i] ?>）
<?php } ?>
                    <span style="display: none;"><?= $form->{'item_etc_'.$i} ?></span>
                    <span class="item-num-label">数量:</span>
                    <?= $form->{'item_num_'.$i} ?>
                </div>
<?php
    }
}
?>
            </div>
            <div class="control-group">
                <label class="control-label"><?= $form->is_expensive->label ?></label>
                <div class="controls">
                    <?= isset($form->cleaned_data['is_expensive']) ? 'あり' : 'なし' ?>
                    <span style="display: none;"><?= $form->is_expensive ?></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?= $form->pickup_boxes->label ?></label>
                <div class="controls">
                    <?= $form->pickup_boxes ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?= $form->message->label ?></label>
                <div class="controls">
                    <?= $form->message ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?= $form->upload->label ?></label>
                <div class="controls">
                    <?= form_hidden('upload', isset($upload_filename) ? $upload_filename : '') ?>
<?php if (isset($upload_file_url)): ?>
                    <img src="<?= $upload_file_url ?>" alt="" width="240" class="img-polaroid">
<?php endif; ?>
                </div>
            </div>
            <div class="actions text-center">
                <?= form_hidden('apply', 1) ?>
                <input type="submit" class="btn" name="back" value="戻る" />
                <input type="submit" class="btn btn-primary" name="send" value="送信する" />
            </div>
        <?= form_close() ?>
    </div>
    <div class="span3" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
