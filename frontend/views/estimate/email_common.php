【お客様情報】

◇お名前
<?= $data['name']."\r\n" ?>

◇フリガナ
<?= $data['kana']."\r\n" ?>

◇メールアドレス
<?= $data['email']."\r\n" ?>

◇電話番号
<?= $data['tel']."\r\n" ?>

◇集荷先
<?= get_item('prefcode')[$data['prefcode']]."\r\n" ?>

◇クリーニング品
<?php
for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) {
    if (strlen($data['item_'.$i]) > 0) {
?>
<?= $data['item_'.$i] ?><?= $data['item_'.$i] == 'その他' ? '（'.$data['item_etc_'.$i].'）' : '' ?><?= '　数量：'.$data['item_num_'.$i] ?><?= "\r\n" ?>
<?php
    }
}
?>

◇高級品の有無
<?= $data['is_expensive'] == '1' ? 'あり' : 'なし'."\r\n" ?>

◇依頼品のサイズ
<?= $data['pickup_boxes']."\r\n" ?>

◇ご要望・汚れの程度
<?= $data['message']."\r\n" ?>
