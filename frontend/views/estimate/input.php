<div class="row-fluid">
    <div class="span9" id="main" role="main">
        <h1><?= $title ?></h1>
        <?= $this->load->view('secure_seal', null, true) ?>
        <?= form_open_multipart('estimate/input', array('class' => 'form-vertical')) ?>
            <div class="control-group">
                <?= $form->name->labelTag ?>
                <div class="controls">
                    <?= $form->name->error ?>
                    <?= $form->name ?>
                </div>
            </div>
            <div class="control-group">
                <?= $form->kana->labelTag ?>
                <div class="controls">
                    <?= $form->kana->error ?>
                    <?= $form->kana ?>
                </div>
            </div>
            <div class="control-group">
                <?= $form->email->labelTag ?>
                <div class="controls">
                    <?= $form->email->error ?>
                    <?= $form->email ?>
                    <?= $form->email->help_text ?>
                </div>
            </div>
            <div class="control-group">
                <?= $form->tel->labelTag ?>
                <div class="controls">
                    <?= $form->tel->error; ?>
                    <?= $form->tel ?>
                </div>
            </div>
            <div class="control-group">
                <?= $form->prefcode->labelTag ?>
                <div class="controls">
                    <?= $form->prefcode->error; ?>
                    <?= $form->prefcode ?>
                    <?= $form->prefcode->help_text ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">クリーニング・修理品<span class="required label label-important">必須</span></label>
<?php for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i) { ?>
                <div class="controls">
                    <?= $form->{'item_' . $i}->error ?>
                    <?= $form->{'item_etc_' . $i}->error ?>
                    <?= $form->{'item_num_' . $i}->error ?>
                    <?= $form->{'item_' . $i} ?>
                     <?= $form->{'item_etc_' . $i} ?>
                    <span class="item-num-label">数量:</span>
                    <?= $form->{'item_num_' . $i} ?>
                </div>
<?php } ?>
                <span class="help-block">※項目が該当しない場合は、その他に「ブランド名・アイテム名・素材など」をご記入ください。<br>
                例）その他（ロエベ・ショール・ウール）</span>
            </div>
            <div class="control-group">
                <?= $form->is_expensive->labelTag ?>
                <div class="controls">
                    <?= $form->is_expensive->error ?>
                    <?= $form->is_expensive ?>
                    <?= $form->is_expensive->help_text ?>
                </div>
            </div>
            <div class="control-group">
                <?= $form->pickup_boxes->labelTag ?>
                <div class="controls">
                    <?= $form->pickup_boxes->error; ?>
                    <?= $form->pickup_boxes ?>
                    <?= $form->pickup_boxes->help_text ?>
                </div>
            </div>
            <div class="control-group">
                <?= $form->message->labelTag ?>
                <div class="controls">
                    <?= $form->message->error ?>
                    <?= $form->message ?>
                    <?= $form->message->help_text ?>
                </div>
            </div>
            <div class="control-group">
                <?= $form->upload->labelTag ?>
                <div class="controls">
                    <?= $form->upload->error ?>
                    <?if (isset($upload_error)): ?>
                    <div class="alert alert-error">
                        <?= $upload_error ?>
                    </div>
                    <?endif?>
                    <?= $form->upload ?>
                    <?= $form->upload->help_text ?>
                </div>
            </div>
            <div class="control-group">
                <?= $form->apply->labelTag ?>
                <div class="controls">
                    <div class="inline-content">
                        <article>
                            <?= $this->load->view('static/privacy_common', null, true) ?>
                        </article>
                    </div>
                    <?= $form->apply->error ?>
                    <?= $form->apply ?>
                </div>
            </div>
            <div class="actions text-center">
                <?= $form->hiddenfields ?>
                <button type="submit" class="btn btn-primary">入力内容を確認する</button>
            </div>
        <?= form_close() ?>
    </div>
    <div class="span3" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>