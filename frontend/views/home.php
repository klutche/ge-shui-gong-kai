<div class="row-fluid">
  <div class="span9" id="main" role="main" style="padding: 0">
    <nav id="nav-home">
    <ul>
      <li>
        <a href="<?= site_url('order/input') ?>">ご注文</a>
      </li>
      <li>
        <a href="<?= site_url('estimate/input') ?>">クリーニング品お問い合わせ</a>
      </li>
      <li>
        <a href="<?= site_url('contact/input') ?>">お問い合わせ</a>
      </li>
<?/*
      <li>
        <a href="<?= site_url('business/input') ?>">業者</a>
      </li>
      <li>
        <a href="<?= site_url('care/input') ?>">お手入れクリーニング</a>
      </li>
      <li>
        <a href="<?= site_url('washstyle/input') ?>">ウォッシュスタイル</a>
      </li>
 */?>
    </ul>
    </nav>

  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>


