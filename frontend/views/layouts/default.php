<!DOCTYPE HTML>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title><?= isset($title) ? $title : 'Untitled' ?> | <?= get_item('site_name') ?></title>
    <!--[if lt IE 9]>
    <script src="<?= site_url('assets/js/libs/html5shiv.js') ?>"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?= site_url('assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= site_url('assets/css/bootstrap-responsive.min.css') ?>">
    <link rel="stylesheet" href="<?= site_url('assets/css/style.css') ?>">
<?if (isset($styles) && is_array($styles)): ?>
<?foreach ($styles as $style): ?>
    <link rel="stylesheet" href="<?= site_url($style) ?>">
<?endforeach?>
<?endif?>
</head>
<body>

    <div id="lead">革・皮革製品（ 靴・バッグ ）の汚れやシミをクリーニング・水洗いの専門店　革水（皮水／かわすい）</div>
<header id="header">
    <div class="container-fluid">
    <h1 id="brand"><a href="/"><img src="<?= site_url('assets/img/logo.png') ?>" alt="革水"></a></h1>
    <div id="header-login">
        <?if ($this->session->userdata('logged_in')): ?>
        <a href="<?= site_url('member/logout') ?>">ログアウト</a>
        <?else:?>
        <a href="<?= site_url('member/login') ?>">ログイン</a>
        <?endif?>
    </div>
    </div>
    <div id="tel-box">お電話でのお問い合せ：TEL 096-234-8677　お電話受付時間：月～金 9：00～18：00/土 9:00〜17:30 （日・祝日は休み）</div>
</header>
<div class="container-fluid">
<?php
$notifier = notifier();
if ($notifier):?>
<div id="notifier">
    <div class="alert alert-<?= $notifier->type ?>">
    <a class="close" data-dismiss="alert">×</a>
    <?= $notifier->msg ?>
    </div>
</div>
<?endif?>

{yield}
</div>

<footer id="footer">
    <div class="container-fluid">
        <p id="copyright"><small>&copy; Kawasui.</small></p>
    </div>
</footer>


<script>
var base_url = '<?= site_url() ?>';
</script>
<script src="<?= site_url('assets/js/libs/jquery-1.9.1.min.js') ?>"></script>
<script src="<?= site_url('assets/js/libs/jquery.pjax.js') ?>"></script>
<script src="<?= site_url('assets/js/libs/jquery.cookie.js') ?>"></script>
<script src="<?= site_url('assets/js/libs/jquery.flickable.js') ?>"></script>
<script src="<?= site_url('assets/js/libs/bootstrap.min.js') ?>"></script>
<script src="<?= site_url('assets/js/application.js') ?>"></script>
<?if (isset($scripts) && is_array($scripts)): ?>
<?foreach ($scripts as $script): ?>
<script src="<?= site_url($script) ?>"></script>
<?endforeach?>
<?endif?>


<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5R4DQR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5R4DQR');</script>
<!-- End Google Tag Manager -->
</body>
</html>
