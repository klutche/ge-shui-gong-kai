<!DOCTYPE HTML>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title><?= isset($title) ? $title : 'Untitled' ?> | <?= get_item('site_name') ?></title>
    <!--[if lt IE 9]>
    <script src="<?= site_url('assets/js/libs/html5shiv.js') ?>"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?= site_url('assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= site_url('assets/css/bootstrap-responsive.min.css') ?>">
    <link rel="stylesheet" href="<?= site_url('assets/css/style.css') ?>">
<?if (isset($styles) && is_array($styles)): ?>
<?foreach ($styles as $style): ?>
    <link rel="stylesheet" href="<?= site_url($style) ?>">
<?endforeach?>
<?endif?>
</head>
<body>

{yield}

<script>
var base_url = '<?= site_url() ?>';
</script>
<script src="<?= site_url('assets/js/libs/jquery-1.9.1.min.js') ?>"></script>
<script src="<?= site_url('assets/js/libs/bootstrap.min.js') ?>"></script>
<script src="<?= site_url('assets/js/application.js') ?>"></script>
<?if (isset($scripts) && is_array($scripts)): ?>
<?foreach ($scripts as $script): ?>
<script src="<?= site_url($script) ?>"></script>
<?endforeach?>
<?endif?>
</body>
</html>

