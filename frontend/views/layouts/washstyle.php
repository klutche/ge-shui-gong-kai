<!DOCTYPE HTML>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title><?= isset($title) ? $title : 'Untitled' ?> | <?= get_item('site_name') ?></title>
    <!--[if lt IE 9]>
    <script src="<?= site_url('assets/js/libs/html5shiv.js') ?>"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?= site_url('assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= site_url('assets/css/bootstrap-responsive.min.css') ?>">
    <link rel="stylesheet" href="<?= site_url('assets/css/style.css') ?>">
<?if (isset($styles) && is_array($styles)): ?>
<?foreach ($styles as $style): ?>
    <link rel="stylesheet" href="<?= site_url($style) ?>">
<?endforeach?>
<?endif?>
</head>
<body style="background-color: #fff; padding: 0;">

<div class="container-fluid">
<?php
$notifier = notifier();
if ($notifier):?>
<div id="notifier">
    <div class="alert alert-<?= $notifier->type ?>">
    <a class="close" data-dismiss="alert">×</a>
    <?= $notifier->msg ?>
    </div>
</div>
<?endif?>

{yield}
</div>


<script>
var base_url = '<?= site_url() ?>';
</script>
<script src="<?= site_url('assets/js/libs/jquery-1.9.1.min.js') ?>"></script>
<script src="<?= site_url('assets/js/libs/jquery.pjax.js') ?>"></script>
<script src="<?= site_url('assets/js/libs/jquery.cookie.js') ?>"></script>
<script src="<?= site_url('assets/js/libs/jquery.flickable.js') ?>"></script>
<script src="<?= site_url('assets/js/libs/bootstrap.min.js') ?>"></script>
<script src="<?= site_url('assets/js/application.js') ?>"></script>
<?if (isset($scripts) && is_array($scripts)): ?>
<?foreach ($scripts as $script): ?>
<script src="<?= site_url($script) ?>"></script>
<?endforeach?>
<?endif?>


<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5R4DQR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5R4DQR');</script>
<!-- End Google Tag Manager -->
</body>
</html>
