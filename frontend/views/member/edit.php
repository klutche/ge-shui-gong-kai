<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <?= form_open('member/edit') ?>
    <?if (isset($error)): ?>
    <div class="alert alert-error">
      <?= $error ?>
    </div>
    <?endif?>

    <?= $form ?>

    <div class="actions">
      <?= form_submit('update', '保存する', 'class="btn btn-primary"') ?>
    </div>
    <?= form_close() ?>

    <?if (isset($member) && strlen($member->name) > 0): ?>
        <div class="text-right">
        <a href="<?= site_url('member/resign') ?>">退会する</a>
        </div>
    <?endif?>
  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>

