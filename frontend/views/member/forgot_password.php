<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>
    <p>パスワードを再送信しますので、登録したメールアドレスを入力し、[再送信]ボタンを押してください。</p>
    <?= form_open('member/forgot_password') ?>
    <?if (isset($error)): ?>
    <div class="alert alert-error">
      <?= $error ?>
      <a class="btn btn-primary" href="<?= site_url('member/signup') ?>">新規会員登録はこちら</a>
    </div>
    <?endif?>

    <?= $form ?>

    <div class="actions">
      <?= form_submit('submit', '再送信', 'class="btn btn-primary"') ?>
    </div>
    <?= form_close() ?>

  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>

