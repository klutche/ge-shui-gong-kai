<div class="main-sub main-form" id="main">

<section id="member-index">
<h1><?= $title ?></h1>

<div class="container">
<div class="span11">

<ul>
<li><a href="<?= site_url('member/edit') ?>">会員情報の変更</a></li>
<li><a href="<?= site_url('pickup/index') ?>">集荷先の管理</a></li>
<li><a href="<?= site_url('member/update_password') ?>">パスワードの変更</a></li>
<li><a href="<?= site_url('member/resign') ?>">退会</a></li>
</ul>

</div>
</div>

</section>
</div>



