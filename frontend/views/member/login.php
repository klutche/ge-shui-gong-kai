<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <?= form_open('member/login') ?>
    <?if (isset($error)): ?>
    <div class="alert alert-error">
      <?= $error ?>
    </div>
    <?endif?>

    <?= $form ?>

    <div class="actions">
      <?= $form->hiddenfields ?>
      <?= form_submit('login', 'ログイン', 'class="btn btn-primary"') ?>
    </div>
    <?= form_close() ?>

    <p><a href="<?= site_url('member/forgot_password') ?>">パスワードを忘れた場合</a></p>
    <p><a href="<?= site_url('member/signup') ?>">会員登録</a></p>

  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>

