<section style="padding:10px 10px 0;">
<? $this->load->view('common/breadlist'); ?>
</section>
<section style="padding:10px;">
<h1>会員ログイン</h1>

<?= form_open('member/login', array('id' => 'form-login')) ?>
<?= $form ?>

<div class="actions">
<?= form_submit('login', 'ログイン') ?>
</div>
<?= form_close() ?>

<form id="postform" method="POST" action="<?= $this->config->item('chintai_url') ?>member/login" target="iframe">
<input type="hidden" name="username" value="" />
<input type="hidden" name="password" value="" />
</form>
<iframe name="iframe"></iframe>
<script>
$(document).ready(function() {
    $('#form-login').submit(function() {
        $('#postform input[name=username]').val($('#form-login input[name=username]').val());
        $('#postform input[name=password]').val($('#form-login input[name=password]').val());
        $('#postform').submit();
    });
});
</script>


<p><a href="<?= site_url('member/signup') ?>">会員登録</a></p>
</section>

