<div class="row-fluid">
    <div class="span9" id="main" role="main">
        <h1><?= $title ?></h1>
<?php if ($error != '') { ?>
        <p class="alert alert-error"><?= $error ?></p>
<?php } else { ?>
        <div class="control-group" id="memberfullform-field-0">
            <label class="control-label">メールアドレス</label>
            <div class="controls">
                <?= $email ?>
            </div>
        </div>
        <?= form_open() ?>
            <?= $form ?>
            <div class="actions">
                <?= form_submit('update', '保存する', 'class="btn btn-primary"') ?>
            </div>
        <?= form_close() ?>
<?php } ?>
    </div>
    <div class="span3" id="sidebar">
        <?= $this->load->view('sidebar', null, true) ?>
    </div>
</div>
