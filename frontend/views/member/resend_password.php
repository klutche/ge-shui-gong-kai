<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <div class="alert alert-info">
      パスワードを登録メールアドレス宛に送信しましたので、ご確認ください。
    </div>

  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>
