<div class="main-sub main-form" id="main">


<h1><?= $title ?></h1>

<div class="container">
<div class="form span4">
<?= form_open('member/resign') ?>
<?if (isset($error)): ?>
<div class="alert alert-error">
<?= $error ?>
</div>
<?endif?>

<?= $form ?>

<div class="actions">
<?= form_submit('submit', '退会する', 'class="btn btn-danger"') ?>
</div>
<?= form_close() ?>

</div>

<div class="span7">
<h2 class="h2">退会についての注意点</h2>
<ul>
<li>退会後、数日以内に登録情報が削除されます。</li>
<li>退会後はログインすることができません。</li>
<li>退会後、再度ご登録いただくと会員機能が利用できます。</li>
</ul>
</div>
</div>

</div>


