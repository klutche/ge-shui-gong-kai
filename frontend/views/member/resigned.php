<div class="main-sub main-form" id="main">


<section id="member-resigned">
<h1><?= $title ?></h1>

<div class="container">
<div class="span11">
<div class="alert alert-info">
<strong>退会処理を行いました。</strong>ご利用ありがとうございました。<br />
10秒後、自動的にログアウトします。
</div>

<script>
setTimeout(function() {
    location.href = '/ap/member/logout';
}, 10000);
</script>

</div>
</div>

</section>
</div>




