<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <div class="row-fluid">
    <?if (preg_match('/iPhone|Android/i', $this->input->server('HTTP_USER_AGENT'))): ?>
    <div class="span7">
        <img src="<?= site_url('assets/img/signup_merit.png') ?>" alt="">
        <!-- h2>革水会員の皆様へ</h2>

        <p>会員様向けサービスの向上に伴い、新機能及び特典をご利用頂くためにはメールアドレスとパスワードが必要となりました。<br>
          すでにご登録いただきました会員様につきましても、 メールアドレスとパスワードが必要となりますので、大変お手数でございますが、ご登録のお手続きをお願い申し上げます。</p -->

    </div>
    <?endif?>
    <div class="span5">
    <?= $this->load->view('secure_seal', null, true) ?>

    <?= form_open('member/signup') ?>
    <?if (isset($error)): ?>
    <div class="alert alert-error">
      <?= $error ?>
    </div>
    <?endif?>

    <?= $form ?>

    <div class="actions">
      <p>[登録する]ボタンを押すと、承認用のメールが送られますので、携帯のメールアドレスをお使いの方は<?= get_item('email_domain') ?>からのメールを受けとれるように、ドメイン指定をお願い致します。</p>

      <?= form_submit('login', '登録する', 'class="btn btn-primary"') ?>
    </div>
    <?= form_close() ?>
      <p><a href="<?= site_url('member/login') ?>">ログイン</a></p>
    </div>
    <?if ( ! preg_match('/iPhone|Android/i', $this->input->server('HTTP_USER_AGENT'))): ?>
    <div class="span7">
        <img src="<?= site_url('assets/img/signup_merit.png') ?>" alt="">
        <!--div style="margin-top: 20px;" class="member-notice">
        <h2>革水会員の皆様へ</h2>

        <p>会員様向けサービスの向上に伴い、新機能及び特典をご利用頂くためにはメールアドレスとパスワードが必要となりました。<br>
          すでにご登録いただきました会員様につきましても、 メールアドレスとパスワードが必要となりますので、大変お手数でございますが、ご登録のお手続きをお願い申し上げます。</p>
        </div -->

    </div>
    <?endif?>
    </div>
  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>

