<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <div class="alert alert-info">
      会員登録を受付ました。<br />
      <strong style="color: #c33">まだ会員登録は完了しておりません。</strong><br />
      <strong><span style="color: #c33">入力いただいたメールアドレス宛に承認用のメールを送信</span>しておりますので、メールに記載されているURLヘアクセスして会員登録を完了させてください。</strong>
    </div>

    <div class="alert alert-info">
      GMailやYahooメールなどお使いの方は、メールが迷惑メールとして処理されることがありますので、ご注意ください。
    </div>

  </div>
  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>

