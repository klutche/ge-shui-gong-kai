<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>
    <?= form_open('member/update_email') ?>
    <?if (isset($error)): ?>
    <div class="alert alert-error">
      <?= $error ?>
    </div>
    <?endif?>

    <div class="control-group">
        <label class="control-label">現在のメールアドレス</label>
        <div>
            <?= $current_email ?>
        </div>
    </div>
    <?= $form ?>

    <div class="actions">
      <?= form_submit('submit', '送信', 'class="btn btn-primary"') ?>
    </div>
    <?= form_close() ?>

  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>


