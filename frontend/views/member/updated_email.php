<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>
    <div class="alert alert-info">
      メールアドレスを更新しました。
    </div>

  </div>
  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>


