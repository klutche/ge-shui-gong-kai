<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <div class="alert alert-info">
      メールアドレス確認が完了いたしました。<a href="<?= site_url('member/login') ?>">ログイン</a>後、お客様情報の入力へお進みください。
    </div>

    <?= form_open('member/login') ?>
    <?if (isset($error)): ?>
    <div class="alert alert-error">
      <?= $error ?>
    </div>
    <?endif?>

    <?= $form ?>

    <div class="actions">
      <?= form_submit('login', 'ログイン', 'class="btn btn-primary"') ?>
    </div>
    <?= form_close() ?>
  </div>
  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>


