<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>
    <div class="alert alert-success">
    <p><strong style="color: #c33">※まだメールアドレスの変更は完了していません。</strong></p>

    <p>確認コードを記載したメールを新しいメールアドレス宛に送信しました。<br>
    新しいメールアドレスに変更するには、メールに記載されている確認コードをご入力ください。</p>
    </div>

    <?= form_open('member/verify_email') ?>
    <?if (isset($error)): ?>
    <div class="alert alert-error">
      <?= $error ?>
    </div>
    <?endif?>

    <?= $form ?>

    <div class="actions">
      <?= form_submit('submit', '送信', 'class="btn btn-primary"') ?>
    </div>
    <?= form_close() ?>

  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>


