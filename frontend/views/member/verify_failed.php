<div class="main-sub main-form" id="main">


<section id="member-update-passowrd">
<h1><?= $title ?></h1>

<div class="container">
<div class="span11">
<div class="alert alert-error">
<p>メールアドレスの認証に失敗しました。以下の点をご確認ください。</p>
<ul>
<li>URLが間違っていないか。(2行になっている場合は、1行にしてからブラウザのアドレス欄に貼り付けてください。)</li>
<li>すでに認証が完了していないか。（認証済みであれば、ログイン後ご利用ください）</li>
</ul>
</div>

</div>
</div>

</section>
</div>



