<?= form_open('order/partial/1', array('class' => 'partial')) ?>
<div class="control-group">
<?for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i): ?>
<div class="controls">
<?= $form->{'item_' . $i}->error ?>
<?= $form->{'item_etc_' . $i}->error ?>
<?= $form->{'item_num_' . $i}->error ?>
<?= $form->{'item_' . $i} ?>
 <?= $form->{'item_etc_' . $i} ?>
<span class="item-num-label">数量:</span>
<?= $form->{'item_num_' . $i} ?>
</div>
<?endfor?>

<span class="help-block">
※項目が該当しない場合は、その他に「ブランド名・アイテム名・素材など」をご記入ください。<br>
例）その他（ロエベ・ショール・ウール）</span>
</div>

<div class="control-group">
<?= $form->is_expensive->labelTag ?>
<div class="controls">
<?= $form->is_expensive->error ?>
<?= $form->is_expensive ?>
<?= $form->is_expensive->help_text ?>
</div>
</div>

<div class="actions">
<button type="submit" class="btn btn-primary">続ける</button>
</div>
<?= form_close() ?>
