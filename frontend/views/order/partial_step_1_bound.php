<div class="control-group">
<table class="table" style="border-bottom: 1px solid #ddd">
<?for ($i = 0, $max = get_item('max_order_item'); $i < $max; ++$i): ?>
<?if (strlen($input['item_' . $i]) > 0 && $input['item_num_' . $i] > 0): ?>
<tr>
<td>
<?= $input['item_' . $i] ?>
<?if ($input['item_' . $i] == 'その他' && !empty($input['item_etc_' . $i])): ?>
(<?= $input['item_etc_' . $i] ?>)
<?endif?>
</td>
<td>
数量:&nbsp;
<?= $input['item_num_' . $i] ?>
</td>
</tr>
<?endif?>
<?endfor?>
</table>
</div>

<div class="control-group">
<label class="control-label">高級品</label>
<div class="controls">
<?= (isset($input['is_expensive']) ? 'あり' : 'なし') ?>
</div>
</div>
