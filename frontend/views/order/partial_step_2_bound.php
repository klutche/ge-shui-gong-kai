<div class="control-group">
<label class="control-label">お名前</label>
<div class="controls">
<?= $input['name'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">郵便番号</label>
<div class="controls">
〒<?= $input['zipcode'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">住所</label>
<div class="controls">
<?= $input['address'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">電話番号</label>
<div class="controls">
<?= $input['tel'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">メールアドレス</label>
<div class="controls">
<?= $input['email'] ?>
</div>
</div>
