<?= form_open('order/partial/4', array('class' => 'partial')) ?>
<?= $form ?>
<div class="help-block">
＜代金引換＞<br />
※ 財布・小物単品の場合、「代金引換」は御利用いただけません。<br />
※ 代金引換の場合は、現金のみとなります。<br />
＜銀行振込＞<br />
※ 銀行振込は 肥後銀行・ゆうちょ銀行・PayPay銀行(旧ジャパンネット銀行)の口座をご準備しております。<br />
＜クレジットカード決済＞<br />
※ Paypal決済を利用してクレジットカード払いができます。詳しくはクレジットカード決済についてページをご覧ください。
</div>
<div class="actions">
<button type="submit" class="btn btn-primary">続ける</button>
</div>
<?= form_close() ?>

