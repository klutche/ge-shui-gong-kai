<?= form_open('order/partial/5', array('class' => 'partial')) ?>
    <div class="control-group">
        <?= $form->message->labelTag ?>
        <div class="controls">
            <?= $form->message->error; ?>
            <?= $form->message ?>
        </div>
    </div>
    <div class="control-group">
        <?= $form->question_1->labelTag ?>
        <div class="controls" id="questions">
            <?= $form->question_1->error; ?>
            <?= $form->question_1 ?>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <?= $form->remarks_1->error; ?>
            <?= $form->remarks_1 ?>
            <?= $form->remarks_2->error; ?>
            <?= $form->remarks_2 ?>
            <?= $form->remarks_3->error; ?>
            <?= $form->remarks_3 ?>
            <?= $form->remarks_4->error; ?>
            <?= $form->remarks_4 ?>
        </div>
    </div>
    <div class="actions">
        <button type="submit" class="btn btn-primary">続ける</button>
    </div>
<?= form_close() ?>

<script>
$(function() {
    $('#id_question_1_7').parent('label').after($('#id_remarks_1'));
    $('#id_question_1_8').parent('label').after($('#id_remarks_2'));
    $('#id_question_1_9').parent('label').after($('#id_remarks_3'));
    $('#id_question_1_10').parent('label').after($('#id_remarks_4'));
});
</script>