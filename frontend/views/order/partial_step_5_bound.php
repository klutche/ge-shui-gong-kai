<?php if (strlen($input['message']) > 0) { ?>
<div class="control-group">
    <label class="control-label">ご要望・備考</label>
    <div class="controls">
        <?= nl2br($input['message']) ?>
    </div>
</div>
<?php } ?>
<?php if (count($input['question_1']) > 0) { ?>
<div class="control-group">
    <label class="control-label">当サイトをどこで知りましたか？</label>
    <div class="controls">
<?php foreach($input['question_1'] as $val) { ?>
        <?= $val ?><br>
<?php } ?>
    </div>
</div>
<?php } ?>
<?php if (strlen($input['remarks_1']) > 0) { ?>
<div class="control-group">
    <label class="control-label">その他SNS</label>
    <div class="controls">
        <?= nl2br($input['remarks_1']) ?>
    </div>
</div>
<?php } ?>
<?php if (strlen($input['remarks_2']) > 0) { ?>
<div class="control-group">
    <label class="control-label">知人からの口コミ</label>
    <div class="controls">
        <?= nl2br($input['remarks_2']) ?>
    </div>
</div>
<?php } ?>
<?php if (strlen($input['remarks_3']) > 0) { ?>
<div class="control-group">
    <label class="control-label">口コミサイトを見て</label>
    <div class="controls">
        <?= nl2br($input['remarks_3']) ?>
    </div>
</div>
<?php } ?>
<?php if (strlen($input['remarks_4']) > 0) { ?>
<div class="control-group">
    <label class="control-label">その他</label>
    <div class="controls">
        <?= nl2br($input['remarks_4']) ?>
    </div>
</div>
<?php } ?>
