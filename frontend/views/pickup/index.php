<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <p class="text-right"><a class="btn btn-primary" href="<?= site_url('pickup/add') ?>">集配先の追加</a></p>

    <?if (is_array($pickups) && count($pickups) > 0): ?>
    <table class="table">
      <thead>
        <tr>
          <th>名前</th>
          <th>郵便番号</th>
          <th>住所</th>
          <th>TEL</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?foreach ($pickups as $pickup): ?>
        <tr>
          <td><?= $pickup->name ?></td>
          <td><?= $pickup->zipcode ?></td>
          <td><?= $pickup->address ?></td>
          <td><?= $pickup->tel ?></td>
          <td><a class="btn btn-primary" href="<?= site_url('pickup/edit/' . $pickup->id) ?>">編集</a></td>
        </tr>
        <?endforeach?>
      </tbody>
    </table>
    <?endif?>

  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>

