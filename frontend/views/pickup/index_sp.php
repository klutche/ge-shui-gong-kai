<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <p class="text-right"><a class="btn btn-primary" href="<?= site_url('pickup/add') ?>">集配先の追加</a></p>

    <?if (is_array($pickups) && count($pickups) > 0): ?>
    <table class="table">
      <tbody>
        <?foreach ($pickups as $pickup): ?>
        <tr>
          <td><?= $pickup->name ?><br>
          〒<?= $pickup->zipcode ?><br>
          <?= $pickup->address ?><br>
          <?= $pickup->tel ?><br>
          <a class="btn btn-primary pull-right" href="<?= site_url('pickup/edit/' . $pickup->id) ?>">編集</a></td>
        </tr>
        <?endforeach?>
      </tbody>
    </table>
    <?endif?>

  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>

