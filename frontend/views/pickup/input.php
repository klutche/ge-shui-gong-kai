<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>
    <?= form_open($action) ?>
    <?if (isset($error)): ?>
    <div class="alert alert-error">
      <?= $error ?>
    </div>
    <?endif?>

    <?= $form ?>

    <div class="actions">
      <?= form_submit('save', $btn_label, 'class="btn btn-primary"') ?>
      <?if (isset($has_delete_link)): ?>
      <a href="<?= site_url('pickup/delete/' . $id) ?>" class="pull-right">削除する</a>
      <?endif?>

    </div>
    <?= form_close() ?>

  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>


