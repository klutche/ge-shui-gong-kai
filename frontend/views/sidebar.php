<?if (isset($is_static)): ?>
<aside class="aside-sidebar">
<nav>
<ul>
      <li>
        <a href="<?= site_url('order/input') ?>">ご注文</a>
      </li>
      <li>
        <a href="<?= site_url('estimate/input') ?>">クリーニング・修理品無料見積もり</a>
      </li>
      <li>
        <a href="<?= site_url('contact/input') ?>">お問い合わせ</a>
      </li>
</ul>
</nav>
</aside>
<?endif?>

<?if ($this->session->userdata('logged_in')):?>
<aside class="aside-sidebar">
<nav>
<ul>
  <li>
  <a href="<?= site_url('member/edit') ?>">お客様情報の変更</a>
  </li>
  <li>
  <a href="<?= site_url('pickup/index') ?>">集配先の管理</a>
  </li>
  <li>
  <a href="<?= site_url('member/update_email') ?>">メールアドレスの確認・変更</a>
  </li>
  <li>
  <a href="<?= site_url('member/update_password') ?>">パスワードの変更</a>
  </li>
  <li>
  <a href="<?= site_url('member/logout') ?>">ログアウト</a>
  </li>
</ul>
</nav>
</aside>
<?elseif (isset($login_form)):?>
<aside class="aside-sidebar">
<?= form_open('member/login') ?>
<p>会員の方はログイン後にご利用ください。</p>
<?= $login_form ?>
<div class="actions">
  <?= $login_form->hiddenfields ?>
  <button type="submit" class="btn btn-primary">ログイン</button>
</div>

<div class="sub-actions">
  <a href="https://www.kawasui.com/ap/member/forgot_password">パスワードを忘れた場合</a><br>
  <a href="https://www.kawasui.com/ap/member/signup">会員登録</a>
</div>
<?= form_close() ?>
</aside>
<?endif?>


<aside class="aside-sidebar">
<nav>
<ul>
<?if (! $this->session->userdata('logged_in')):?>
  <li>
  <a href="<?= site_url('member/signup') ?>">会員特典</a>
  </li>
<?endif?>
  <li>
  <a href="<?= site_url('static/specified_commercial_transactions_act') ?>">特定商取引法に基づく表記</a>
  </li>
  <li>
  <a href="<?= site_url('static/term_service') ?>">利用規約</a>
  </li>
  <li>
  <a href="<?= site_url('static/standards_compensation') ?>">事故賠償基準</a>
  </li>
  <li>
  <a href="<?= site_url('static/disclaimer') ?>">了承事項</a>
  </li>
  <li>
  <a href="<?= site_url('static/privacy') ?>">プライバシーポリシー</a>
  </li>
</ul>
</nav>
</aside>
