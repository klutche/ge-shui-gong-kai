<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>

    <?= $this->load->view('static/' . $page . '_common', null, true) ?>
  </div>

  <div class="span3" id="sidebar">
    <?= $this->load->view('sidebar', null, true) ?>
  </div>
</div>



