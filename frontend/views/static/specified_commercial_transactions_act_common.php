<table class="table">
<tr>
<th>社名</th>
<td>有限会社 九州ホールセール</td>
</tr>
<tr>
<th>店舗名</th>
<td>洗えないを洗う 革水（かわすい）</td>
</tr>
<tr>
<th>運営責任者</th>
<td>藤木 幸広</td>
</tr>
<tr>
<th>所在地</th>
<td>熊本県熊本市西区蓮台寺4-2-1</td>
</tr>
<tr>
<th>連絡先</th>
<td>Eメール：<?= safe_mailto('info@kawasui.com') ?>, <?= safe_mailto('kawasui.c@gmail.com') ?>
<br>
TEL: 096-234-8677 FAX: 096-354-8666
</td>
</tr>
<tr>
<th>URL</th>
<td>https://www.kawasui.com</td>
</tr>
<tr>
<th>お申し込み方法</th>
<td>Webサイト上・お電話・FAXにてお申し込み</td>
</tr>
<tr>
<th>価格について</th>
<td><a href="https://www.kawasui.com/price_list/">料金表</a>にて記載しております</td>
</tr>
<tr>
<th>価格以外の必要費用</th>
<td>当社への送料<br>
お支払いに関わる銀行振込み手数料、代引き手料
</td>
</tr>
<tr>
<th>お支払い方法</th>
<td>代金引換え<br>
銀行口座振込・ゆうちょ銀行・PayPay銀行(旧ジャパンネット銀行)
</td>
</tr>
<tr>
<th>クリーニング・修理の納期</th>
<td>代金引換えの場合・・お客様ご確認後、約3週間<br>
銀行口座振込の場合・・お客様ご確認、ご入金後、約3週間
</td>
</tr>
<tr>
<th>クリーニング・修理のキャンセルについて</th>
<td>各作業に入ってからのキャンセルはお受けできません。<br>
作業前の場合は、着払いにて返品致します。
</td>
</tr>
<tr>
<th>仕上り品質について</th>
<td>仕上り品質に、ご意見等ございましたら、お届け１週間以内にご連絡下さい。責任を持って対応させていただきます。
</td>
</tr>
<tr>
<th>クリーニング事故について</th>
<td>別ページ「<a href="<?= site_url('static/standards_compensation') ?>">クリーニング事故賠償基準</a>」にて記載しております。宅配業者による事故に関しましては補償の対象外となります。
</td>
</tr>
<tr>
<th>個人情報について</th>
<td>別ページ「<a href="https://www.kawasui.com/privacy/">個人情報保護方針</a>」にて記載しております</td>
</tr>
</tbody>
</table>
