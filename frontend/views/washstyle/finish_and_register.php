<div class="row-fluid">
  <div class="span9" id="main" role="main">
    <h1><?= $title ?></h1>
    <h2>ご注文ありがとうございました。</h2>
    <p>ご入力頂いたメールアドレス宛に確認のメールを送信しました。</p>

    <h3>会員登録しませんか？</h3>
    <p>先程ご入力頂いた情報で会員登録ができます。</p>

    <p><a class="btn btn-primary" href="<?= site_url('member/signup_after/washstyle') ?>">会員登録する</a><p>
  </div>

</div>

