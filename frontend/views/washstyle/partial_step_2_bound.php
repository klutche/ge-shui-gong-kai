<div class="control-group">
<label class="control-label">お名前</label>
<div class="controls">
<?= $input['name'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">郵便番号</label>
<div class="controls">
〒<?= $input['zipcode'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">住所</label>
<div class="controls">
<?= $input['address'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">電話番号</label>
<div class="controls">
<?= $input['tel'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">集配方法</label>
<div class="controls">
<?= $input['method'] ?>
</div>
</div>

<?if ($input['method'] != '手配必要なし'): ?>
<div class="control-group">
<label class="control-label">ご希望の集荷日</label>
<div class="controls">
<?= $input['pickup_date'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">ご希望の集荷時間</label>
<div class="controls">
<?= $input['pickup_time'] ?>
</div>
</div>
<?endif?>

<div class="control-group">
<label class="control-label">ご希望の配送日</label>
<div class="controls">
<?= $input['delivery_date'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">ご希望の配送時間</label>
<div class="controls">
<?= $input['delivery_time'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">注文バッグ数 (大)</label>
<div class="controls">
<?= $input['bag_large'] ?>
</div>
</div>

<div class="control-group">
<label class="control-label">注文バッグ数 (小)</label>
<div class="controls">
<?= $input['bag_small'] ?>
</div>
</div>
