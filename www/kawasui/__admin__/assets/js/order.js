$(function() {
    if ($('#id_is_sendmail_0').prop('checked')) {
        $('#id_is_sendmail_0').next('span').text('送信済み');
    } else {
        $('#id_is_sendmail_0').next('span').text('未送信');
    }
    $('#id_is_sendmail_0').change(function() {
        if ($(this).prop('checked')) {
            $(this).next('span').text('送信済み');
        } else {
            $(this).next('span').text('未送信');
        }
    });
    
    $('.item').change(function() {
        var etc = $(this).siblings('.item-etc');
        if ($(this).val() == 'その他') {
            etc.show();
        } else {
            etc.hide();
        }
    });
});
