$(function() {
    var btnOptions = [
        {
            label: '<i class="lite-editor-font-back"></i>',
            action: 'undo',
            group: 'action'
        }, {
            label: '<i class="lite-editor-font-go"></i>',
            action: 'redo',
            group: 'action'
        }, {
            label: '<i class="lite-editor-font-link"></i>',
            tag: 'a',
            className: '',
            group: 'link',
            sampleText: 'link text'
        }
    ];

    const editor = new LiteEditor('.js-lite-editor', {
        maxHeight: 500,
        btnOptions: btnOptions
    });
});
