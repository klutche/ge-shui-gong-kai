var KAWASUI = {};

$(document).ready(function() {

  // 郵便番号の横に「住所検索」ボタンを足す
  var appendAddressSearchButton = function() {
    $('.btn-zip-search').remove();
    $('.address-search').each(function() {
      var zipcode = $(this);
      var button = $('<button>');
      button.attr({
        'type': 'button',
        'class': 'btn btn-zip-search'
      });
      button.css({
        'margin-bottom': '10px',
        'margin-left': '10px'
      });
      button.text('住所検索');
      button.on('click', function() {
        searchAddress(zipcode.val());
      });
      $(this).after(button);
    });
  };

  // 郵便番号から住所検索し、住所にセットする
  var searchAddress = function(zipcode) {
    var address_field = $('input[name=address]');
    address_field.val('検索中...');

    $('#alert-address').remove();

    $.ajax({
      url: base_url + 'api/search_address/' + zipcode,
      dataType: 'json',
      type: 'GET',
      success: function(json) {
        address_field.val(json.address);
        address_field.after('<div class="alert alert-success" id="alert-address">丁目・番地・建物名(部屋番号)を続けてご入力ください。</div>');
      },
      error: function(json) {
        address_field.val('');
        address_field.after('<div class="alert alert-danger" id="alert-address">住所は見つかりません。郵便番号をご確認いただくか、住所を手入力してください。</div>');
      }
    });
  };

  appendAddressSearchButton();

  KAWASUI.appendAddressSearchButton = appendAddressSearchButton;

  if (navigator.userAgent.match(/Android 2/)) {
    $('.inline-content').flickable();
  }
});
