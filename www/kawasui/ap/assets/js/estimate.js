$(document).ready(function() {
    $(document).on('change', '.item', function(e) {
        var etc = $(this).siblings('.item-etc');
        if ($(this).val() == 'その他') {
            etc.show();
        } else {
            etc.hide();
        }
    });
});
