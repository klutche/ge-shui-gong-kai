$(document).ready(function() {

  /*
   * フォームを読み込む
   * editable=1場合、強制的に編集状態にする
   */
  var loadForm = function(id, editable, scroll) {
    if (scroll == undefined) { scroll = true; }

    var section = $('#section-form-' + id);
    var content = $('.section-content', section);
    var url = section.data('url');


    $('section.section-form').removeClass('active');
    if (editable) {
      section.addClass('active');
    }

    $.ajax({
      url : url,
      type: 'GET',
      data: [
        {name: 'editable', value: editable},
        {name: 'csrf_test_name', value: $.cookie('csrf_cookie_name')}],
      dataType: 'json',
      success: function(r) {
        content.html(r.html);
        KAWASUI.appendAddressSearchButton();
        if (editable && scroll) {
          scrollTo(id);
        }
      }
    });
  };

  /*
   * フォームをPOSTし、エラーがなければ次のフォームを開く
   */
  var submitForm = function(form) {
    var section = form.closest('section');
    var content = form.closest('.section-content');
    var url = form.attr('action');
    var data = form.serializeArray();
    var btn_edit = $('.btn-edit-form', section);


    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: data,
      success: function(r) {
        content.html(r.html);

        if (r.status == 'bound') {
          section.data('bound', 1);
          section.addClass('bound');
          var sections = $('#partial-form-group > section');
          var next = null;
          sections.each(function() {
            if ($(this).data('bound') != 1 && next == null) {
              next = $(this).data('id');
            }
          });

          loadForm(next, 1);
          btn_edit.show();
        } else {
          scrollTo(section.data('id'));
        }
      }
    });

  };

  /*
   * アニメーションでスクロール
   */
  var scrollTo = function(id) {
    var section = $('#section-form-' + id);
    var top = section.offset().top;
    $('html, body').animate({
      scrollTop: top
    }, 500);

  };

  var toggleSendButton = function() {
    if ($('#id_apply_0').is(':checked')) {
      $('#btn-send').removeAttr('disabled');
    } else {
      $('#btn-send').attr('disabled', 'disabled');
    }
  };

  var copyStep1Data = function() {
    $.ajax({
      url: base_url + 'washstyle/get_step1_data',
      type: 'GET',
      dataType: 'json',
      success: function(d) {
        var data = d.data;
        for (var k in data) {
          var input = $('[name=' + k + ']');
          if (input.size() > 0) {
            input.val(data[k]);
          }
        }
      }
    });
  };

  var loadPickup = function(id) {
    $.ajax({
      url: base_url + 'washstyle/get_step2_data',
      type: 'GET',
      data: [{name: 'id', value: id}],
      dataType: 'json',
      success: function(d) {
        var data = d.data;
        if (data) {
          for (var k in data) {
            var input = $('[name=' + k + ']');
            if (input.size() > 0) {
              input.val(data[k]);
            }
          }
        } else {
          $('input[name=name],input[name=zipcode],input[name=address],input[name=tel]').val('');
        }
      }
    });
  };


  // 最初のフォームを読み込み
  loadForm('1', 1, false);

  // Events
  $(document).on('submit', '.section-content > form.partial', function(e) {
    e.preventDefault();
    submitForm($(this));
  });

  $(document).on('click', '.btn-edit-form', function(e) {
    if ($('section.active').size() > 0) {
      var active_id = $('section.active').data('id');
      loadForm(active_id, 0);
    }

    var id = $(this).closest('section').data('id');
    loadForm(id, 1);
    scrollTo(id);
    $(this).hide();
  });

  $(document).on('click', '#btn-copy-step1-data', function(e) {
    copyStep1Data();
  });

  $(document).on('click', '.pickups input[type=radio]', function(e) {
    var pickup_id = $(this).data('pickup-id');
    loadPickup(pickup_id);
  });

  $(document).on('click', 'input[name=method]', function(e) {
    var date = $('#washstylestep2form-field-5');
    var time = $('#washstylestep2form-field-6');
    if ($(this).val() == '手配必要なし') {
      date.fadeOut();
      time.fadeOut();
    } else {
      date.fadeIn();
      time.fadeIn();
    }
  });

  setInterval(function() {
    toggleSendButton();
  }, 500);

});
